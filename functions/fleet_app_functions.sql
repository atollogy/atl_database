--
-- PostgreSQL database dump
--

-- Dumped from database version 10.13
-- Dumped by pg_dump version 10.15 (Ubuntu 10.15-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: add_edge_config_v4(integer, integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.add_edge_config_v4(user_id_in integer, camera_id_in integer, data_in json) RETURNS TABLE(id integer, name character varying, camera_id bigint, image_step json, photo character varying, orientation integer, resolution_type integer, resolution character varying, "interval" integer, exposure_time integer, duration integer, fps integer, image_collector boolean, optimal_brightness integer, threshold integer, capture_gap integer, capture_count integer, regions jsonb, iso integer, white_balance jsonb, steps json, hdr boolean)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_camera record;
	_role_id integer;
	_camera_edge_config record;
	_step json;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;

	select cec.* into _camera_edge_config from camera_edge_config cec where cec.camera_id = camera_id_in and cec.expired_time is null limit 1;
	
	if _camera_edge_config.id is not null then
		raise exception 'ONLY_SUPPORT_ONE_EDGE';		
	end if;

	select * into _camera
	from camera c
	where c.id = camera_id_in;

	insert into camera_edge_config(
		camera_id, 
		image_step_id, 
		photo,
		created_by,
		orientation,
		resolution_type,
		resolution,
		"interval",
		duration,
		fps,
		image_collector,
		exposure_time,
		device_name,
		"name",
		optimal_brightness,
		threshold,
		capture_gap,
		capture_count,
		regions,
		white_balance,
		iso,
		hdr
	)
	select 
		camera_id_in,
		("data_in" ->> 'imageStepId')::integer,
		coalesce("data_in" ->> 'photo', _camera.photo),
		user_id_in,
		("data_in" ->> 'orientation')::integer,
		("data_in" ->> 'resolutionType')::integer,
		("data_in" ->> 'resolution')::varchar,
		("data_in" ->> 'interval')::integer,
		("data_in" ->> 'duration')::integer,
		("data_in" ->> 'fps')::integer,
		("data_in" ->> 'imageCollector')::boolean,
		("data_in" ->> 'exposureTime')::integer,
		("data_in" ->> 'deviceName')::varchar,
		("data_in" ->> 'name')::varchar,
		("data_in" ->> 'optimalBrightness')::integer,
		("data_in" ->> 'threshold')::integer,
		("data_in" ->> 'captureGap')::integer,
		("data_in" ->> 'captureCount')::integer,
		("data_in" ->> 'regions')::jsonb,
		("data_in" ->> 'whiteBalance')::jsonb,
		("data_in" ->> 'iso')::integer,
		("data_in" ->> 'hdr')::boolean
	returning * into _camera_edge_config;
	
	for _step in select * from json_array_elements(data_in -> 'steps')
	loop
		insert into edge_config_step(
			camera_edge_config_id, 
			"name",
			created_by,
			image_aggregate,
			image_transform,
			parameter_crop,
			parameter_scale,
			parameter_location,
			parameter_radius,
			parameter_vthreshold
		)
		select 
			_camera_edge_config.id,
			(_step ->> 'name')::varchar,
			user_id_in,
			(_step ->> 'imageAggregate')::varchar,
			(_step ->> 'imageTransform')::varchar,
			(_step ->> 'crop')::jsonb,
			(_step ->> 'scale')::float,
			(_step ->> 'location')::jsonb,
			(_step ->> 'radius')::float,
			(_step ->> 'vThreshold')::float;
	end loop;
	
	
	update camera c 
	set status = 2, updated_by = user_id_in, updated_time = current_timestamp, is_config_published = false
	where c.id = camera_id_in;

	update "group" g
	set updated_by = user_id_in, updated_time = current_timestamp
	where g.id = (
		select cus.id
		from camera c
		left join "group" fac on fac.id = c.group_id
		left join "group" cus on cus.id = fac.parent_group_id
		where c.id = camera_id_in
	);

	return query select 
		g.id, g.name, g.camera_id, g.image_step, g.photo, g.orientation, g.resolution_type, g.resolution, g."interval", g.exposure_time, g.duration, g.fps, g.image_collector, g.optimal_brightness, g.threshold, g.capture_gap, g.capture_count, g.regions, g.iso, g.white_balance, g.steps, g.hdr
	from get_edge_config_v4(user_id_in, _camera_edge_config.id) g;
END
$$;


ALTER FUNCTION public.add_edge_config_v4(user_id_in integer, camera_id_in integer, data_in json) OWNER TO atollogy;

--
-- Name: add_image_installation_v2(integer, integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.add_image_installation_v2(user_id_in integer, camera_id_in integer, data_in json) RETURNS TABLE(id integer, camera_id bigint, photo character varying, notes json)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_image_installation record;
	_count_notes integer := 0;
	_notes json;
	_note_item json;
begin
	insert into camera_installation_image(
		camera_id, 
		photo,
		created_by
	)
	select
		camera_id_in,
		"data_in" ->> 'photo',
		user_id_in
	returning * into _image_installation;
	
	loop
      exit when _count_notes = json_array_length(data_in -> 'notes');
      _notes := data_in -> 'notes';
      _note_item := _notes -> _count_notes;
       raise notice 'sada:%', _note_item;
     insert into camera_installation_image_note(camera_installation_image_id, box_x_postion, box_y_position, box_width, box_height, description, color)
     select
     	_image_installation.id,
		("_note_item" ->> 'boxXPosition')::integer,
		("_note_item" ->> 'boxYPosition')::integer,
		("_note_item" ->> 'boxWidth')::integer,
		("_note_item" ->> 'boxHeight')::integer,
		"_note_item" ->> 'description',
		"_note_item" ->> 'color';
		
      _count_notes := _count_notes + 1;
  	end loop;
	
	return query select
		ci.id, ci.camera_id, ci.photo, ci.notes
	from get_image_installation_detail_v2(user_id_in, _image_installation.id) ci;
END
$$;


ALTER FUNCTION public.add_image_installation_v2(user_id_in integer, camera_id_in integer, data_in json) OWNER TO atollogy;

--
-- Name: add_image_step_v2(integer, integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.add_image_step_v2(user_id_in integer, camera_id_in integer, data_in json) RETURNS TABLE(id integer, photo character varying, image_step json, box_x_position integer, box_y_position integer, box_width integer, box_height integer, opacity numeric)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_image_step record;
begin
	insert into camera_image_step(
		camera_id, 
		image_step_id,
		photo,
		box_x_position,
		box_y_position,
		box_width,
		box_height,
		opacity,
		created_by
	)
	select
		camera_id_in,
		("data_in" ->> 'imageStepId')::integer,
		"data_in" ->> 'photo',
		("data_in" ->> 'boxXPosition')::integer,
		("data_in" ->> 'boxYPosition')::integer,
		("data_in" ->> 'boxWidth')::integer,
		("data_in" ->> 'boxHeight')::integer,
		("data_in" ->> 'opacity')::numeric,
		user_id_in
	returning * into _image_step;
	
	return query select
		cis.id,
		cis.photo,
		json_build_object('id', ims.id, 'name', ims."name") as image_step,
		cis.box_x_position,
		cis.box_y_position,
		cis.box_width,
		cis.box_height,
		cis.opacity
		from camera_image_step cis
		left join image_step ims on ims.id = cis.image_step_id
		where cis.id = _image_step.id;
END
$$;


ALTER FUNCTION public.add_image_step_v2(user_id_in integer, camera_id_in integer, data_in json) OWNER TO atollogy;

--
-- Name: add_image_step_v3(integer, integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.add_image_step_v3(user_id_in integer, camera_id_in integer, data_in json) RETURNS TABLE(id integer, name character varying, image_step json, photo character varying, region_filter boolean, box_name character varying, box_x_position integer, box_y_position integer, box_width integer, box_height integer, opacity numeric, brightness_track boolean, annotate_after_filter boolean, index integer, config jsonb, enable boolean)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_image_step record;
	_camera record;
	_largest_index integer;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	select c.* into _image_step 
	from camera_image_step c 
	where (c.expired_time is null or c.expired_time > now()) and (lower(c.name) = lower(data_in ->> 'name')) and c.camera_id = camera_id_in
	limit 1;

	case when (_image_step.id is not null) THEN
		raise exception 'IMAGE_STEP_EXISTED' using detail = 'name=' || _image_step.name;
	else end case;
	
	select * into _camera
	from camera c
	where c.id = camera_id_in;

	select cis."index" into _largest_index
	from camera_image_step cis
	where (cis.expired_time is null or cis.expired_time > now()) and cis.camera_id = camera_id_in and
		case when ("data_in" ->> 'imageStepId')::integer in (1, 2) then cis.image_step_id in (1, 2)
			else cis.image_step_id in (5, 6)
		end
	order by cis.id desc limit 1;

	if _largest_index is null
		then _largest_index = 0;
	end if;
	
	insert into camera_image_step(
		camera_id,
		"name",
		image_step_id, 
		photo,
		region_filter,
		box_name,
		box_x_position,
		box_y_position,
		box_width,
		box_height,
		opacity,
		brightness_track,
		annotate_after_filter,
		created_by,
		"index",
		config,
		subjects,
		brightness,
		input_image_indexes,
		input_step,
		color_space
	)
	
	select
		camera_id_in,
		"data_in" ->> 'name',
		("data_in" ->> 'imageStepId')::integer,
		coalesce("data_in" ->> 'photo', _camera.photo),
		("data_in" ->> 'regionFilter')::bool,
		"data_in" ->> 'boxName',
		case when ("data_in" ->> 'imageStepId')::integer = 6 then null
			else ("data_in" ->> 'boxXPosition')::integer
		end,
		case when ("data_in" ->> 'imageStepId')::integer = 6 then null
			else ("data_in" ->> 'boxYPosition')::integer
		end,
		case when ("data_in" ->> 'imageStepId')::integer = 6 then null
			else ("data_in" ->> 'boxWidth')::integer
		end,
		case when ("data_in" ->> 'imageStepId')::integer = 6 then null
			else ("data_in" ->> 'boxHeight')::integer
		end,
		("data_in" ->> 'opacity')::numeric,
		case when ("data_in" ->> 'imageStepId')::integer = 1 then ("data_in" ->> 'brightnessTrack')::bool
			else null
		end,
		case when ("data_in" ->> 'imageStepId')::integer = 1 then true 
			when ("data_in" ->> 'imageStepId')::integer = 2 then false
			else null
		end,
		user_id_in,
		_largest_index + 1,
		("data_in" ->> 'config')::jsonb,
		("data_in" ->> 'subjects')::jsonb,
		("data_in" ->> 'brightness')::jsonb,
		("data_in" ->> 'inputImageIndexes')::jsonb,
		("data_in" ->> 'inputStep')::varchar,
		("data_in" ->> 'colorSpace')::varchar
	returning * into _image_step;

	update camera c 
	set status = 2, updated_by = user_id_in, updated_time = current_timestamp, is_config_published = false
	where c.id = camera_id_in;

	update "group" g
	set updated_by = user_id_in, updated_time = current_timestamp
	where g.id = (
		select cus.id
		from camera c
		left join "group" fac on fac.id = c.group_id
		left join "group" cus on cus.id = fac.parent_group_id
		where c.id = camera_id_in
	);

	return query select
		cis.id,
		cis."name",
		json_build_object('id', ims.id, 'name', ims."name", 'imageStepId', ims.image_step_id) as image_step,
		cis.photo,
		cis.region_filter,
		cis.box_name,
		cis.box_x_position,
		cis.box_y_position,
		cis.box_width,
		cis.box_height,
		cis.opacity,
		cis.brightness_track,
		cis.annotate_after_filter,
		cis."index",
		cis.config,
		cis."enable"
		from camera_image_step cis
		left join image_step ims on ims.id = cis.image_step_id
		where cis.id = _image_step.id;
END
$$;


ALTER FUNCTION public.add_image_step_v3(user_id_in integer, camera_id_in integer, data_in json) OWNER TO atollogy;

--
-- Name: add_image_step_v4(integer, integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.add_image_step_v4(user_id_in integer, camera_id_in integer, data_in json) RETURNS TABLE(id integer, name character varying, image_step json, photo character varying, region_filter boolean, box_name character varying, box_x_position integer, box_y_position integer, box_width integer, box_height integer, opacity numeric, brightness_track boolean, annotate_after_filter boolean, index integer, config jsonb, enable boolean, subjects jsonb, brightness jsonb, input_image_indexes jsonb, input_step character varying, color_space character varying, regions jsonb, input_crop jsonb, image_complexity_threshold integer, "verbose" boolean)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_image_step record;
	_camera record;
	_largest_index integer;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	select c.* into _image_step 
	from camera_image_step c 
	where c.expired_time is null and (lower(c.name) = lower(data_in ->> 'name')) and c.camera_id = camera_id_in
	limit 1;

	case when (_image_step.id is not null) THEN
		raise exception 'IMAGE_STEP_EXISTED' using detail = 'name=' || _image_step.name;
	else end case;
	
	select * into _camera
	from camera c
	where c.id = camera_id_in;

	select cis."index" into _largest_index
	from camera_image_step cis
	where cis.expired_time is null and cis.camera_id = camera_id_in and
		case when ("data_in" ->> 'imageStepId')::integer in (1, 2) then cis.image_step_id in (1, 2)
			else cis.image_step_id in (5, 6)
		end
	order by cis.id desc limit 1;

	if _largest_index is null
		then _largest_index = 0;
	end if;
	
	insert into camera_image_step(
		camera_id,
		"name",
		image_step_id, 
		photo,
		region_filter,
		box_name,
		box_x_position,
		box_y_position,
		box_width,
		box_height,
		opacity,
		brightness_track,
		annotate_after_filter,
		created_by,
		"index",
		config,
		subjects,
		brightness,
		input_image_indexes,
		input_step,
		color_space,
		regions,
		input_crop,
		image_complexity_threshold, 
		"verbose"
	)
	
	select
		camera_id_in,
		"data_in" ->> 'name',
		("data_in" ->> 'imageStepId')::integer,
		coalesce("data_in" ->> 'photo', _camera.photo),
		("data_in" ->> 'regionFilter')::bool,
		"data_in" ->> 'boxName',
		case when ("data_in" ->> 'imageStepId')::integer = 6 then null
			else ("data_in" ->> 'boxXPosition')::integer
		end,
		case when ("data_in" ->> 'imageStepId')::integer = 6 then null
			else ("data_in" ->> 'boxYPosition')::integer
		end,
		case when ("data_in" ->> 'imageStepId')::integer = 6 then null
			else ("data_in" ->> 'boxWidth')::integer
		end,
		case when ("data_in" ->> 'imageStepId')::integer = 6 then null
			else ("data_in" ->> 'boxHeight')::integer
		end,
		("data_in" ->> 'opacity')::numeric,
		case when ("data_in" ->> 'imageStepId')::integer = 1 then ("data_in" ->> 'brightnessTrack')::bool
			else null
		end,
		case when ("data_in" ->> 'imageStepId')::integer = 1 then true 
			when ("data_in" ->> 'imageStepId')::integer = 2 then false
			else null
		end,
		user_id_in,
		_largest_index + 1,
		("data_in" ->> 'config')::jsonb,
		("data_in" ->> 'subjects')::jsonb,
		("data_in" ->> 'brightness')::jsonb,
		("data_in" ->> 'inputImageIndexes')::jsonb,
		("data_in" ->> 'inputStep')::varchar,
		("data_in" ->> 'colorSpace')::varchar,
		("data_in" ->> 'regions')::jsonb,
		("data_in" ->> 'inputCrop')::jsonb,
		("data_in" ->> 'imageComplexityThreshold')::integer,
		("data_in" ->> 'verbose')::bool
	returning * into _image_step;

	update camera c 
	set status = 2, updated_by = user_id_in, updated_time = current_timestamp, is_config_published = false
	where c.id = camera_id_in;

	update "group" g
	set updated_by = user_id_in, updated_time = current_timestamp
	where g.id = (
		select cus.id
		from camera c
		left join "group" fac on fac.id = c.group_id
		left join "group" cus on cus.id = fac.parent_group_id
		where c.id = camera_id_in
	);

	return query select
		cis.id,
		cis."name",
		json_build_object('id', ims.id, 'name', ims."name", 'imageStepId', ims.image_step_id) as image_step,
		cis.photo,
		cis.region_filter,
		cis.box_name,
		cis.box_x_position,
		cis.box_y_position,
		cis.box_width,
		cis.box_height,
		cis.opacity,
		cis.brightness_track,
		cis.annotate_after_filter,
		cis."index",
		cis.config,
		cis."enable",
		cis.subjects, 
		cis.brightness,
		cis.input_image_indexes, 
		cis.input_step,
		cis.color_space,
		cis.regions,
		cis.input_crop,
		cis.image_complexity_threshold,
		cis."verbose"
		from camera_image_step cis
		left join image_step ims on ims.id = cis.image_step_id
		where cis.id = _image_step.id;
END
$$;


ALTER FUNCTION public.add_image_step_v4(user_id_in integer, camera_id_in integer, data_in json) OWNER TO atollogy;

--
-- Name: add_tenants_v2(integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.add_tenants_v2(user_id_in integer, data_in json) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

declare
	_count_tenant integer := 0;
	_tenant json;
	_tenant_record record;

	_count_organization integer := 0;
	_organization json;
	_organization_record record;

	_count_device integer := 0;
	_device json;
	_device_record record;

begin
	loop
		exit when _count_tenant = json_array_length(data_in);
    		_tenant := data_in -> _count_tenant;
    		select g.* into _tenant_record from "group" g where (g.alias_name = _tenant ->> 'tenant') and (g.expired_time is null or g.expired_time > now()) and g.group_type_id = 1;
    
    		if _tenant_record.id is null then
	    		insert into "group"(group_type_id, "name", created_by, alias_name)
				select
					1,
					(_tenant ->> 'tenant')::varchar,
					user_id_in,
					(_tenant ->> 'tenant')::varchar
				returning * into _tenant_record;
		end if;
		_count_organization := 0;
		loop
			exit when _count_organization = json_array_length(_tenant -> 'organizations');
			_organization := _tenant -> 'organizations' -> _count_organization;
			raise notice '_organization%', _organization;
			select g.* into _organization_record from "group" g where (g.alias_name = _organization ->> 'organization') and (g.expired_time is null or g.expired_time > now()) and g.group_type_id = 2;
			
			if _organization_record.id is null then
				insert into "group"(group_type_id, "name", parent_group_id, created_by, alias_name)
				select
					2,
					(_organization ->> 'organization')::varchar,
					_tenant_record.id,
					user_id_in,
					(_organization ->> 'organization')::varchar
				returning * into _organization_record;
			
				insert into facility_report_type_map(facility_id, report_type_id, created_by, "check")
				select g.id, rt.id, user_id_in, false
				from "group" g
				left join report_type rt on true
				where g.id = _organization_record.id and rt.private = false;

			else 
				update "group" g set
					expired_time = null,
					parent_group_id = _tenant_record.id
				where g.id = _organization_record.id;
			end if;
			_count_device := 0;
			loop
				exit when _count_device = json_array_length(_organization -> 'devices');
				_device := _organization -> 'devices' -> _count_device;
				raise notice '_device%', _device;
				select c.* into _device_record from camera c where (c.camera_id = _device ->> 'cameraId') and (c.expired_time is null or c.expired_time > now());
				
				if _device_record.id is null then
					insert into camera(group_id, "name", camera_id, created_by)
					select 
						_organization_record.id,
						(_device ->> 'name')::varchar,
						(_device ->> 'cameraId')::varchar,
						user_id_in
					returning * into _device_record;
				else
					update camera c set 
						expired_time = null,
						name = (_device ->> 'name')::varchar,
						group_id = _organization_record.id
					where c.id = _device_record.id; 
				end if;
				perform import_image_step_v4(user_id_in, _device_record.id, (_device ->> 'imageSteps')::json, (_device ->> 'edgeConfigs')::json);
				_count_device := _count_device + 1;
				
			end loop;	
		
			_count_organization := _count_organization + 1;
		end loop;
	
		_count_tenant := _count_tenant + 1;
  	end loop;
	
	return true;
END
$$;


ALTER FUNCTION public.add_tenants_v2(user_id_in integer, data_in json) OWNER TO atollogy;

--
-- Name: create_camera_v2(integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.create_camera_v2(user_id_in integer, data_in json) RETURNS TABLE(id integer, name character varying, ip_address character varying, photo character varying, camera_id character varying, image_updated_time timestamp without time zone, camera_node json, step_name character varying, interval_time integer, firmware_version character varying, switch json, port integer, power_source json, connected_mode json, location character varying, notes text)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_camera record;
begin
	
	select c.* into _camera 
	from camera c 
	where (c.expired_time is null or c.expired_time > now()) and (c.name = data_in ->> 'name')
	limit 1;

	case when (_camera.id is not null) AND (_camera.name = data_in ->> 'name') THEN
		raise exception 'CAMERA_EXISTED';
	else end case;
	
	insert into camera(group_id, "name", ip_address, camera_id, camera_node_id, interval_time, firmware_version, switch_id, port, power_source_id, connected_mode_id, "location", notes, created_by, photo, step_name)
	select 
		(data_in ->> 'groupId')::integer,
		(data_in ->> 'name')::varchar,
		(data_in ->> 'ipAddress')::varchar,
		(data_in ->> 'cameraId')::varchar,
		(data_in ->> 'cameraNodeId')::int8,
		(data_in ->> 'intervalTime')::integer,
		(data_in ->> 'firmwareVersion')::varchar,
		(data_in ->> 'switchId')::integer,
		(data_in ->> 'port')::integer,
		(data_in ->> 'powerSourceId')::integer,
		(data_in ->> 'connectedModeId')::integer,
		(data_in ->> 'location'),
		(data_in ->> 'notes'),
		user_id_in,
		(data_in ->> 'photo'),
		(data_in ->> 'stepName')
	returning * INTO _camera;
	return query
		select c.id, c.name, c.ip_address, c.photo, c.camera_id, c.image_updated_time, c.camera_node, c.step_name, c.interval_time, c.firmware_version, c.switch, c.port, c.power_source, c.connected_mode, c."location", c.notes
		from get_camera_v2(user_id_in, _camera.id) c;
END

$$;


ALTER FUNCTION public.create_camera_v2(user_id_in integer, data_in json) OWNER TO atollogy;

--
-- Name: create_camera_v3(integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.create_camera_v3(user_id_in integer, data_in json) RETURNS TABLE(id integer, name character varying, ip_address character varying, photo character varying, camera_id character varying, image_updated_time timestamp without time zone, camera_node json, step_name character varying, interval_time integer, firmware_version character varying, switch json, port integer, power_source json, connected_mode json, location character varying, notes text, image_activities json, image_installations character varying[], facility json, customer json, image_width integer, image_height integer, updated_time timestamp without time zone, is_config_published boolean, online_window_time integer)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_camera record;
	_role_id integer;
begin
	
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;

	select c.* into _camera 
	from camera c 
	left join "group" cgr on cgr.id = c.group_id
	left join "group" te on te.id = cgr.parent_group_id
	where (c.expired_time is null or c.expired_time > now()) and (c.camera_id = data_in ->> 'cameraId') and cgr.expired_time is null and te.expired_time is null
	limit 1;

	case when (_camera.id is not null) THEN
		raise exception 'CAMERA_EXISTED';
	else end case;
	
	insert into camera(group_id, "name", ip_address, camera_id, camera_node_id, interval_time, firmware_version, switch_id, port, power_source_id, connected_mode_id, "location", notes, created_by, online_window_time)
	select 
		(data_in ->> 'groupId')::integer,
		(data_in ->> 'name')::varchar,
		(data_in ->> 'ipAddress')::varchar,
		(data_in ->> 'cameraId')::varchar,
		(data_in ->> 'cameraNodeId')::int8,
		case when ("data_in" ->> 'intervalTime')::integer is null then 60 
			else 60
		end,
		(data_in ->> 'firmwareVersion')::varchar,
		(data_in ->> 'switchId')::integer,
		(data_in ->> 'port')::integer,
		(data_in ->> 'powerSourceId')::integer,
		(data_in ->> 'connectedModeId')::integer,
		(data_in ->> 'location'),
		(data_in ->> 'notes'),
		user_id_in,
		case when ("data_in" ->> 'onlineWindowTime')::integer is null then 5
			else ("data_in" ->> 'onlineWindowTime')::integer
		end
	returning * INTO _camera;
	return query
		select c.id, 
		c.name, 
		c.ip_address, 
		c.photo,
		c.camera_id, 
		c.image_updated_time, 
		c.camera_node, 
		c.step_name, 
		c.interval_time, 
		c.firmware_version, 
		c.switch, 
		c.port, 
		c.power_source, 
		c.connected_mode, 
		c."location", 
		c.notes,
		c.image_activities,
		c.image_installations,
		c.facility,
		c.customer,
		c.image_width,
		c.image_height,
		c.updated_time,
		c.is_config_published,
		c.online_window_time
		from get_camera_v4(user_id_in, _camera.id) c;
END

$$;


ALTER FUNCTION public.create_camera_v3(user_id_in integer, data_in json) OWNER TO atollogy;

--
-- Name: create_customer_v1(integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.create_customer_v1(user_id_in integer, name_in character varying, photo_in character varying) RETURNS TABLE(id integer, name character varying, photo character varying)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_customer record;
begin
	SELECT cus.* INTO _customer FROM "customer" cus WHERE (cus.name = name_in) and cus.expired_time is null LIMIT 1;
	CASE WHEN (_customer.id IS NOT NULL) AND (_customer.name = name_in) THEN
		RAISE EXCEPTION 'CUSTOMER_EXISTED';
	ELSE END CASE;
	
	insert into "customer"(
		"name",
		"photo",
		"created_time",
		"updated_time",
		"effective_time",
		"created_by",
		"updated_by"
		)
		select 
			name_in, 
			photo_in, 
			now(),
			now(),
			now(),
			user_id_in,
			user_id_in
		RETURNING "customer".* INTO _customer;
	
	RETURN QUERY 
		SELECT cus."id",
			cus.name,
			cus.photo
		FROM "customer" cus   
		WHERE cus.expired_time is null order by name;
END

$$;


ALTER FUNCTION public.create_customer_v1(user_id_in integer, name_in character varying, photo_in character varying) OWNER TO atollogy;

--
-- Name: create_customer_v3(integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.create_customer_v3(user_id_in integer, name_in character varying, photo_in character varying) RETURNS TABLE(id integer, name character varying, photo character varying, status integer, updated_time timestamp without time zone)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_customer_record record;
	_role_id integer;
begin
	
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 or _role_id = 2 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;

	select g.* into _customer_record 
	from "group" g 
	where (g.expired_time is null or g.expired_time > now()) and g.group_type_id = 1 and (lower(g.alias_name) = lower(regexp_replace(name_in, '\s', '', 'g')))
	limit 1;
	
	case when (_customer_record.id is not null) THEN
		raise exception 'CUSTOMER_EXISTED' using detail = 'name=' || _customer_record.name;
	else end case;
	
	insert into "group"(group_type_id, "name", photo, alias_name, created_by)
	select
		1,
		name_in,
		photo_in,
		regexp_replace(name_in, '\s', '', 'g'),
		user_id_in
	returning * into _customer_record;

	return query
		select g.id, g.name, g.photo, g.status, g.updated_time
		from "group" g
		where g.id = _customer_record.id;
END

$$;


ALTER FUNCTION public.create_customer_v3(user_id_in integer, name_in character varying, photo_in character varying) OWNER TO atollogy;

--
-- Name: create_customer_v3(integer, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.create_customer_v3(user_id_in integer, name_in character varying, alias_name_in character varying, photo_in character varying) RETURNS TABLE(id integer, name character varying, alias_name character varying, photo character varying, status integer, updated_time timestamp without time zone)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_customer_record record;
	_role_id integer;
begin
	
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 or _role_id = 2 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;

	select g.* into _customer_record 
	from "group" g 
	where g.expired_time is null and g.group_type_id = 1 and 
		(g.alias_name = alias_name_in or g."name" = name_in)
	limit 1;
	
	case when (_customer_record.id is not null) THEN
		raise exception 'CUSTOMER_EXISTED' using detail = 'name=' || _customer_record.name;
	else end case;
	
	insert into "group"(group_type_id, "name", photo, alias_name, created_by)
	select
		1,
		name_in,
		photo_in,
		alias_name_in,
		user_id_in
	returning * into _customer_record;

	return query
		select g.id, g.name, g.alias_name, g.photo, g.status, g.updated_time
		from "group" g
		where g.id = _customer_record.id;
END

$$;


ALTER FUNCTION public.create_customer_v3(user_id_in integer, name_in character varying, alias_name_in character varying, photo_in character varying) OWNER TO atollogy;

--
-- Name: create_facility_v1(integer, integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.create_facility_v1(user_id_in integer, customer_id_in integer, name_in character varying, photo_in character varying) RETURNS TABLE(id integer, customer_id integer, name character varying, photo character varying)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_facility record;
begin
	SELECT fac.* INTO _facility FROM "facility" fac WHERE (fac.name = name_in) and fac.expired_time is null and fac.customer_id = customer_id_in LIMIT 1;
	CASE WHEN (_facility.id IS NOT NULL) AND (_facility.name = name_in) THEN
		RAISE EXCEPTION 'FACILITY_EXISTED';
	ELSE END CASE;
	
	insert into "facility"(
		"customer_id",
		"name",
		"photo",
		"created_time",
		"updated_time",
		"effective_time",
		"created_by",
		"updated_by"
		)
		select 
			customer_id_in,
			name_in, 
			photo_in, 
			now(),
			now(),
			now(),
			user_id_in,
			user_id_in
		RETURNING "facility".* INTO _facility;
	
	RETURN QUERY 
		SELECT fac."id",
			fac.customer_id,
			fac.name,
			fac.photo
		FROM "facility" fac   
		WHERE fac.expired_time is null and fac.customer_id = customer_id_in order by name;
END

$$;


ALTER FUNCTION public.create_facility_v1(user_id_in integer, customer_id_in integer, name_in character varying, photo_in character varying) OWNER TO atollogy;

--
-- Name: create_facility_v3(integer, integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.create_facility_v3(user_id_in integer, customer_id_in integer, name_in character varying, photo_in character varying) RETURNS TABLE(id integer, name character varying, photo character varying, status integer, updated_time timestamp without time zone)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_customer record;
	_facility record;
begin
	select g.* into _customer
	from "group" g
	where g.id = customer_id_in and (g.expired_time is null or g.expired_time > now());

	if _customer.id is null then
		raise exception 'CUSTOMER_DOES_NOT_EXIST';
	end if;
	
	select g.* into _facility 
	from "group" g 
	where (g.expired_time is null or g.expired_time > now()) and g.group_type_id = 2 and (lower(g.alias_name) = lower(regexp_replace(name_in, '\s', '', 'g')))
	limit 1;
	
	case when (_facility.id is not null) THEN
		raise exception 'FACILITY_EXISTED' using detail = 'name=' || _facility.name;
	else end case;
	
	insert into "group"(group_type_id, parent_group_id, "name", photo, alias_name, created_by)
	select
		2,
		customer_id_in,
		name_in,
		photo_in,
		regexp_replace(name_in, '\s', '', 'g'),
		user_id_in
	returning * into _facility;

	insert into switch(group_id, "name")
	select _facility.id, unnest(array['Cisco SG95-16', 'Cisco SG95-17', 'Cisco SG95-18']);
	
	insert into camera_node(group_id, "name")
	select _facility.id, unnest(array['Atloverhead04_borgwarner_prd', 'Atloverhead05_borgwarner_prd', 'Atloverhead06_borgwarner_prd']);

	return query
		select g.id, g.name, g.photo, g.status, g.updated_time
		from "group" g
		where g.id = _facility.id;
END

$$;


ALTER FUNCTION public.create_facility_v3(user_id_in integer, customer_id_in integer, name_in character varying, photo_in character varying) OWNER TO atollogy;

--
-- Name: create_facility_v4(integer, integer, character varying, character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.create_facility_v4(user_id_in integer, customer_id_in integer, name_in character varying, photo_in character varying, raw_offset_in integer, dst_offset_in integer) RETURNS TABLE(id integer, name character varying, photo character varying, status integer, updated_time timestamp without time zone, raw_offset integer, dst_offset integer)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_customer record;
	_facility record;
	_role_id integer;
begin
	
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;

	select g.* into _customer
	from "group" g
	where g.id = customer_id_in and (g.expired_time is null or g.expired_time > now());

	if _customer.id is null then
		raise exception 'CUSTOMER_DOES_NOT_EXIST';
	end if;
	
	select g.* into _facility 
	from "group" g 
	where (g.expired_time is null or g.expired_time > now()) and g.group_type_id = 2 and (lower(g.alias_name) = lower(regexp_replace(name_in, '\s', '', 'g'))) and g.parent_group_id = _customer.id
	limit 1;
	
	case when (_facility.id is not null) THEN
		raise exception 'FACILITY_EXISTED' using detail = 'name=' || _facility.name;
	else end case;
	
	insert into "group"(group_type_id, parent_group_id, "name", photo, alias_name, created_by, raw_offset, dst_offset)
	select
		2,
		customer_id_in,
		name_in,
		photo_in,
		regexp_replace(name_in, '\s', '', 'g'),
		user_id_in,
		raw_offset_in,
		dst_offset_in
	returning * into _facility;

	insert into switch(group_id, "name")
	select _facility.id, unnest(array['Cisco SG95-16', 'Cisco SG95-17', 'Cisco SG95-18']);
	
	insert into camera_node(group_id, "name")
	select _facility.id, unnest(array['Atloverhead04_borgwarner_prd', 'Atloverhead05_borgwarner_prd', 'Atloverhead06_borgwarner_prd']);

	return query
		select g.id, g.name, g.photo, g.status, g.updated_time, g.raw_offset, g.dst_offset
		from "group" g
		where g.id = _facility.id;
END

$$;


ALTER FUNCTION public.create_facility_v4(user_id_in integer, customer_id_in integer, name_in character varying, photo_in character varying, raw_offset_in integer, dst_offset_in integer) OWNER TO atollogy;

--
-- Name: create_facility_v4(integer, integer, character varying, character varying, character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.create_facility_v4(user_id_in integer, customer_id_in integer, name_in character varying, alias_name_in character varying, photo_in character varying, raw_offset_in integer, dst_offset_in integer) RETURNS TABLE(id integer, name character varying, alias_name character varying, photo character varying, status integer, updated_time timestamp without time zone, raw_offset integer, dst_offset integer, customer_id integer, report_types json)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_customer record;
	_facility record;
	_role_id integer;
begin
	
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;

	select g.* into _customer
	from "group" g
	where g.id = customer_id_in and (g.expired_time is null or g.expired_time > now());

	if _customer.id is null then
		raise exception 'CUSTOMER_DOES_NOT_EXIST';
	end if;
	
	select g.* into _facility 
	from "group" g 
	where (g.expired_time is null or g.expired_time > now()) and g.group_type_id = 2 and 
		(g.alias_name = alias_name_in or g."name" = name_in) and g.parent_group_id = _customer.id
	limit 1;
	
	case when (_facility.id is not null) THEN
		raise exception 'FACILITY_EXISTED' using detail = 'name=' || _facility.name;
	else end case;
	
	insert into "group"(group_type_id, parent_group_id, "name", photo, alias_name, created_by, raw_offset, dst_offset)
	select
		2,
		customer_id_in,
		name_in,
		photo_in,
		alias_name_in,
		user_id_in,
		raw_offset_in,
		dst_offset_in
	returning * into _facility;

	insert into switch(group_id, "name")
	select _facility.id, unnest(array['Cisco SG95-16', 'Cisco SG95-17', 'Cisco SG95-18']);
	
	insert into camera_node(group_id, "name")
	select _facility.id, unnest(array['Atloverhead04_borgwarner_prd', 'Atloverhead05_borgwarner_prd', 'Atloverhead06_borgwarner_prd']);

	insert into facility_report_type_map(facility_id, report_type_id, created_by, "check")
	select _facility.id, rt.id, user_id_in, false
	from report_type rt
	where rt.private = false;

	return query
		select g.id, g.name, g.alias_name, g.photo, g.status, g.updated_time, g.raw_offset, g.dst_offset, g.parent_group_id as customer_id,
		(
			select concat('[', array_to_string(array_agg(json_build_object('facilityId', r.facility_id, 'reportTypeId', r.report_type_id, 'check', r.check, 'reportTypeName', r.name)), ','), ']')::json
			from (
			   	select frtm.*, rt.*
				from facility_report_type_map frtm
				left join report_type rt on rt.id = frtm.report_type_id
				where frtm.facility_id = g.id
			) r
		) as report_types
		from "group" g
		where g.id = _facility.id;
END

$$;


ALTER FUNCTION public.create_facility_v4(user_id_in integer, customer_id_in integer, name_in character varying, alias_name_in character varying, photo_in character varying, raw_offset_in integer, dst_offset_in integer) OWNER TO atollogy;

--
-- Name: create_facility_v5(integer, integer, character varying, character varying, character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.create_facility_v5(user_id_in integer, customer_id_in integer, name_in character varying, alias_name_in character varying, photo_in character varying, raw_offset_in integer, dst_offset_in integer) RETURNS TABLE(id integer, name character varying, alias_name character varying, photo character varying, status integer, updated_time timestamp without time zone, raw_offset integer, dst_offset integer, customer_id integer, report_types json)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_customer record;
	_facility record;
	_user_info record;
begin
	
	if user_id_in is null then
		select u.* into _user_info
		from "user" u
		where u.expired_time is null and u.role_id = 1
		limit 1;
	else
		select u.* into _user_info
		from "user" u
		where u.id = user_id_in;
	end if;
	
	if _user_info.role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;

	if customer_id_in is not null then
		select g.* into _customer
		from "group" g
		where g.id = customer_id_in and (g.expired_time is null or g.expired_time > now());
	
		if _customer.id is null then
			raise exception 'CUSTOMER_DOES_NOT_EXIST';
		end if;
	end if;
	
	select g.* into _facility 
	from "group" g 
	where (g.expired_time is null or g.expired_time > now()) and g.group_type_id = 2 and 
		(g.alias_name = alias_name_in or g."name" = name_in) and 
		(
			case when customer_id_in is not null then g.parent_group_id = customer_id_in
				else true
			end
		)	
	limit 1;
	
	case when (_facility.id is not null) THEN
		raise exception 'FACILITY_EXISTED' using detail = 'name=' || _facility.name;
	else end case;
	
	insert into "group"(group_type_id, parent_group_id, "name", photo, alias_name, created_by, raw_offset, dst_offset)
	select
		2,
		customer_id_in,
		name_in,
		photo_in,
		alias_name_in,
		_user_info.id,
		raw_offset_in,
		dst_offset_in
	returning * into _facility;

	insert into switch(group_id, "name")
	select _facility.id, unnest(array['Cisco SG95-16', 'Cisco SG95-17', 'Cisco SG95-18']);
	
	insert into camera_node(group_id, "name")
	select _facility.id, unnest(array['Atloverhead04_borgwarner_prd', 'Atloverhead05_borgwarner_prd', 'Atloverhead06_borgwarner_prd']);

	insert into facility_report_type_map(facility_id, report_type_id, created_by, "check")
	select _facility.id, rt.id, _user_info.id, false
	from report_type rt
	where rt.private = false;

	return query
		select g.id, g.name, g.alias_name, g.photo, g.status, g.updated_time, g.raw_offset, g.dst_offset, g.parent_group_id as customer_id,
		(
			select concat('[', array_to_string(array_agg(json_build_object('facilityId', r.facility_id, 'reportTypeId', r.report_type_id, 'check', r.check, 'reportTypeName', r.name)), ','), ']')::json
			from (
			   	select frtm.*, rt.*
				from facility_report_type_map frtm
				left join report_type rt on rt.id = frtm.report_type_id
				where frtm.facility_id = g.id
			) r
		) as report_types
		from "group" g
		where g.id = _facility.id;
END

$$;


ALTER FUNCTION public.create_facility_v5(user_id_in integer, customer_id_in integer, name_in character varying, alias_name_in character varying, photo_in character varying, raw_offset_in integer, dst_offset_in integer) OWNER TO atollogy;

--
-- Name: create_facility_v5(integer, integer, character varying, character varying, character varying, integer, integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.create_facility_v5(user_id_in integer, customer_id_in integer, name_in character varying, alias_name_in character varying, photo_in character varying, raw_offset_in integer, dst_offset_in integer, setting_in json) RETURNS TABLE(id integer, name character varying, alias_name character varying, photo character varying, status integer, updated_time timestamp without time zone, raw_offset integer, dst_offset integer, customer_id integer, report_types json)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_customer record;
	_facility record;
	_user_info record;
begin
	
	if user_id_in is null then
		select u.* into _user_info
		from "user" u
		where u.expired_time is null and u.role_id = 1
		limit 1;
	else
		select u.* into _user_info
		from "user" u
		where u.id = user_id_in;
	end if;
	
	if _user_info.role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;

	if customer_id_in is not null then
		select g.* into _customer
		from "group" g
		where g.id = customer_id_in and (g.expired_time is null or g.expired_time > now());
	
		if _customer.id is null then
			raise exception 'CUSTOMER_DOES_NOT_EXIST';
		end if;
	end if;
	
	select g.* into _facility 
	from "group" g 
	where (g.expired_time is null or g.expired_time > now()) and g.group_type_id = 2 and 
		(g.alias_name = alias_name_in or g."name" = name_in) and 
		(
			case when customer_id_in is not null then g.parent_group_id = customer_id_in
				else true
			end
		)	
	limit 1;
	
	case when (_facility.id is not null) THEN
		raise exception 'FACILITY_EXISTED' using detail = 'name=' || _facility.name;
	else end case;
	
	insert into "group"(group_type_id, parent_group_id, "name", photo, alias_name, created_by, raw_offset, dst_offset, setting)
	select
		2,
		customer_id_in,
		name_in,
		photo_in,
		alias_name_in,
		_user_info.id,
		raw_offset_in,
		dst_offset_in,
		setting_in::jsonb
	returning * into _facility;

	insert into switch(group_id, "name")
	select _facility.id, unnest(array['Cisco SG95-16', 'Cisco SG95-17', 'Cisco SG95-18']);
	
	insert into camera_node(group_id, "name")
	select _facility.id, unnest(array['Atloverhead04_borgwarner_prd', 'Atloverhead05_borgwarner_prd', 'Atloverhead06_borgwarner_prd']);

	insert into facility_report_type_map(facility_id, report_type_id, created_by, "check")
	select _facility.id, rt.id, _user_info.id, false
	from report_type rt
	where rt.private = false;

	return query
		select g.id, g.name, g.alias_name, g.photo, g.status, g.updated_time, g.raw_offset, g.dst_offset, g.parent_group_id as customer_id,
		(
			select concat('[', array_to_string(array_agg(json_build_object('facilityId', r.facility_id, 'reportTypeId', r.report_type_id, 'check', r.check, 'reportTypeName', r.name)), ','), ']')::json
			from (
			   	select frtm.*, rt.*
				from facility_report_type_map frtm
				left join report_type rt on rt.id = frtm.report_type_id
				where frtm.facility_id = g.id
			) r
		) as report_types
		from "group" g
		where g.id = _facility.id;
END

$$;


ALTER FUNCTION public.create_facility_v5(user_id_in integer, customer_id_in integer, name_in character varying, alias_name_in character varying, photo_in character varying, raw_offset_in integer, dst_offset_in integer, setting_in json) OWNER TO atollogy;

--
-- Name: create_nancy_user_v1(json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.create_nancy_user_v1(user_profile_ip json) RETURNS TABLE(id bigint, username character varying, status integer, first_name character varying, last_name character varying, photo character varying, role_id integer, role_name character varying, account_type_id integer, account_type_name character varying, phone_country_code character varying, customers json, created_time timestamp without time zone)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_user record;
begin	
	insert into "user"(
		"username",
		"password",
		"first_name", 
		"last_name",
		"hashed_username", 
		"role_id",
		"email", 
		"account_type_id", 
		status
	)
	select 
		"user_profile_ip" ->> 'username', 
		"user_profile_ip" ->> 'password', 
		"user_profile_ip" ->> 'firstName', 
		"user_profile_ip" ->> 'lastName', 
		"user_profile_ip" ->> 'hashedUsername',  
		("user_profile_ip" ->> 'roleId')::int4,
		"user_profile_ip" ->> 'email',
		1,
		1
	RETURNING "user".* INTO _user;

	insert into public.facility_user(user_id, facility_id, created_by)
	values(_user.id, 
			("user_profile_ip" ->> 'cgrId')::int4,
			_user.id
	);
	
	
	RETURN QUERY 
		select u.id, u.username, u.status, u.first_name, u.last_name, u.photo, u.role_id, ur.name as role_name, u.account_type_id, ac.name as account_type_name, u.phone_country_code,
		(
			select concat('[', array_to_string(array_agg(row_to_json(r.*)), ','), ']')::json 
			from get_customers_v4(u.id::integer) r
		) as customers,
		u.created_time
		from "user" u
		left join user_role ur on ur.id = u.role_id
		left join account_type ac on ac.id = u.account_type_id
		left join customer_user cu on cu.user_id = u.id
		left join "group" g on g.id = cu.customer_id
		where u.expired_time is null and u."id" = _user."id";
END

$$;


ALTER FUNCTION public.create_nancy_user_v1(user_profile_ip json) OWNER TO atollogy;

--
-- Name: create_user_mugshot_criteria_selection(integer, character varying, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.create_user_mugshot_criteria_selection(user_id_in integer, query_url_in character varying, body_in json) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

DECLARE
	_info record;
begin
	select * into _info from user_mugshot_criteria_selection where user_id = user_id_in;

	if _info.id is null then
		insert into user_mugshot_criteria_selection(user_id, "timestamp", query_url, body)
		values(
			user_id_in, 
			current_timestamp, 
			query_url_in,
			body_in
		);
	else
		update user_mugshot_criteria_selection set
			timestamp = current_timestamp,
			query_url = query_url_in,
			body = body_in
		where id = _info.id;
	end if;
	
	return true;
END

$$;


ALTER FUNCTION public.create_user_mugshot_criteria_selection(user_id_in integer, query_url_in character varying, body_in json) OWNER TO atollogy;

--
-- Name: create_user_v1(json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.create_user_v1(user_profile_ip json) RETURNS TABLE(id bigint, username character varying, status integer, first_name character varying, last_name character varying, photo character varying, role_id integer, role_name character varying, account_type_id integer, account_type_name character varying, phone_country_code character varying, customers json, created_time timestamp without time zone)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_user record;
	_customer record;
	_facility_ids jsonb;
begin
	SELECT u.* INTO _user FROM "user" u 
	WHERE (u.username = ("user_profile_ip" ->> 'username')) and u.expired_time is null
	LIMIT 1;
	CASE WHEN (_user.id IS NOT NULL) AND (_user.username = ("user_profile_ip" ->> 'username')) THEN
		RAISE EXCEPTION 'EMAIL_EXISTED';
	ELSE END CASE;

	if (("user_profile_ip" ->> 'roleId')::int4 = 2) then
		select g.* into _customer from "group" g 
		where g.group_type_id = 1 and g.id = ("user_profile_ip" ->> 'customerId')::int4 and g.expired_time is null;
		if _customer.id is null then
			raise exception 'CUSTOMER_DOES_NOT_EXIST';
		end if;
	end if;

	if (("user_profile_ip" ->> 'roleId')::int4 = 3 or ("user_profile_ip" ->> 'roleId')::int4 = 5) then
		select json_agg(g.id) into _facility_ids
		from "group" g
		where g.group_type_id = 2 and g.expired_time is null
		and g.id = any(
			select r::text::integer from json_array_elements((user_profile_ip ->> 'facilityIds')::json) r
		);
		
		if not (_facility_ids::jsonb @> (user_profile_ip ->> 'facilityIds')::jsonb and (user_profile_ip ->> 'facilityIds')::jsonb @> _facility_ids::jsonb) then
			raise exception 'YOU_DONT_HAVE_PERMISSION';
		end if;
	end if;
	
	insert into "user"(
		"username",
		"password",
		"first_name", 
		"last_name",
		"phone_number",
		"photo", 
		phone_country_code, 
		"hashed_username", 
		"gender", 
		"date_of_birth", 
		"role_id",
		"email", 
		"account_type_id", 
		status
		)
		select 
			"user_profile_ip" ->> 'username', 
			"user_profile_ip" ->> 'password', 
			"user_profile_ip" ->> 'firstName', 
			"user_profile_ip" ->> 'lastName', 
			"user_profile_ip" ->> 'phoneNumber', 
			"user_profile_ip" ->> 'photo', 
			"user_profile_ip" ->> 'phoneCountryCode', 
			"user_profile_ip" ->> 'hashedUsername', 
			"user_profile_ip" ->> 'gender', 
			("user_profile_ip" ->> 'dateOfBirth')::TIMESTAMP, 
			("user_profile_ip" ->> 'roleId')::int4,
			"user_profile_ip" ->> 'email',
			1,
			1
		RETURNING "user".* INTO _user;

	if _user.role_id = 2 then
		insert into public.customer_user(user_id, customer_id, created_by)
		values(_user.id, 
				("user_profile_ip" ->> 'customerId')::int4,
				("user_profile_ip" ->> 'userId')::int4
		);
	end if;

	if _user.role_id = 3 or _user.role_id = 5 then
		insert into facility_user(user_id, facility_id, created_by)
		select _user.id,
				r::text::integer,
				("user_profile_ip" ->> 'userId')::int4
		from json_array_elements((user_profile_ip ->> 'facilityIds')::json) r;
	end if;
	
	RETURN QUERY 
		select u.id, u.username, u.status, u.first_name, u.last_name, u.photo, u.role_id, ur.name as role_name, u.account_type_id, ac.name as account_type_name, u.phone_country_code,
		(
			select concat('[', array_to_string(array_agg(row_to_json(r.*)), ','), ']')::json 
			from get_customers_v4(u.id::integer) r
		) as customers,
		u.created_time
		from "user" u
		left join user_role ur on ur.id = u.role_id
		left join account_type ac on ac.id = u.account_type_id
		left join customer_user cu on cu.user_id = u.id
		left join "group" g on g.id = cu.customer_id
		where u.expired_time is null and u."id" = _user."id";
END

$$;


ALTER FUNCTION public.create_user_v1(user_profile_ip json) OWNER TO atollogy;

--
-- Name: create_user_workbench_criteria_selection(integer, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.create_user_workbench_criteria_selection(user_id_in integer, query_url_in character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

DECLARE
	_info record;
begin
	select * into _info from user_workbench_criteria_selection where user_id = user_id_in;

	if _info.id is null then
		insert into user_workbench_criteria_selection(user_id, "timestamp", query_url)
		values(
			user_id_in, 
			current_timestamp, 
			query_url_in
		);
	else
		update user_workbench_criteria_selection set
			timestamp = current_timestamp,
			query_url = query_url_in
		where id = _info.id;
	end if;
	
	return true;
END

$$;


ALTER FUNCTION public.create_user_workbench_criteria_selection(user_id_in integer, query_url_in character varying) OWNER TO atollogy;

--
-- Name: delete_camera_v3(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.delete_camera_v3(user_id_in integer, camera_id_in integer) RETURNS TABLE(id integer, name character varying, ip_address character varying, photo character varying, camera_id character varying, image_updated_time timestamp without time zone, camera_node json, step_name character varying, interval_time integer, firmware_version character varying, switch json, port integer, power_source json, connected_mode json, location character varying, notes text, image_activities json, image_installations character varying[], facility json, customer json, image_width integer, image_height integer, updated_time timestamp without time zone, is_config_published boolean, online_window_time integer)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_current_camera record;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	select c.* into _current_camera
	from "camera" c
	where c.id = camera_id_in and (c.expired_time is null or c.expired_time > now());

	if _current_camera.id is null then
		raise exception 'CAMERA_DOES_NOT_EXIST';
	end if;

	update "camera" c
		set expired_time = current_timestamp,
		updated_by = user_id_in
	where c.id = camera_id_in;

	return query
		select c.id, 
		c.name, 
		c.ip_address, 
		c.photo,
		c.camera_id, 
		c.image_updated_time, 
		c.camera_node, 
		c.step_name, 
		c.interval_time, 
		c.firmware_version, 
		c.switch, 
		c.port, 
		c.power_source, 
		c.connected_mode, 
		c."location", 
		c.notes,
		c.image_activities,
		c.image_installations,
		c.facility,
		c.customer,
		c.image_width,
		c.image_height,
		c.updated_time,
		c.is_config_published,
		c.online_window_time
		from get_camera_v4(user_id_in, camera_id_in) c;
END

$$;


ALTER FUNCTION public.delete_camera_v3(user_id_in integer, camera_id_in integer) OWNER TO atollogy;

--
-- Name: delete_customer_v1(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.delete_customer_v1(user_id_in integer, id_in integer) RETURNS TABLE(id integer, name character varying, photo character varying)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_customer record;
	_current_customer_id integer;
begin
	select cus.id into _current_customer_id from customer cus where cus.id = id_in;
	if _current_customer_id is null then
		RAISE EXCEPTION 'CUSTOMER_DOES_NOT_EXIST';
	end if;
	update "customer" cus set
		"expired_time" = now()
		where cus.id = id_in;
	
	RETURN QUERY 
		SELECT cus."id",
			cus.name,
			cus.photo
		FROM "customer" cus   
		WHERE cus.expired_time is null order by name;
END

$$;


ALTER FUNCTION public.delete_customer_v1(user_id_in integer, id_in integer) OWNER TO atollogy;

--
-- Name: delete_customer_v3(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.delete_customer_v3(user_id_in integer, customer_id_in integer) RETURNS TABLE(id integer, name character varying, photo character varying, status integer, updated_time timestamp without time zone)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_current_customer record;
	_role_id integer;
begin
	
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 or _role_id = 2 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	select g.* into _current_customer
	from "group" g
	where g.id = customer_id_in and g.group_type_id = 1 and (g.expired_time is null or g.expired_time > now());

	if _current_customer.id is null then
		raise exception 'CUSTOMER_DOES_NOT_EXIST';
	end if;

	update "group" g
		set expired_time = current_timestamp,
		updated_by = user_id_in
	where g.id = customer_id_in;

	return query
		select cus.id,
				cus."name",
				cus.photo,
				cus.status,
				cus.updated_time
		from "group" cus
		where cus.group_type_id = 1 and cus.id = customer_id_in;
END

$$;


ALTER FUNCTION public.delete_customer_v3(user_id_in integer, customer_id_in integer) OWNER TO atollogy;

--
-- Name: delete_edge_config_v4(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.delete_edge_config_v4(user_id_in integer, edge_config_id_in integer) RETURNS TABLE(id integer, camera_id bigint, image_step json, photo character varying, orientation integer, resolution_type integer, resolution character varying, "interval" integer, exposure_time integer, duration integer, fps integer, image_collector boolean, steps json)
    LANGUAGE plpgsql
    AS $$

declare
	_edge_config record;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;

	update camera_edge_config cec
	set expired_time = now()
	where cec.id = edge_config_id_in;

	select cec.* into _edge_config from camera_edge_config cec where cec.id = edge_config_id_in;

	update camera c 
	set is_config_published = false
	where c.id = _edge_config.camera_id;

	return query select 
		g.id, g.camera_id, g.image_step, g.photo, g.orientation, g.resolution_type, g.resolution, g."interval", g.exposure_time, g.duration, g.fps, g.image_collector, g.steps
	from get_edge_config_v4(user_id_in, edge_config_id_in) g;
END

$$;


ALTER FUNCTION public.delete_edge_config_v4(user_id_in integer, edge_config_id_in integer) OWNER TO atollogy;

--
-- Name: delete_facility_v1(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.delete_facility_v1(user_id_in integer, id_in integer) RETURNS TABLE(id integer, customer_id integer, name character varying, photo character varying)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_facility record;
	_current_facility record;
begin
	select fac.* into _current_facility from facility fac where fac.id = id_in;
	if _current_facility.id is null then
		RAISE EXCEPTION 'FACILITY_DOES_NOT_EXIST';
	end if;
	update "facility" fac set
		"expired_time" = now()
		where fac.id = id_in;
	
	RETURN QUERY 
		SELECT fac."id",
			fac.customer_id,
			fac.name,
			fac.photo
		FROM "facility" fac   
		WHERE fac.expired_time is null and fac.customer_id = _current_facility.customer_id order by name;
END

$$;


ALTER FUNCTION public.delete_facility_v1(user_id_in integer, id_in integer) OWNER TO atollogy;

--
-- Name: delete_facility_v3(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.delete_facility_v3(user_id_in integer, facility_id_in integer) RETURNS TABLE(id integer, name character varying, photo character varying, status integer, updated_time timestamp without time zone)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_facility record;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	select g.* into _facility
	from "group" g
	where g.id = facility_id_in and g.group_type_id = 2 and (g.expired_time is null or g.expired_time > now());

	if _facility.id is null then
		raise exception 'FACILITY_DOES_NOT_EXIST';
	end if;

	update "group" g
		set expired_time = current_timestamp,
		updated_by = user_id_in
	where g.id = facility_id_in;

	return query
		select fac.id,
				fac."name",
				fac.photo,
				fac.status,
				fac.updated_time
		from "group" fac
		where fac.group_type_id = 2 and fac.id = facility_id_in;
END

$$;


ALTER FUNCTION public.delete_facility_v3(user_id_in integer, facility_id_in integer) OWNER TO atollogy;

--
-- Name: delete_image_step_v2(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.delete_image_step_v2(user_id_in integer, image_step_id_in integer) RETURNS TABLE(id integer, name character varying, image_step json, photo character varying, region_filter boolean, box_name character varying, box_x_position integer, box_y_position integer, box_width integer, box_height integer, opacity numeric, brightness_track boolean, annotate_after_filter boolean, index integer, config jsonb, enable boolean, subjects jsonb, brightness jsonb, input_image_indexes jsonb, input_step character varying, color_space character varying, regions jsonb, input_crop jsonb, image_complexity_threshold integer)
    LANGUAGE plpgsql
    AS $$

declare
	_image_step record;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	update camera_image_step cis
	set expired_time= now()
	where cis.id = image_step_id_in;

	select i.* into _image_step from camera_image_step i where i.id = image_step_id_in;

	update camera c 
	set is_config_published = false
	where c.id = _image_step.camera_id;

	return query select
		cis.id,
		cis."name",
		json_build_object('id', ims.id, 'name', ims."name", 'imageStepId', ims.image_step_id) as image_step,
		cis.photo,
		cis.region_filter,
		cis.box_name,
		cis.box_x_position,
		cis.box_y_position,
		cis.box_width,
		cis.box_height,
		cis.opacity,
		cis.brightness_track,
		cis.annotate_after_filter,
		cis."index",
		cis.config,
		cis."enable",
		cis.subjects, 
		cis.brightness,
		cis.input_image_indexes, 
		cis.input_step,
		cis.color_space,
		cis.regions,
		cis.input_crop,
		cis.image_complexity_threshold
		from camera_image_step cis
		left join image_step ims on ims.id = cis.image_step_id
		where cis.id = image_step_id_in;
END

$$;


ALTER FUNCTION public.delete_image_step_v2(user_id_in integer, image_step_id_in integer) OWNER TO atollogy;

--
-- Name: delete_user_by_admin_v4(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.delete_user_by_admin_v4(user_id_in integer, user_delete_id integer) RETURNS TABLE(id bigint, hashed_username character varying)
    LANGUAGE plpgsql
    AS $$

declare
	_user_info record;
begin
	select u.* into _user_info from "user" u where u.id = user_id_in;
	if _user_info.role_id <> 1 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	update "user" u set expired_time = current_timestamp where u.id = user_delete_id;
	return query select u.id, u.hashed_username from "user" u where u.id = user_delete_id;
END

$$;


ALTER FUNCTION public.delete_user_by_admin_v4(user_id_in integer, user_delete_id integer) OWNER TO atollogy;

--
-- Name: get_camera_nodes_v2(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_camera_nodes_v2(user_id_in integer, group_id_in integer) RETURNS TABLE(id integer, group_id integer, name character varying)
    LANGUAGE plpgsql
    AS $$

DECLARE
begin
	return query select 
		cn.id, 
		cn.group_id,
		cn.name
	from camera_node as cn
	where cn.group_id = group_id_in;
END

$$;


ALTER FUNCTION public.get_camera_nodes_v2(user_id_in integer, group_id_in integer) OWNER TO atollogy;

--
-- Name: get_camera_v2(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_camera_v2(user_id_in integer, camera_id_in integer) RETURNS TABLE(id integer, name character varying, ip_address character varying, photo character varying, camera_id character varying, image_updated_time timestamp without time zone, camera_node json, step_name character varying, interval_time integer, firmware_version character varying, switch json, port integer, power_source json, connected_mode json, location character varying, notes text, image_activities character varying[], image_installations character varying[])
    LANGUAGE plpgsql
    AS $$

declare
	_image_activities varchar[];
	_image_installations varchar[];
begin
	select array_agg(cis.photo) into _image_activities from camera_image_step cis where cis.camera_id = camera_id_in;
	select array_agg(cii.photo) into _image_installations from camera_installation_image cii where cii.camera_id = camera_id_in;
	
	return query select 
		c.id, 
		c.name, 
		c.ip_address, 
		c.photo,
		c.camera_id, 
		c.image_updated_time, 
		json_build_object('id', cn.id,'name', cn.name) as camera_node, 
		c.step_name, 
		c.interval_time, 
		c.firmware_version, 
		json_build_object('id', s.id,'name', s.name) as switch, 
		c.port, 
		json_build_object('id', ps.id,'name', ps.name) as power_source, 
		json_build_object('id', cm.id,'name', cm.name) as connected_mode, 
		c."location", 
		c.notes,
		_image_activities as image_activities,
		_image_installations as image_installations
	from camera as c
	left join camera_node cn on cn.id = c.camera_node_id
	left join switch s on s.id = c.switch_id
	left join power_souce ps on ps.id = c.power_source_id
	left join connected_mode cm on cm.id = c.connected_mode_id
	where (c.expired_time is null or c.expired_time > now()) and c.id = camera_id_in;
END

$$;


ALTER FUNCTION public.get_camera_v2(user_id_in integer, camera_id_in integer) OWNER TO atollogy;

--
-- Name: get_camera_v3(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_camera_v3(user_id_in integer, camera_id_in integer) RETURNS TABLE(id integer, name character varying, ip_address character varying, photo character varying, camera_id character varying, image_updated_time timestamp without time zone, camera_node json, step_name character varying, interval_time integer, firmware_version character varying, switch json, port integer, power_source json, connected_mode json, location character varying, notes text, image_activities json, image_installations character varying[], facility json, customer json, image_width integer, image_height integer, updated_time timestamp without time zone, is_config_published boolean)
    LANGUAGE plpgsql
    AS $$
declare
    _image_activities json;
    _image_installations varchar[];
begin
    select concat('[', array_to_string(array_agg(row_to_json(r.*)), ','), ']')::json into _image_activities
    from (
        select cis.id, cis.name, cis.box_name, cis.region_filter, cis.brightness_track, cis.annotate_after_filter, cis.opacity, cis.camera_id, cis.photo, cis.box_x_position, cis.box_y_position, cis.box_width, cis.box_height,json_build_object('id', ci.id, 'name', ci.name, 'imageStepId' ,ci.image_step_id) as image_step, cis."index"
        from camera_image_step cis 
        left join image_step ci on ci.id = cis.image_step_id
        where cis.camera_id = camera_id_in and (cis.expired_time is null or cis.expired_time > now())
        order by cis.index asc
    ) r;
    select array_agg(cii.photo) into _image_installations from camera_installation_image cii where cii.camera_id = camera_id_in;
    
    return query select 
        c.id, 
        c.name, 
        c.ip_address, 
        c.photo,
        c.camera_id, 
        c.image_updated_time, 
        json_build_object('id', cn.id,'name', cn.name) as camera_node, 
        c.step_name, 
        c.interval_time, 
        c.firmware_version, 
        json_build_object('id', s.id,'name', s.name) as switch, 
        c.port, 
        json_build_object('id', ps.id,'name', ps.name) as power_source, 
        json_build_object('id', cm.id,'name', cm.name) as connected_mode, 
        c."location", 
        c.notes,
        _image_activities as image_activities,
        _image_installations as image_installations,
        json_build_object('id', g.id, 'name', g."name", 'alias_name', g.alias_name) as facility,
        json_build_object('id', g1.id, 'name', g1."name", 'alias_name', g1.alias_name) as customer,
        c.image_width,
        c.image_height,
        c.updated_time,
        c.is_config_published
    from camera as c
    left join camera_node cn on cn.id = c.camera_node_id
    left join switch s on s.id = c.switch_id
    left join power_souce ps on ps.id = c.power_source_id
    left join connected_mode cm on cm.id = c.connected_mode_id
    left join "group" g on g.id = c.group_id and g.group_type_id = 2
    left join "group" g1 on g1.id = g.parent_group_id and g1.group_type_id = 1
    where (c.expired_time is null or c.expired_time > now()) and c.id = camera_id_in;
END
$$;


ALTER FUNCTION public.get_camera_v3(user_id_in integer, camera_id_in integer) OWNER TO atollogy;

--
-- Name: get_camera_v4(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_camera_v4(user_id_in integer, camera_id_in integer) RETURNS TABLE(id integer, name character varying, ip_address character varying, photo character varying, camera_id character varying, image_updated_time timestamp without time zone, camera_node json, step_name character varying, interval_time integer, firmware_version character varying, switch json, port integer, power_source json, connected_mode json, location character varying, notes text, image_activities json, image_installations character varying[], facility json, customer json, image_width integer, image_height integer, updated_time timestamp without time zone, is_config_published boolean, online_window_time integer, edge_configs json, gateway_id character varying)
    LANGUAGE plpgsql
    AS $$
declare
    _image_activities json;
   	_edge_configs json;
    _image_installations varchar[];
   	_user_info record;
begin
	select u.id, u.role_id, cu.customer_id into _user_info 
	from "user" u 
	left join customer_user cu on cu.user_id = u.id
	where u.id = user_id_in;

	select concat('[', array_to_string(array_agg(row_to_json(r.*)), ','), ']')::json into _edge_configs
	from (
	   	select r.*
		from camera_edge_config cec, get_edge_config_v4(user_id_in, cec.id) r
		where cec.camera_id = camera_id_in and cec.expired_time is null
	) r;

    select concat('[', array_to_string(array_agg(row_to_json(r.*)), ','), ']')::json into _image_activities
    from (
        select cis.id, cis.name, cis.box_name, cis.region_filter, cis.brightness_track, cis.annotate_after_filter, cis.opacity, cis.camera_id, cis.photo, cis.box_x_position, cis.box_y_position, cis.box_width, cis.box_height,json_build_object('id', ci.id, 'name', ci.name, 'imageStepId' ,ci.image_step_id) as image_step, cis."index", cis.config, cis."enable", cis.original_data, cis.subjects, cis.brightness, cis.input_image_indexes, cis.input_step, cis.color_space, cis.regions, cis.input_crop, cis.image_complexity_threshold, cis.verbose
        from camera_image_step cis 
        left join image_step ci on ci.id = cis.image_step_id
        where cis.camera_id = camera_id_in and (cis.expired_time is null)
        order by cis.index asc
    ) r;
    select array_agg(cii.photo) into _image_installations from camera_installation_image cii where cii.camera_id = camera_id_in;
    
    return query select 
        c.id, 
        c.name, 
        c.ip_address, 
        c.photo,
        c.camera_id, 
        c.image_updated_time, 
        json_build_object('id', cn.id,'name', cn.name) as camera_node, 
        c.step_name, 
        c.interval_time, 
        c.firmware_version, 
        json_build_object('id', s.id,'name', s.name) as switch, 
        c.port, 
        json_build_object('id', ps.id,'name', ps.name) as power_source, 
        json_build_object('id', cm.id,'name', cm.name) as connected_mode, 
        c."location", 
        c.notes,
        _image_activities as image_activities,
        _image_installations as image_installations,
        json_build_object('id', g.id, 'name', g."name", 'alias_name', g.alias_name) as facility,
        json_build_object('id', g1.id, 'name', g1."name", 'alias_name', g1.alias_name) as customer,
        c.image_width,
        c.image_height,
        c.updated_time,
        c.is_config_published,
        c.online_window_time,
        _edge_configs as edge_configs,
        c.gateway_id
    from camera as c
    left join camera_node cn on cn.id = c.camera_node_id
    left join switch s on s.id = c.switch_id
    left join power_souce ps on ps.id = c.power_source_id
    left join connected_mode cm on cm.id = c.connected_mode_id
    left join "group" g on g.id = c.group_id and g.group_type_id = 2
    left join "group" g1 on g1.id = g.parent_group_id and g1.group_type_id = 1
    where (c.expired_time is null or c.expired_time > now()) and c.id = camera_id_in
    	and case when _user_info.role_id = 2 then _user_info.customer_id = g1.id
				else true
			end;
END
$$;


ALTER FUNCTION public.get_camera_v4(user_id_in integer, camera_id_in integer) OWNER TO atollogy;

--
-- Name: get_camera_v5(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_camera_v5(user_id_in integer, camera_id_in integer) RETURNS TABLE(id integer, name character varying, ip_address character varying, photo character varying, camera_id character varying, image_updated_time timestamp without time zone, camera_node json, step_name character varying, interval_time integer, firmware_version character varying, switch json, port integer, power_source json, connected_mode json, location character varying, notes text, image_activities json, image_installations character varying[], facility json, customer json, image_width integer, image_height integer, updated_time timestamp without time zone, is_config_published boolean, online_window_time integer, edge_configs json, gateway_id character varying, truth_image jsonb)
    LANGUAGE plpgsql
    AS $$
declare
    _image_activities json;
   	_edge_configs json;
    _image_installations varchar[];
   	_user_info record;
   	_truth_image jsonb;
begin
	select u.id, u.role_id, cu.customer_id into _user_info 
	from "user" u 
	left join customer_user cu on cu.user_id = u.id
	where u.id = user_id_in;

	select ct.data_image into _truth_image from camera_truth_image_map ct where ct.camera_id = camera_id_in order by ct.id desc limit 1;

	select concat('[', array_to_string(array_agg(row_to_json(r.*)), ','), ']')::json into _edge_configs
	from (
	   	select r.*
		from camera_edge_config cec, get_edge_config_v4(user_id_in, cec.id) r
		where cec.camera_id = camera_id_in and cec.expired_time is null
	) r;

    select concat('[', array_to_string(array_agg(row_to_json(r.*)), ','), ']')::json into _image_activities
    from (
        select cis.id, cis.name, cis.box_name, cis.region_filter, cis.brightness_track, cis.annotate_after_filter, cis.opacity, cis.camera_id, cis.photo, cis.box_x_position, cis.box_y_position, cis.box_width, cis.box_height,json_build_object('id', ci.id, 'name', ci.name, 'imageStepId' ,ci.image_step_id) as image_step, cis."index", cis.config, cis."enable", cis.original_data, cis.subjects, cis.brightness, cis.input_image_indexes, cis.input_step, cis.color_space, cis.regions, cis.input_crop, cis.image_complexity_threshold, cis.verbose
        from camera_image_step cis 
        left join image_step ci on ci.id = cis.image_step_id
        where cis.camera_id = camera_id_in and (cis.expired_time is null)
        order by cis.index asc
    ) r;
    select array_agg(cii.photo) into _image_installations from camera_installation_image cii where cii.camera_id = camera_id_in;
    
    return query select 
        c.id, 
        c.name, 
        c.ip_address, 
        c.photo,
        c.camera_id, 
        c.image_updated_time, 
        json_build_object('id', cn.id,'name', cn.name) as camera_node, 
        c.step_name, 
        c.interval_time, 
        c.firmware_version, 
        json_build_object('id', s.id,'name', s.name) as switch, 
        c.port, 
        json_build_object('id', ps.id,'name', ps.name) as power_source, 
        json_build_object('id', cm.id,'name', cm.name) as connected_mode, 
        c."location", 
        c.notes,
        _image_activities as image_activities,
        _image_installations as image_installations,
        json_build_object('id', g.id, 'name', g."name", 'alias_name', g.alias_name) as facility,
        json_build_object('id', g1.id, 'name', g1."name", 'alias_name', g1.alias_name) as customer,
        c.image_width,
        c.image_height,
        c.updated_time,
        c.is_config_published,
        c.online_window_time,
        _edge_configs as edge_configs,
        c.gateway_id,
        _truth_image as truth_image
    from camera as c
    left join camera_node cn on cn.id = c.camera_node_id
    left join switch s on s.id = c.switch_id
    left join power_souce ps on ps.id = c.power_source_id
    left join connected_mode cm on cm.id = c.connected_mode_id
    left join "group" g on g.id = c.group_id and g.group_type_id = 2
    left join "group" g1 on g1.id = g.parent_group_id and g1.group_type_id = 1
    where (c.expired_time is null or c.expired_time > now()) and c.id = camera_id_in
    	and case when _user_info.role_id = 2 then _user_info.customer_id = g1.id
				else true
			end;
END
$$;


ALTER FUNCTION public.get_camera_v5(user_id_in integer, camera_id_in integer) OWNER TO atollogy;

--
-- Name: get_camera_v6(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_camera_v6(user_id_in integer, camera_id_in integer) RETURNS TABLE(id integer, name character varying, ip_address character varying, photo character varying, camera_id character varying, image_updated_time timestamp without time zone, camera_node json, step_name character varying, interval_time integer, firmware_version character varying, switch json, port integer, power_source json, connected_mode json, location character varying, notes text, image_activities json, image_installations character varying[], facility json, customer json, image_width integer, image_height integer, updated_time timestamp without time zone, is_config_published boolean, online_window_time integer, edge_configs json, gateway_id character varying, truth_image_data jsonb, current_image character varying, truth_image character varying)
    LANGUAGE plpgsql
    AS $$
declare
    _image_activities json;
   	_edge_configs json;
    _image_installations varchar[];
   	_user_info record;
   	_truth_image_data jsonb;
   	_camera_image record;
   	_is_lpr boolean:= false;
begin
	select u.id, u.role_id, cu.customer_id into _user_info 
	from "user" u 
	left join customer_user cu on cu.user_id = u.id
	where u.id = user_id_in;

	select ct.data_image into _truth_image_data from camera_truth_image_map ct where ct.camera_id = camera_id_in order by ct.id desc limit 1;
	select ci.* into _camera_image
	from camera_image ci 
	where ci.camera_id = camera_id_in;

	select concat('[', array_to_string(array_agg(row_to_json(r.*)), ','), ']')::json into _edge_configs
	from (
	   	select r.*
		from camera_edge_config cec, get_edge_config_v4(user_id_in, cec.id) r
		where cec.camera_id = camera_id_in and cec.expired_time is null
	) r;

    select concat('[', array_to_string(array_agg(row_to_json(r.*)), ','), ']')::json into _image_activities
    from (
        select cis.id, cis.name, cis.box_name, cis.region_filter, cis.brightness_track, cis.annotate_after_filter, cis.opacity, cis.camera_id, cis.photo, cis.box_x_position, cis.box_y_position, cis.box_width, cis.box_height,json_build_object('id', ci.id, 'name', ci.name, 'imageStepId' ,ci.image_step_id) as image_step, cis."index", cis.config, cis."enable", cis.original_data, cis.subjects, cis.brightness, cis.input_image_indexes, cis.input_step, cis.color_space, cis.regions, cis.input_crop, cis.image_complexity_threshold, cis.verbose
        from camera_image_step cis 
        left join image_step ci on ci.id = cis.image_step_id
        where cis.camera_id = camera_id_in and (cis.expired_time is null)
        order by cis.index asc
    ) r;
    select array_agg(cii.photo) into _image_installations from camera_installation_image cii where cii.camera_id = camera_id_in;
   
   select not exists(
		select *
		from camera_image_step cis 
		left join image_step i on i.id = cis.image_step_id
		where cis.camera_id = camera_id_in and cis.expired_time is null and cis.image_step_id <> 9 and i.image_config_type_id = 2
	) into _is_lpr;
    
    return query select 
        c.id, 
        c.name, 
        c.ip_address, 
        c.photo,
        c.camera_id, 
        c.image_updated_time, 
        json_build_object('id', cn.id,'name', cn.name) as camera_node, 
        c.step_name, 
        c.interval_time, 
        c.firmware_version, 
        json_build_object('id', s.id,'name', s.name) as switch, 
        c.port, 
        json_build_object('id', ps.id,'name', ps.name) as power_source, 
        json_build_object('id', cm.id,'name', cm.name) as connected_mode, 
        c."location", 
        c.notes,
        _image_activities as image_activities,
        _image_installations as image_installations,
        json_build_object('id', g.id, 'name', g."name", 'alias_name', g.alias_name) as facility,
        json_build_object('id', g1.id, 'name', g1."name", 'alias_name', g1.alias_name) as customer,
        c.image_width,
        c.image_height,
        c.updated_time,
        c.is_config_published,
        c.online_window_time,
        _edge_configs as edge_configs,
        c.gateway_id,
        _truth_image_data as truth_image_data,
        (
        		case when _is_lpr = true and _camera_image.current_image_expired is not null and 
        			extract(epoch from (current_timestamp::timestamp - _camera_image.current_image_expired::timestamp))/60 <= 40 then _camera_image.current_image
        		else null
        		end
        ) as current_image,
        (
        		case when _camera_image.truth_image_expired is not null and 
        			extract(epoch from (current_timestamp::timestamp - _camera_image.truth_image_expired::timestamp))/60 <= 2160 then _camera_image.truth_image
        		else null
        		end
        ) as truth_image
    from camera as c
    left join camera_node cn on cn.id = c.camera_node_id
    left join switch s on s.id = c.switch_id
    left join power_souce ps on ps.id = c.power_source_id
    left join connected_mode cm on cm.id = c.connected_mode_id
    left join "group" g on g.id = c.group_id and g.group_type_id = 2
    left join "group" g1 on g1.id = g.parent_group_id and g1.group_type_id = 1
    where (c.expired_time is null or c.expired_time > now()) and c.id = camera_id_in
    	and case when _user_info.role_id = 2 then _user_info.customer_id = g1.id
				else true
			end;
END
$$;


ALTER FUNCTION public.get_camera_v6(user_id_in integer, camera_id_in integer) OWNER TO atollogy;

--
-- Name: get_cameras_v2(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_cameras_v2(user_id_in integer, group_id_in integer) RETURNS TABLE(id integer, name character varying, ip_address character varying, camera_id character varying, photo character varying, image_updated_time timestamp without time zone, camera_node json, step_name character varying, interval_time integer, firmware_version character varying, switch json, port integer, power_source json, connected_mode json, location character varying, notes text)
    LANGUAGE plpgsql
    AS $$

DECLARE
begin
	return query select 
		c.id, 
		c.name, 
		c.ip_address, 
		c.camera_id, 
		c.photo,
		c.image_updated_time, 
		json_build_object('id', cn.id,'name', cn.name) as camera_node, 
		c.step_name, 
		c.interval_time, 
		c.firmware_version, 
		json_build_object('id', s.id,'name', s.name) as switch, 
		c.port, 
		json_build_object('id', ps.id,'name', ps.name) as power_source, 
		json_build_object('id', cm.id,'name', cm.name) as connected_mode, 
		c."location", 
		c.notes
	from camera as c
	left join camera_node cn on cn.id = c.camera_node_id
	left join switch s on s.id = c.switch_id
	left join power_souce ps on ps.id = c.power_source_id
	left join connected_mode cm on cm.id = c.power_source_id
	where (c.expired_time is null or c.expired_time > now()) and c.group_id = group_id_in;
END

$$;


ALTER FUNCTION public.get_cameras_v2(user_id_in integer, group_id_in integer) OWNER TO atollogy;

--
-- Name: get_cameras_v3(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_cameras_v3(user_id_in integer, group_id_in integer) RETURNS TABLE(id integer, name character varying, ip_address character varying, camera_id character varying, photo character varying, image_updated_time timestamp without time zone, camera_node json, step_name character varying, interval_time integer, firmware_version character varying, switch json, port integer, power_source json, connected_mode json, location character varying, notes text, updated_time timestamp without time zone, status integer, online_window_time integer, gateway_id character varying, facility json, customer json)
    LANGUAGE plpgsql
    AS $$

declare
	_user_info record;
begin
	select u.id, u.role_id, cu.customer_id into _user_info 
	from "user" u 
	left join customer_user cu on cu.user_id = u.id
	where u.id = user_id_in;

	return query select 
		c.id, 
		c.name, 
		c.ip_address, 
		c.camera_id, 
		c.photo,
		c.image_updated_time, 
		json_build_object('id', cn.id,'name', cn.name) as camera_node, 
		c.step_name, 
		c.interval_time, 
		c.firmware_version, 
		json_build_object('id', s.id,'name', s.name) as switch, 
		c.port, 
		json_build_object('id', ps.id,'name', ps.name) as power_source, 
		json_build_object('id', cm.id,'name', cm.name) as connected_mode, 
		c."location", 
		c.notes,
		c.updated_time,
		c.status,
		c.online_window_time,
		c.gateway_id,
		json_build_object('id', fac.id, 'name', fac."name", 'alias_name', fac.alias_name) as facility,
        json_build_object('id', cus.id, 'name', cus."name", 'alias_name', cus.alias_name) as customer
	from camera as c
	left join camera_node cn on cn.id = c.camera_node_id
	left join switch s on s.id = c.switch_id
	left join power_souce ps on ps.id = c.power_source_id
	left join connected_mode cm on cm.id = c.power_source_id
	left join "group" fac on fac.id = c.group_id
	left join "group" cus on cus.id = fac.parent_group_id
	where (c.expired_time is null or c.expired_time > now()) and c.group_id = group_id_in
		and case when _user_info.role_id = 2 then _user_info.customer_id = cus.id
				else true
			end
	order by c.id asc;
END

$$;


ALTER FUNCTION public.get_cameras_v3(user_id_in integer, group_id_in integer) OWNER TO atollogy;

--
-- Name: get_cgr_all_v5(integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_cgr_all_v5(user_id_in integer) RETURNS TABLE(customers json[], image_steps json[])
    LANGUAGE plpgsql
    AS $$

DECLARE
	_customers json[];
	_image_steps json[];
begin
	with customers as (
		select fac.id, fac.name, fac.alias_name,(
			select concat('[', array_to_string(array_agg(row_to_json(d.*)), ','), ']')::text from get_image_step_v4(fac.id, null) d
		) as image_steps,
		fac.timezone
		from "group" fac 
		where fac.group_type_id = 2 and fac.alias_name is not null and fac.expired_time is null
	), cameras as (
		select cus.*, c.id as camera_id, c.name as camera_name, c.camera_id as camera_alias_name, c.gateway_id,
		(
			select array_remove(array_append(array_agg(cgm.gateway_id), c.gateway_id), null) as gateway_ids
		)
		, (
			select concat('[', array_to_string(array_agg(row_to_json(d.*)), ','), ']')::text from get_image_step_v4(null, c.id) d
		) as image_steps_camera
		from customers cus
		left join camera c on c.expired_time is null and c.group_id = cus.id
		left join camera_gateway_map cgm on cgm.camera_id = c.id
		where c.camera_id <> c.name
		group by cus.id, cus.name, cus.alias_name, cus.image_steps, cus.timezone, c.id
		order by c.id asc
	), images_steps as (
		select c.*, concat('[', array_to_string(array_agg(row_to_json(cis.*)), ','), ']')::text as camera_image_steps
		from cameras c
		left join camera_image_step cis on cis.camera_id = c.camera_id and cis.expired_time is null
		group by c.id, c.name, c.alias_name, c.image_steps, c.timezone, c.camera_id, c.camera_name, c.camera_alias_name, c.gateway_id, c.gateway_ids, c.image_steps_camera
		order by c.camera_id asc
	), group_facilites as (
		select c.id, c.name, c.alias_name, c.image_steps::json, c.timezone,
		concat('[', array_to_string(array_agg(json_build_object('id', c.camera_id, 'alias_name', c.camera_alias_name, 'name', c.camera_name, 'gateway_id', c.gateway_id, 'gateway_ids', c.gateway_ids ,'image_steps', c.image_steps_camera, 'camera_image_steps', c.camera_image_steps)), ','), ']')::json as cameras
		from images_steps c
		group by c.id, c.name, c.alias_name, c.image_steps, c.timezone
		order by c.name asc
	)
	select array_agg(row_to_json(c.*)) into _customers
	from group_facilites c;

	select array_agg(row_to_json(d.*)) into _image_steps from get_image_step_v4(null, null) d;

	return query
		select _customers as customers, _image_steps as image_steps;
END

$$;


ALTER FUNCTION public.get_cgr_all_v5(user_id_in integer) OWNER TO atollogy;

--
-- Name: get_customer_all_v4(integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_customer_all_v4(user_id_in integer) RETURNS TABLE(tenants json[], image_steps json[])
    LANGUAGE plpgsql
    AS $$

DECLARE
	_tenants json[];
	_image_steps json[];
begin
	with tenants as (
		select te.id as tenant_id, te.name as tenant_name, te.alias_name as tenant_alias_name
		from "group" te
		where te.expired_time is null and te.group_type_id = 1
		order by te.name
	), customers as (
		select te.*, fac.id as facility_id, fac.name as facility_name, fac.alias_name as facility_alias_name,(
			select concat('[', array_to_string(array_agg(row_to_json(d.*)), ','), ']')::text from get_image_step_v4(fac.id, null) d
		) as image_steps_facility
		from tenants te
		left join "group" fac on te.tenant_id = fac.parent_group_id and fac.group_type_id = 2 and fac.parent_group_id = te.tenant_id and fac.alias_name is not null and fac.expired_time is null
		order by te.tenant_alias_name
	), cameras as (
		select cus.*, c.id as camera_id, c.name as camera_name, c.camera_id as camera_alias_name, c.gateway_id, (
			select concat('[', array_to_string(array_agg(row_to_json(d.*)), ','), ']')::text from get_image_step_v4(null, c.id) d
		) as image_steps_camera
		from customers cus
		left join camera c on c.expired_time is null and c.group_id = cus.facility_id
		order by c.id asc
	), images_steps as (
		select c.*, concat('[', array_to_string(array_agg(row_to_json(cis.*)), ','), ']')::text as camera_image_steps
		from cameras c
		left join camera_image_step cis on cis.camera_id = c.camera_id and cis.expired_time is null
		group by c.tenant_id, c.tenant_name, c.tenant_alias_name, c.facility_id, c.facility_name, c.facility_alias_name, c.image_steps_facility, c.camera_id, c.camera_name, c.camera_alias_name, c.gateway_id, c.image_steps_camera
		order by c.camera_id asc
	), group_facilites as (
		select c.tenant_id, c.tenant_name, c.tenant_alias_name, c.facility_id, c.facility_name, c.facility_alias_name, c.image_steps_facility, 
		concat('[', array_to_string(array_agg(json_build_object('id', c.camera_id, 'alias_name', c.camera_alias_name, 'name', c.camera_name, 'gateway_id', c.gateway_id, 'image_steps', c.image_steps_camera, 'camera_image_steps', c.camera_image_steps)), ','), ']') as cameras
		from images_steps c
		group by c.tenant_id, c.tenant_name, c.tenant_alias_name, c.facility_id, c.facility_name, c.facility_alias_name, c.image_steps_facility
		order by c.facility_name asc
	), group_tenants as (
		select c.tenant_id as id, c.tenant_name as name, c.tenant_alias_name as alias_name, 
		concat('[', array_to_string(array_agg(json_build_object('id', c.facility_id, 'name', c.facility_name, 'alias_name', c.facility_alias_name, 'image_steps', c.image_steps_facility, 'cameras', c.cameras)), ','), ']') as facilities
		from group_facilites c
		group by c.tenant_id, c.tenant_name, c.tenant_alias_name
		order by c.tenant_name asc
	)
	select array_agg(row_to_json(c.*)) into _tenants
	from group_tenants c;


	select array_agg(row_to_json(d.*)) into _image_steps from get_image_step_v4(null, null) d;

	return query
		select _tenants as tenants, _image_steps as image_steps;
	
END

$$;


ALTER FUNCTION public.get_customer_all_v4(user_id_in integer) OWNER TO atollogy;

--
-- Name: get_customer_all_v5(integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_customer_all_v5(user_id_in integer) RETURNS TABLE(tenants json[], image_steps json[])
    LANGUAGE plpgsql
    AS $$

DECLARE
	_tenants json[];
	_image_steps json[];
begin
	with tenants as (
		select te.id as tenant_id, te.name as tenant_name, te.alias_name as tenant_alias_name
		from "group" te
		where te.expired_time is null and te.group_type_id = 1
		order by te.name
	), customers as (
		select te.*, fac.id as facility_id, fac.name as facility_name, fac.alias_name as facility_alias_name,(
			select concat('[', array_to_string(array_agg(row_to_json(d.*)), ','), ']')::text from get_image_step_v4(fac.id, null) d
		) as image_steps_facility,
		fac.timezone
		from tenants te
		left join "group" fac on te.tenant_id = fac.parent_group_id and fac.group_type_id = 2 and fac.parent_group_id = te.tenant_id and fac.alias_name is not null and fac.expired_time is null
		order by te.tenant_alias_name
	), cameras as (
		select cus.*, c.id as camera_id, c.name as camera_name, c.camera_id as camera_alias_name, c.gateway_id,
		(
			select array_remove(array_append(array_agg(cgm.gateway_id), c.gateway_id), null) as gateway_ids
		)
		, (
			select concat('[', array_to_string(array_agg(row_to_json(d.*)), ','), ']')::text from get_image_step_v4(null, c.id) d
		) as image_steps_camera
		from customers cus
		left join camera c on c.expired_time is null and c.group_id = cus.facility_id
		left join camera_gateway_map cgm on cgm.camera_id = c.id
		where c.camera_id <> c.name
		group by cus.tenant_id, cus.tenant_name, cus.tenant_alias_name, cus.facility_id, cus.facility_name, cus.facility_alias_name, cus.image_steps_facility, cus.timezone, c.id
		order by c.id asc
	), images_steps as (
		select c.*, concat('[', array_to_string(array_agg(row_to_json(cis.*)), ','), ']')::text as camera_image_steps
		from cameras c
		left join camera_image_step cis on cis.camera_id = c.camera_id and cis.expired_time is null
		group by c.tenant_id, c.tenant_name, c.tenant_alias_name, c.facility_id, c.facility_name, c.facility_alias_name, c.image_steps_facility, c.timezone, c.camera_id, c.camera_name, c.camera_alias_name, c.gateway_id, c.gateway_ids, c.image_steps_camera
		order by c.camera_id asc
	), group_facilites as (
		select c.tenant_id, c.tenant_name, c.tenant_alias_name, c.facility_id, c.facility_name, c.facility_alias_name, c.image_steps_facility, c.timezone,
		concat('[', array_to_string(array_agg(json_build_object('id', c.camera_id, 'alias_name', c.camera_alias_name, 'name', c.camera_name, 'gateway_id', c.gateway_id, 'gateway_ids', c.gateway_ids ,'image_steps', c.image_steps_camera, 'camera_image_steps', c.camera_image_steps)), ','), ']') as cameras
		from images_steps c
		group by c.tenant_id, c.tenant_name, c.tenant_alias_name, c.facility_id, c.facility_name, c.facility_alias_name, c.image_steps_facility, c.timezone
		order by c.facility_name asc
	), group_tenants as (
		select c.tenant_id as id, c.tenant_name as name, c.tenant_alias_name as alias_name, 
		concat('[', array_to_string(array_agg(json_build_object('id', c.facility_id, 'name', c.facility_name, 'alias_name', c.facility_alias_name, 'image_steps', c.image_steps_facility, 'timezone', c.timezone, 'cameras', c.cameras)), ','), ']') as facilities
		from group_facilites c
		group by c.tenant_id, c.tenant_name, c.tenant_alias_name
		order by c.tenant_name asc
	)
	select array_agg(row_to_json(c.*)) into _tenants
	from group_tenants c;


	select array_agg(row_to_json(d.*)) into _image_steps from get_image_step_v4(null, null) d;

	return query
		select _tenants as tenants, _image_steps as image_steps;
	
END

$$;


ALTER FUNCTION public.get_customer_all_v5(user_id_in integer) OWNER TO atollogy;

--
-- Name: get_customer_v1(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_customer_v1(user_id_in integer, id_in integer) RETURNS TABLE(id integer, name character varying, photo character varying)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_customer record;
	_current_customer_id integer;
begin
	select cus.id into _current_customer_id from customer cus where cus.id = id_in;
	if _current_customer_id is null then
		RAISE EXCEPTION 'CUSTOMER_DOES_NOT_EXIST';
	end if;
	RETURN QUERY 
		SELECT cus."id",
			cus.name,
			cus.photo
		FROM "customer" cus   
		WHERE cus.expired_time is null and cus.id = id_in;
END

$$;


ALTER FUNCTION public.get_customer_v1(user_id_in integer, id_in integer) OWNER TO atollogy;

--
-- Name: get_customer_v3(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_customer_v3(user_id_in integer, customer_id_in integer) RETURNS TABLE(id integer, name character varying, photo character varying, status integer, updated_time timestamp without time zone)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_current_customer record;
	_user_info record;
begin
	select u.id, u.role_id, cu.customer_id into _user_info 
	from "user" u 
	left join customer_user cu on cu.user_id = u.id
	where u.id = user_id_in;

	select g.* into _current_customer
	from "group" g
	where g.id = customer_id_in and g.group_type_id = 1 and (g.expired_time is null or g.expired_time > now())
		and case when _user_info.role_id = 2 then _user_info.customer_id = g.id
				else true
			end;

	if _current_customer.id is null then
		raise exception 'CUSTOMER_DOES_NOT_EXIST';
	end if;
	return query
		select cus.id,
				cus."name",
				cus.photo,
				cus.status,
				cus.updated_time
		from "group" cus
		where (cus.expired_time is null) and cus.group_type_id = 1 and cus.id = customer_id_in;
END

$$;


ALTER FUNCTION public.get_customer_v3(user_id_in integer, customer_id_in integer) OWNER TO atollogy;

--
-- Name: get_customers_v2(integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_customers_v2(user_id_in integer) RETURNS TABLE(id integer, name character varying, photo character varying, status integer)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_customer record;
begin
	return query
		select cus.id,
				cus."name",
				cus.photo,
				cus.status
		from "group" cus
		where (cus.expired_time is null) and cus.group_type_id = 1
		order by cus.name;
END

$$;


ALTER FUNCTION public.get_customers_v2(user_id_in integer) OWNER TO atollogy;

--
-- Name: get_customers_v3(integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_customers_v3(user_id_in integer) RETURNS TABLE(id integer, name character varying, alias_name character varying, photo character varying, status integer, updated_time timestamp without time zone)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_customer record;
	_user_info record;
begin
	
	select u.id, u.role_id, array_agg(cu.customer_id) as customers into _user_info
	from "user" u 
	left join customer_user cu on cu.user_id = u.id
	where u.id = user_id_in
	group by u.id, u.role_id;


	return query
		select cus.id,
				cus."name",
				cus.alias_name,
				cus.photo,
				cus.status,
				cus.updated_time
		from "group" cus
		where (cus.expired_time is null) and cus.group_type_id = 1 
			and case when _user_info.role_id = 1 then true
				else cus.id = any(_user_info.customers)
			end
		order by cus."name" asc;
END

$$;


ALTER FUNCTION public.get_customers_v3(user_id_in integer) OWNER TO atollogy;

--
-- Name: get_customers_v4(integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_customers_v4(user_id_in integer) RETURNS TABLE(id integer, name character varying, alias_name character varying, photo character varying, status integer, updated_time timestamp without time zone, facilities json)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_customer record;
	_user_info record;
begin
	
	select u.id, u.role_id, array_agg(cu.customer_id) as customers, array_agg(fu.facility_id) as facilities, array_agg(g.parent_group_id) as parent_customers into _user_info
	from "user" u 
	left join customer_user cu on cu.user_id = u.id
	left join facility_user fu on fu.user_id = u.id
	left join "group" g on g.id = fu.facility_id and g.group_type_id = 2
	where u.id = user_id_in
	group by u.id, u.role_id;

	return query
		select cus.id,
				cus."name",
				cus.alias_name,
				cus.photo,
				cus.status,
				cus.updated_time,
				(
					select concat('[', array_to_string(array_agg(row_to_json(r.*)), ','), ']')::json 
					from get_facilities_v4(user_id_in::integer, cus.id::integer) r
				) as facilities
		from "group" cus
		where (cus.expired_time is null) and cus.group_type_id = 1 
			and case when _user_info.role_id = 1 then true
					when _user_info.role_id = 2 then cus.id = any(_user_info.customers)
					when _user_info.role_id = 3 then cus.id = any(_user_info.parent_customers)
					when _user_info.role_id = 5 then cus.id = any(_user_info.parent_customers)
				else true
			end
		order by cus."name" asc;
END

$$;


ALTER FUNCTION public.get_customers_v4(user_id_in integer) OWNER TO atollogy;

--
-- Name: get_edge_config_v4(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_edge_config_v4(user_id_in integer, edge_config_id_in integer) RETURNS TABLE(id integer, name character varying, camera_id bigint, image_step json, photo character varying, orientation integer, resolution_type integer, resolution character varying, "interval" integer, exposure_time integer, duration integer, fps integer, image_collector boolean, optimal_brightness integer, threshold integer, capture_gap integer, capture_count integer, regions jsonb, iso integer, white_balance jsonb, steps json, hdr boolean)
    LANGUAGE plpgsql
    AS $$
declare
    _edge_config_step json;
begin
    select concat('[', array_to_string(array_agg(row_to_json(r.*)), ','), ']')::json into _edge_config_step
    from (
        select ecs.id, ecs."name", ecs.image_aggregate, ecs.image_transform, ecs.parameter_crop, ecs.parameter_location, ecs.parameter_min_flashing_transition_count, ecs.parameter_radius, ecs.parameter_scale, ecs.parameter_vthreshold
        from edge_config_step ecs 
        where ecs.camera_edge_config_id = edge_config_id_in and ecs.expired_time is null
        order by ecs.created_time asc
    ) r;
    
    return query select 
        cec.id, 
        cec.name,
        cec.camera_id, 
        json_build_object('id', si.id, 'name', si.name, 'imageStepId', si.image_step_id) as image_step,
        cec.photo, 
        cec.orientation, 
        cec.resolution_type, 
        cec.resolution, 
        cec."interval", 
        cec.exposure_time, 
        cec.duration, 
        cec.fps,
        cec.image_collector,
        cec.optimal_brightness,
        cec.threshold,
        cec.capture_gap,
        cec.capture_count,
        cec.regions,
        cec.iso,
        cec.white_balance,
        _edge_config_step as steps,
        cec.hdr
    from camera_edge_config as cec
    left join image_step si on si.id = cec.image_step_id
    where cec.id = edge_config_id_in;
 
END
$$;


ALTER FUNCTION public.get_edge_config_v4(user_id_in integer, edge_config_id_in integer) OWNER TO atollogy;

--
-- Name: get_facilities_v1(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_facilities_v1(user_id_in integer, customer_id_in integer) RETURNS TABLE(id integer, customer_id integer, name character varying, photo character varying)
    LANGUAGE plpgsql
    AS $$

DECLARE
begin
	RETURN QUERY 
		SELECT fac."id",
			fac.customer_id,
			fac.name,
			fac.photo
		FROM "facility" fac   
		WHERE fac.expired_time is null and fac.customer_id = customer_id_in order by name;
END

$$;


ALTER FUNCTION public.get_facilities_v1(user_id_in integer, customer_id_in integer) OWNER TO atollogy;

--
-- Name: get_facilities_v2(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_facilities_v2(user_id_in integer, customer_id_in integer) RETURNS TABLE(id integer, customer_id integer, name character varying, photo character varying)
    LANGUAGE plpgsql
    AS $$

DECLARE
begin
	RETURN QUERY 
		select fac.id, fac.parent_group_id as customer_id, fac.name, fac.photo
		from "group" fac
		where (fac.expired_time is null or fac.expired_time > now()) and fac.parent_group_id = customer_id_in
		order by fac."name" asc;
END

$$;


ALTER FUNCTION public.get_facilities_v2(user_id_in integer, customer_id_in integer) OWNER TO atollogy;

--
-- Name: get_facilities_v4(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_facilities_v4(user_id_in integer, customer_id_in integer) RETURNS TABLE(id integer, customer_id integer, name character varying, alias_name character varying, photo character varying, raw_offset integer, dst_offset integer, report_types json, cycle_time_report integer)
    LANGUAGE plpgsql
    AS $$

declare
	_user_info record;
begin
	select u.id, u.role_id, array_agg(cu.customer_id) as customers, array_agg(fu.facility_id) as facilities, array_agg(g.parent_group_id) as parent_customers into _user_info
	from "user" u 
	left join customer_user cu on cu.user_id = u.id
	left join facility_user fu on fu.user_id = u.id
	left join "group" g on g.id = fu.facility_id and g.group_type_id = 2
	where u.id = user_id_in
	group by u.id, u.role_id;
	
	RETURN QUERY 
		select fac.id, fac.parent_group_id as customer_id, fac.name, fac.alias_name, fac.photo, fac.raw_offset, fac.dst_offset,
		(
			select concat('[', array_to_string(array_agg(json_build_object('facilityId', r.facility_id, 'reportTypeId', r.report_type_id, 'check', r.check, 'reportTypeName', r.name, 'path', r.path)), ','), ']')::json
			from (
			   	select frtm.*, rt.*
				from facility_report_type_map frtm
				left join report_type rt on rt.id = frtm.report_type_id
				where frtm.facility_id = fac.id
			) r
		) as report_types,
		fac.cycle_time_report
		from "group" fac
		left join "group" cus on cus.id = fac.parent_group_id
		where (fac.expired_time is null or fac.expired_time > now()) and fac.alias_name is not null and cus.expired_time is null and fac.group_type_id = 2
		and case when customer_id_in is null then true
				else fac.parent_group_id = customer_id_in
			end
			and case when _user_info.role_id = 1 then true
				when _user_info.role_id = 2 then cus.id = any(_user_info.customers)
				when _user_info.role_id = 3 then fac.id = any(_user_info.facilities)
				when _user_info.role_id = 5 then fac.id = any(_user_info.facilities)
				else true
			end
		order by fac."name" asc;
END

$$;


ALTER FUNCTION public.get_facilities_v4(user_id_in integer, customer_id_in integer) OWNER TO atollogy;

--
-- Name: get_facilities_v5(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_facilities_v5(user_id_in integer, customer_id_in integer) RETURNS TABLE(id integer, name character varying, alias_name character varying, photo character varying, raw_offset integer, dst_offset integer, report_types json, cycle_time_report integer, customer json, timezone character varying)
    LANGUAGE plpgsql
    AS $$

declare
	_user_info record;
begin
	select u.id, u.role_id, array_agg(cu.customer_id) as customers, array_agg(fu.facility_id) as facilities, array_agg(g.parent_group_id) as parent_customers into _user_info
	from "user" u 
	left join customer_user cu on cu.user_id = u.id
	left join facility_user fu on fu.user_id = u.id
	left join "group" g on g.id = fu.facility_id and g.group_type_id = 2
	where u.id = user_id_in
	group by u.id, u.role_id;
	
	RETURN QUERY 
		select fac.id, fac.name, fac.alias_name, fac.photo, fac.raw_offset, fac.dst_offset,
		(
			select concat('[', array_to_string(array_agg(json_build_object('facilityId', r.facility_id, 'reportTypeId', r.report_type_id, 'check', r.check, 'reportTypeName', r.name, 'path', r.path)), ','), ']')::json
			from (
			   	select frtm.*, rt.*
				from facility_report_type_map frtm
				left join report_type rt on rt.id = frtm.report_type_id
				where frtm.facility_id = fac.id
			) r
		) as report_types,
		fac.cycle_time_report,
		json_build_object('id', cus.id, 'name', cus.name, 'aliasName', cus.alias_name) as customer,
		fac.timezone
		from "group" fac
		left join "group" cus on cus.id = fac.parent_group_id
		where (fac.expired_time is null or fac.expired_time > now()) and fac.alias_name is not null and cus.expired_time is null and fac.group_type_id = 2
		and case when customer_id_in is null then true
				else fac.parent_group_id = customer_id_in
			end
			and case when _user_info.role_id = 1 then true
				when _user_info.role_id = 2 then cus.id = any(_user_info.customers)
				when _user_info.role_id = 3 then fac.id = any(_user_info.facilities)
				when _user_info.role_id = 5 then fac.id = any(_user_info.facilities)
				else true
			end
		order by fac."name" asc;
END

$$;


ALTER FUNCTION public.get_facilities_v5(user_id_in integer, customer_id_in integer) OWNER TO atollogy;

--
-- Name: get_facility_v1(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_facility_v1(user_id_in integer, id_in integer) RETURNS TABLE(id integer, customer_id integer, name character varying, photo character varying)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_facility record;
	_current_facility record;
begin
	select fac.* into _current_facility from facility fac where fac.id = id_in;
	if _current_facility.id is null then
		RAISE EXCEPTION 'FACILITY_DOES_NOT_EXIST';
	end if;
	RETURN QUERY 
		SELECT fac."id",
			fac.customer_id,
			fac.name,
			fac.photo
		FROM "facility" fac   
		WHERE fac.expired_time is null and fac.id = id_in;
END

$$;


ALTER FUNCTION public.get_facility_v1(user_id_in integer, id_in integer) OWNER TO atollogy;

--
-- Name: get_facility_v3(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_facility_v3(user_id_in integer, facility_id_in integer) RETURNS TABLE(id integer, name character varying, photo character varying, status integer, updated_time timestamp without time zone)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_facility record;
begin
	select g.* into _facility
	from "group" g
	where g.id = facility_id_in and g.group_type_id = 2 and (g.expired_time is null or g.expired_time > now());

	if _facility.id is null then
		raise exception 'FACILITY_DOES_NOT_EXIST';
	end if;

	return query
		select g.id, g.name, g.photo, g.status, g.updated_time
		from "group" g
		where g.id = _facility.id;
END

$$;


ALTER FUNCTION public.get_facility_v3(user_id_in integer, facility_id_in integer) OWNER TO atollogy;

--
-- Name: get_facility_v4(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_facility_v4(user_id_in integer, facility_id_in integer) RETURNS TABLE(id integer, name character varying, photo character varying, status integer, updated_time timestamp without time zone, raw_offset integer, dst_offset integer, report_types json, cycle_time_report integer)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_facility record;
	_user_info record;
begin
	select u.id, u.role_id, cu.customer_id into _user_info 
	from "user" u 
	left join customer_user cu on cu.user_id = u.id
	where u.id = user_id_in;
	
	select g.* into _facility
	from "group" g
	left join "group" cus on cus.id = g.parent_group_id
	where g.id = facility_id_in and g.group_type_id = 2 and (g.expired_time is null or g.expired_time > now())
		and case when _user_info.role_id = 2 then _user_info.customer_id = cus.id
				else true
			end;

	if _facility.id is null then
		raise exception 'FACILITY_DOES_NOT_EXIST';
	end if;

	return query
		select g.id, g.name, g.photo, g.status, g.updated_time, g.raw_offset, g.dst_offset,
		(
			select concat('[', array_to_string(array_agg(json_build_object('facilityId', r.facility_id, 'reportTypeId', r.report_type_id, 'check', r.check, 'reportTypeName', r.name, 'path', r.path)), ','), ']')::json
			from (
			   	select frtm.*, rt.*
				from facility_report_type_map frtm
				left join report_type rt on rt.id = frtm.report_type_id
				where frtm.facility_id = g.id
			) r
		) as report_types,
		g.cycle_time_report
		from "group" g
		where g.id = _facility.id;
END

$$;


ALTER FUNCTION public.get_facility_v4(user_id_in integer, facility_id_in integer) OWNER TO atollogy;

--
-- Name: get_image_installation_detail_v2(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_image_installation_detail_v2(user_id_in integer, installation_image_id_in integer) RETURNS TABLE(id integer, camera_id bigint, photo character varying, notes json)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_image_installation record;
	_count_notes integer := 0;
	_notes json;
	_note_item json;
begin
	return query 
	select ci.id, ci.camera_id, ci.photo, 
		COALESCE(NULLIF(json_agg(cin.*)::TEXT, '[null]'), '[]')::JSON AS notes 
--	json_agg(cin.*) as notes
		from camera_installation_image ci
		left join camera_installation_image_note cin on cin.camera_installation_image_id = ci.id
		where ci.id = installation_image_id_in
		group by ci.id;
END
$$;


ALTER FUNCTION public.get_image_installation_detail_v2(user_id_in integer, installation_image_id_in integer) OWNER TO atollogy;

--
-- Name: get_image_installation_v2(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_image_installation_v2(user_id_in integer, camera_id_in integer) RETURNS TABLE(id integer, camera_id bigint, photo character varying, notes json)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_image_installation record;
	_count_notes integer := 0;
	_notes json;
	_note_item json;
begin
	return query 
	select ci.id, ci.camera_id, ci.photo, COALESCE(NULLIF(json_agg(cin.*)::TEXT, '[null]'), '[]')::JSON AS notes 
		from camera_installation_image ci
		left join camera_installation_image_note cin on cin.camera_installation_image_id = ci.id
		where ci.camera_id = camera_id_in
		group by ci.id;
END
$$;


ALTER FUNCTION public.get_image_installation_v2(user_id_in integer, camera_id_in integer) OWNER TO atollogy;

--
-- Name: get_image_step_v4(); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_image_step_v4() RETURNS TABLE(id integer, name character varying, image_step_id character varying, result_types jsonb)
    LANGUAGE plpgsql
    AS $$

declare

begin
	return query
		select im.id, im.name, im.image_step_id, im.result_types
		from image_step im
		where image_config_type_id = 2;
END

$$;


ALTER FUNCTION public.get_image_step_v4() OWNER TO atollogy;

--
-- Name: get_image_step_v4(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_image_step_v4(facility_id_in integer, camera_id_in integer) RETURNS TABLE(id integer, name character varying, image_step_id character varying, step_function_metadata jsonb, object_detection boolean)
    LANGUAGE plpgsql
    AS $$

DECLARE
	
begin
	if (facility_id_in is null and camera_id_in is null) then
		return query
			select ims.id, ims.name, ims.image_step_id, ims.step_function_metadata, ims.object_detection
			from image_step ims
			where ims.image_config_type_id = 2;
	else
		return query 
			select distinct(ims.id), ims.name, ims.image_step_id, ims.step_function_metadata, ims.object_detection
			from "group" g
			left join camera c on c.group_id = g.id
			left join camera_image_step cis on cis.camera_id = c.id
			left join image_step ims on ims.id = cis.image_step_id
			where (case when facility_id_in is not null then g.id = facility_id_in else true end)
				and g.group_type_id = 2 and g.expired_time is null and c.expired_time is null and cis.expired_time is null and cis.id is not null 
				and (case when camera_id_in is not null then c.id = camera_id_in else true end);
	end if;
END

$$;


ALTER FUNCTION public.get_image_step_v4(facility_id_in integer, camera_id_in integer) OWNER TO atollogy;

--
-- Name: get_info_user_by_camera_ids_v1(character varying[]); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_info_user_by_camera_ids_v1(camera_ids_in character varying[]) RETURNS TABLE(user_id bigint, first_name character varying, email character varying, receive_email_notification boolean, camera_ids character varying[], tokens text[])
    LANGUAGE plpgsql
    AS $$

declare

begin
	return query 
		select cu.user_id, u.first_name, u.email, u.receive_email_notification, array_agg(c.camera_id) as camera_ids, array_agg(dt.token) as tokens
		from camera c
		left join "group" fac on fac.id = c.group_id
		left join "group" cus on cus.id = fac.parent_group_id
		left join customer_user cu on cu.customer_id = cus.id
		left join "user" u on u.id = cu.user_id
		left join device_token dt on dt.user_id = u.id
		where c.camera_id = any(camera_ids_in)
		and u.role_id = 2 and u.expired_time is null
		group by cu.user_id, u.first_name, u.email, u.receive_email_notification
		union distinct
		select fu.user_id, u.first_name, u.email, u.receive_email_notification, array_agg(c.camera_id) as camera_ids, array_agg(dt.token) as tokens
		from camera c
		left join "group" fac on fac.id = c.group_id
		left join facility_user fu on fu.facility_id = fac.id
		left join "user" u on u.id = fu.user_id
		left join device_token dt on dt.user_id = u.id
		where c.camera_id = any(camera_ids_in)
		and u.role_id = 3 and u.expired_time is null
		group by fu.user_id, u.first_name, u.email, u.receive_email_notification
		union distinct
		select u.id as user_id, u.first_name, u.email, u.receive_email_notification, array_agg(c.camera_id) as camera_ids, array_agg(dt.token) as tokens
		from camera c
		left join "user" u on u.role_id = 1
		left join device_token dt on dt.user_id = u.id
		where c.camera_id = any(camera_ids_in)
		and u.expired_time is null
		group by u.id, u.first_name, u.email, u.receive_email_notification;
END

$$;


ALTER FUNCTION public.get_info_user_by_camera_ids_v1(camera_ids_in character varying[]) OWNER TO atollogy;

--
-- Name: get_switches_v2(integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_switches_v2(user_id_in integer, group_id_in integer) RETURNS TABLE(id integer, group_id integer, name character varying)
    LANGUAGE plpgsql
    AS $$

DECLARE
begin
	return query select 
		s.id, 
		s.group_id,
		s.name
	from switch as s
	where s.group_id = group_id_in;
END

$$;


ALTER FUNCTION public.get_switches_v2(user_id_in integer, group_id_in integer) OWNER TO atollogy;

--
-- Name: get_tokens_by_camera_v4(character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_tokens_by_camera_v4(camera_id_in character varying) RETURNS TABLE(id bigint, first_name character varying, email character varying, receive_email_notification boolean, tokens text[])
    LANGUAGE plpgsql
    AS $$

declare
	tokens text[];
	tokens_admins text[];
begin
	return query 
		select u.id, u.first_name, u.email, u.receive_email_notification, array_agg(dt.token) as tokens
		from camera c 
		left join "group" fac on fac.id = c.group_id
		left join "group" cus on cus.id = fac.parent_group_id
		left join customer_user cu on cu.customer_id = cus.id
		left join device_token dt on dt.user_id = cu.user_id
		left join "user" u on u.id = dt.user_id
		where c.camera_id = camera_id_in and c.expired_time is null and u.id is not null and u.expired_time is null
		group by u.id, u.first_name, u.email
		union DISTINCT
		select u.id, u.first_name, u.email, u.receive_email_notification, array_agg(dt.token) as tokens
		from "user" u 
		left join device_token dt on dt.user_id = u.id
		where u.role_id = 1 and u.expired_time is null
		group by u.id, u.first_name, u.email;
END

$$;


ALTER FUNCTION public.get_tokens_by_camera_v4(camera_id_in character varying) OWNER TO atollogy;

--
-- Name: get_tokens_by_email_v4(character varying[]); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_tokens_by_email_v4(emails_in character varying[]) RETURNS TABLE(id bigint, email character varying, first_name character varying, receive_email_notification boolean, tokens text[])
    LANGUAGE plpgsql
    AS $$

declare
	
begin
	return query
 		select u.id, u.email, u.first_name, u.receive_email_notification, array_agg(dt.token) as tokens
		from "user" u
		left join device_token dt on dt.user_id = u.id
		where u.email = any(emails_in) and u.expired_time is null
		group by u.id, u.email, u.first_name, u.receive_email_notification;
	
END

$$;


ALTER FUNCTION public.get_tokens_by_email_v4(emails_in character varying[]) OWNER TO atollogy;

--
-- Name: get_user_info_v1(integer, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_user_info_v1(user_id_ip integer, username_ip character varying, phone_number_in character varying, phone_country_code_in character varying) RETURNS TABLE(info json, username character varying, password character varying, role_id integer, account_type_id integer, phone_number character varying, phone_country_code character varying)
    LANGUAGE plpgsql
    AS $$

declare
    _user_block_info record;
		_restaurants json;
		_total_orders int4;
		_food_court json;
		_user record;
BEGIN
	SELECT us.* INTO _user from "user" us 
	WHERE us.id = user_id_ip or us.username = username_ip or (us.phone_number = phone_number_in AND us.phone_country_code = phone_country_code_in);

	RETURN QUERY
		select to_json(u.*) as info, 
			u.username,
			u."password",
			u."role_id" as "role_id",
			u.account_type_id as account_type_id,
			u.phone_number,
			u.phone_country_code
		from "user" u 
		where u.id = _user.id;
END;

$$;


ALTER FUNCTION public.get_user_info_v1(user_id_ip integer, username_ip character varying, phone_number_in character varying, phone_country_code_in character varying) OWNER TO atollogy;

--
-- Name: get_user_info_v4(integer, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_user_info_v4(user_id_ip integer, username_ip character varying, phone_number_in character varying, phone_country_code_in character varying) RETURNS TABLE(info json, username character varying, password character varying, role_id integer, role_name character varying, account_type_id integer, phone_number character varying, phone_country_code character varying, receive_email_notification boolean, customers json)
    LANGUAGE plpgsql
    AS $$

declare
    _user_block_info record;
	_restaurants json;
	_total_orders int4;
	_food_court json;
	_user record;
	_customers json := null;
BEGIN
	SELECT us.* INTO _user 
	from "user" us 
	left join customer_user cu on cu.user_id = us.id
	WHERE (us.id = user_id_ip or us.username = username_ip or (us.phone_number = phone_number_in AND us.phone_country_code = phone_country_code_in)) and us.expired_time is null;

	if _user.role_id in (2, 3) then
		select concat('[', array_to_string(array_agg(row_to_json(c.*)), ','), ']')::json into _customers
		from get_customers_v3(_user.id::integer) c;
	end if;

	RETURN QUERY
		select to_json(u.*) as info, 
			u.username,
			u."password",
			u."role_id" as "role_id",
			ur.name as "role_name",
			u.account_type_id as account_type_id,
			u.phone_number,
			u.phone_country_code,
			u.receive_email_notification,
			_customers as customers
		from "user" u 
		left join user_role ur on ur.id = u.role_id
		where u.id = _user.id and u.expired_time is null;
END;

$$;


ALTER FUNCTION public.get_user_info_v4(user_id_ip integer, username_ip character varying, phone_number_in character varying, phone_country_code_in character varying) OWNER TO atollogy;

--
-- Name: get_user_info_v4(integer, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_user_info_v4(user_id_ip integer, username_ip character varying, phone_number_in character varying, phone_country_code_in character varying, api_key_in character varying) RETURNS TABLE(info json, username character varying, password character varying, role_id integer, role_name character varying, account_type_id integer, phone_number character varying, phone_country_code character varying, receive_email_notification boolean, customers json)
    LANGUAGE plpgsql
    AS $$

declare
    _user_block_info record;
	_restaurants json;
	_total_orders int4;
	_food_court json;
	_user record;
	_customers json := null;
	_api_key varchar;
BEGIN
	SELECT us.* INTO _user 
	from "user" us 
	left join customer_user cu on cu.user_id = us.id
	WHERE (us.id = user_id_ip or us.username = username_ip or (us.phone_number = phone_number_in AND us.phone_country_code = phone_country_code_in)) and us.expired_time is null;

	if api_key_in is not null then
		select sc.api_key into _api_key
		from api_key sc
		where sc.api_key = api_key_in and sc.expired_time > now()
		limit 1;
	
		if _api_key is null then
			raise exception 'API_KEY_INCORRECT';
		end if;
	end if;

	if _user.role_id in (2, 3) then
		select concat('[', array_to_string(array_agg(row_to_json(c.*)), ','), ']')::json into _customers
		from get_customers_v3(_user.id::integer) c;
	end if;

	RETURN QUERY
		select to_json(u.*) as info, 
			u.username,
			u."password",
			u."role_id" as "role_id",
			ur.name as "role_name",
			u.account_type_id as account_type_id,
			u.phone_number,
			u.phone_country_code,
			u.receive_email_notification,
			_customers as customers
		from "user" u 
		left join user_role ur on ur.id = u.role_id
		where u.id = _user.id and u.expired_time is null;
END;

$$;


ALTER FUNCTION public.get_user_info_v4(user_id_ip integer, username_ip character varying, phone_number_in character varying, phone_country_code_in character varying, api_key_in character varying) OWNER TO atollogy;

--
-- Name: get_user_info_v5(integer, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_user_info_v5(user_id_ip integer, username_ip character varying, phone_number_in character varying, phone_country_code_in character varying, api_key_in character varying) RETURNS TABLE(info json, username character varying, password character varying, role_id integer, role_name character varying, account_type_id integer, phone_number character varying, phone_country_code character varying, receive_email_notification boolean, customers json, last_workbench_criteria_selection character varying, last_mugshot_criteria_selection json)
    LANGUAGE plpgsql
    AS $$

declare
    _user_block_info record;
	_restaurants json;
	_total_orders int4;
	_food_court json;
	_user record;
	_customers json := null;
	_api_key varchar;
	_last_workbench_criteria_selection varchar;
	_last_mugshot_criteria_selection json;
BEGIN
	SELECT us.* INTO _user 
	from "user" us 
	left join customer_user cu on cu.user_id = us.id
	WHERE (us.id = user_id_ip or us.username = username_ip or (us.phone_number = phone_number_in AND us.phone_country_code = phone_country_code_in)) and us.expired_time is null;

	if api_key_in is not null then
		select sc.api_key into _api_key
		from api_key sc
		where sc.api_key = api_key_in and sc.expired_time > now()
		limit 1;
	
		if _api_key is null then
			raise exception 'API_KEY_INCORRECT';
		end if;
	end if;

	if _user.role_id in (2, 3, 5) then
		select concat('[', array_to_string(array_agg(row_to_json(c.*)), ','), ']')::json into _customers
		from get_customers_v4(_user.id::integer) c;
	end if;

	select query_url into _last_workbench_criteria_selection
	from user_workbench_criteria_selection
	where user_id = _user.id
	order by timestamp desc
	limit 1;

	select json_build_object('query_url', query_url, 'body', body) into _last_mugshot_criteria_selection
	from user_mugshot_criteria_selection
	where user_id = _user.id
	order by timestamp desc
	limit 1;

	RETURN QUERY
		select to_json(u.*) as info, 
			u.username,
			u."password",
			u."role_id" as "role_id",
			ur.name as "role_name",
			u.account_type_id as account_type_id,
			u.phone_number,
			u.phone_country_code,
			u.receive_email_notification,
			_customers as customers,
			_last_workbench_criteria_selection as last_workbench_criteria_selection,
			_last_mugshot_criteria_selection as last_mugshot_criteria_selection
		from "user" u 
		left join user_role ur on ur.id = u.role_id
		where u.id = _user.id and u.expired_time is null;
END;

$$;


ALTER FUNCTION public.get_user_info_v5(user_id_ip integer, username_ip character varying, phone_number_in character varying, phone_country_code_in character varying, api_key_in character varying) OWNER TO atollogy;

--
-- Name: get_user_recieve_service_error_v3(); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_user_recieve_service_error_v3() RETURNS TABLE(id bigint, email character varying, first_name character varying)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_current_customer record;
begin
	return query 
		select u.id, u.email, u.first_name
	from admin_user_setting au
	left join "user" u on u.id = au.user_id
	where receive_service_error_notification = true and (u.expired_time is null or u.expired_time > now());
END

$$;


ALTER FUNCTION public.get_user_recieve_service_error_v3() OWNER TO atollogy;

--
-- Name: get_users_v4(integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.get_users_v4(user_id_in integer) RETURNS TABLE(id bigint, username character varying, status integer, first_name character varying, last_name character varying, photo character varying, role_id integer, role_name character varying, account_type_id integer, account_type_name character varying, phone_country_code character varying, customers json, created_time timestamp without time zone, phone_number character varying)
    LANGUAGE plpgsql
    AS $$

declare
    _user record;
BEGIN
	select u.* into _user from "user" u where u.id = user_id_in;

	if _user.role_id != 1 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;

	return query
		select u.id, u.username, u.status, u.first_name, u.last_name, u.photo, u.role_id, ur.name as role_name, u.account_type_id, ac.name as account_type_name, u.phone_country_code,
		(
			select concat('[', array_to_string(array_agg(row_to_json(r.*)), ','), ']')::json 
			from get_customers_v4(u.id::integer) r
		) as customers,
		u.created_time,
		u.phone_number
		from "user" u
		left join user_role ur on ur.id = u.role_id
		left join account_type ac on ac.id = u.account_type_id
		left join customer_user cu on cu.user_id = u.id
		left join "group" g on g.id = cu.customer_id
		where u.expired_time is null and u.id != user_id_in
		order by u.email asc;
END;

$$;


ALTER FUNCTION public.get_users_v4(user_id_in integer) OWNER TO atollogy;

--
-- Name: import_image_step_v4(integer, integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.import_image_step_v4(user_id_in integer, camera_id_in integer, data_in json) RETURNS TABLE(id integer, name character varying, box_name character varying, region_filter boolean, brightness_track boolean, annotate_after_filter boolean, opacity numeric, camera_id bigint, photo character varying, box_x_position integer, box_y_position integer, box_width integer, box_height integer, image_step json, index integer, config jsonb, enable boolean)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_role_id integer;
	_image_step json;
	_camera record;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;

	select c.* into _camera  from camera c where c.id = camera_id_in;
	
	update camera_image_step cis set expired_time = current_timestamp
	where cis.camera_id = camera_id_in and cis.image_step_id in (1, 2, 7, 8, 9, 10);


	for _image_step in select * from json_array_elements(data_in)
	loop
		insert into camera_image_step(
			camera_id,
			"name",
			image_step_id, 
			photo,
			region_filter,
			box_name,
			box_x_position,
			box_y_position,
			box_width,
			box_height,
			opacity,
			brightness_track,
			annotate_after_filter,
			created_by,
			"index",
			config,
			"enable",
			original_data
		)
		
		select
			camera_id_in,
			"_image_step" ->> 'name',
			("_image_step" ->> 'imageStepId')::integer,
			coalesce("_image_step" ->> 'photo', _camera.photo),
			("_image_step" ->> 'regionFilter')::bool,
			"_image_step" ->> 'boxName',
			case when ("_image_step" ->> 'imageStepId')::integer = 6 then null
				else ("_image_step" ->> 'boxXPosition')::integer
			end,
			case when ("_image_step" ->> 'imageStepId')::integer = 6 then null
				else ("_image_step" ->> 'boxYPosition')::integer
			end,
			case when ("_image_step" ->> 'imageStepId')::integer = 6 then null
				else ("_image_step" ->> 'boxWidth')::integer
			end,
			case when ("_image_step" ->> 'imageStepId')::integer = 6 then null
				else ("_image_step" ->> 'boxHeight')::integer
			end,
			("_image_step" ->> 'opacity')::numeric,
			case when ("_image_step" ->> 'imageStepId')::integer = 1 then ("_image_step" ->> 'brightnessTrack')::bool
				else null
			end,
			case when ("_image_step" ->> 'imageStepId')::integer = 1 then true 
				when ("_image_step" ->> 'imageStepId')::integer = 2 then false
				else null
			end,
			user_id_in,
			("_image_step" ->> 'index')::integer,
			("_image_step" ->> 'config')::jsonb,
			("_image_step" ->> 'enable')::bool,
			("_image_step" ->> 'originalData')::jsonb;
	end loop;

	return query
		select cis.id, cis.name, cis.box_name, cis.region_filter, cis.brightness_track, cis.annotate_after_filter, cis.opacity, cis.camera_id, cis.photo, cis.box_x_position, cis.box_y_position, cis.box_width, cis.box_height, json_build_object('id', ci.id, 'name', ci.name, 'imageStepId' ,ci.image_step_id) as image_step, cis."index", cis.config, cis."enable"
		from camera_image_step cis
		left join image_step ci on ci.id = cis.image_step_id
		where cis.camera_id = camera_id_in and cis.expired_time is null
		order by cis."index" asc;
END
$$;


ALTER FUNCTION public.import_image_step_v4(user_id_in integer, camera_id_in integer, data_in json) OWNER TO atollogy;

--
-- Name: import_image_step_v4(integer, integer, json, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.import_image_step_v4(user_id_in integer, camera_id_in integer, image_step_in json, edge_config_in json) RETURNS TABLE(id integer, name character varying, ip_address character varying, photo character varying, camera_id character varying, image_updated_time timestamp without time zone, camera_node json, step_name character varying, interval_time integer, firmware_version character varying, switch json, port integer, power_source json, connected_mode json, location character varying, notes text, image_activities json, image_installations character varying[], facility json, customer json, image_width integer, image_height integer, updated_time timestamp without time zone, is_config_published boolean, online_window_time integer, edge_configs json)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_role_id integer;
	_image_step json;
	_edge_config json;
	_camera record;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;

	select c.* into _camera  from camera c where c.id = camera_id_in;
	
	update camera_image_step cis set expired_time = current_timestamp
	where cis.camera_id = camera_id_in;

	update camera_edge_config cec set expired_time = current_timestamp
	where cec.camera_id = camera_id_in;

	update edge_config_step ecs set expired_time = current_timestamp
	where ecs.camera_edge_config_id in (
		select cec.id
		from camera_edge_config cec
		where cec.camera_id = camera_id_in
	);

	for _image_step in select * from json_array_elements(image_step_in)
	loop
		insert into camera_image_step(
			camera_id,
			"name",
			image_step_id, 
			photo,
			region_filter,
			box_name,
			box_x_position,
			box_y_position,
			box_width,
			box_height,
			opacity,
			brightness_track,
			annotate_after_filter,
			created_by,
			"index",
			config,
			"enable",
			original_data,
			subjects,
			brightness,
			input_image_indexes,
			input_step,
			color_space,
			regions,
			input_crop,
			image_complexity_threshold
		)
		
		select
			camera_id_in,
			"_image_step" ->> 'name',
			("_image_step" ->> 'imageStepId')::integer,
			coalesce("_image_step" ->> 'photo', _camera.photo),
			("_image_step" ->> 'regionFilter')::bool,
			"_image_step" ->> 'boxName',
			case when ("_image_step" ->> 'imageStepId')::integer = 6 then null
				else ("_image_step" ->> 'boxXPosition')::integer
			end,
			case when ("_image_step" ->> 'imageStepId')::integer = 6 then null
				else ("_image_step" ->> 'boxYPosition')::integer
			end,
			case when ("_image_step" ->> 'imageStepId')::integer = 6 then null
				else ("_image_step" ->> 'boxWidth')::integer
			end,
			case when ("_image_step" ->> 'imageStepId')::integer = 6 then null
				else ("_image_step" ->> 'boxHeight')::integer
			end,
			("_image_step" ->> 'opacity')::numeric,
			case when ("_image_step" ->> 'imageStepId')::integer = 1 then ("_image_step" ->> 'brightnessTrack')::bool
				else null
			end,
			case when ("_image_step" ->> 'imageStepId')::integer = 1 then true 
				when ("_image_step" ->> 'imageStepId')::integer = 2 then false
				else null
			end,
			user_id_in,
			("_image_step" ->> 'index')::integer,
			("_image_step" ->> 'config')::jsonb,
			("_image_step" ->> 'enable')::bool,
			("_image_step" ->> 'originalData')::jsonb,
			("_image_step" ->> 'subjects')::jsonb,
			("_image_step" ->> 'brightness')::jsonb,
			("_image_step" ->> 'inputImageIndexes')::jsonb,
			("_image_step" ->> 'inputStep')::varchar,
			("_image_step" ->> 'colorSpace')::varchar,
			("_image_step" ->> 'regions')::jsonb,
			("_image_step" ->> 'inputCrop')::jsonb,
			("_image_step" ->> 'imageComplexityThreshold')::integer;
	end loop;

	for _edge_config in select * from json_array_elements(edge_config_in)
	loop
		perform add_edge_config_v4(user_id_in, camera_id_in, _edge_config);
	end loop;

	update camera c set is_config_published = true where c.id = camera_id_in;

	return query
		select c.id, 
		c.name, 
		c.ip_address, 
		c.photo,
		c.camera_id, 
		c.image_updated_time, 
		c.camera_node, 
		c.step_name, 
		c.interval_time, 
		c.firmware_version, 
		c.switch, 
		c.port, 
		c.power_source, 
		c.connected_mode, 
		c."location", 
		c.notes,
		c.image_activities,
		c.image_installations,
		c.facility,
		c.customer,
		c.image_width,
		c.image_height,
		c.updated_time,
		c.is_config_published,
		c.online_window_time,
		c.edge_configs
		from get_camera_v4(user_id_in, camera_id_in) c;
END
$$;


ALTER FUNCTION public.import_image_step_v4(user_id_in integer, camera_id_in integer, image_step_in json, edge_config_in json) OWNER TO atollogy;

--
-- Name: import_report_type_v4(integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.import_report_type_v4(user_id_in integer, data_in json) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

declare
	_count_facility integer := 0;
	_facility json;
	_facility_record record;

	_count_report_type integer := 0;
	_report_type json;

	_report_type_record record;

begin
	loop
		exit when _count_facility = json_array_length(data_in);
    		_facility := data_in -> _count_facility;
    	
    		select g.* into _facility_record from "group" g where g.expired_time is null and g.group_type_id = 2 and g.alias_name = _facility ->> 'aliasName';
    
    		if _facility_record.id is not null then
	    		_count_report_type := 0;
			loop
				exit when _count_report_type = json_array_length(_facility -> 'reportTypes');
				_report_type := _facility -> 'reportTypes' -> _count_report_type;
				
				select rt.* into _report_type_record from report_type rt where rt.alias_name = _report_type ->> 'aliasName';
				if _report_type_record.id is not null then
					update facility_report_type_map frtm set
						"check" = (_report_type ->> 'check')::bool,
						updated_by = user_id_in,
						updated_time = current_timestamp
					where frtm.facility_id = _facility_record.id and frtm.report_type_id = _report_type_record.id;
				end if;
			
				_count_report_type := _count_report_type + 1;
			end loop;
		end if;
		
		_count_facility := _count_facility + 1;
  	end loop;
	
	return true;
END
$$;


ALTER FUNCTION public.import_report_type_v4(user_id_in integer, data_in json) OWNER TO atollogy;

--
-- Name: link_device_v3(integer, character varying, text); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.link_device_v3(user_id_in integer, device_id_in character varying, token_in text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
declare 
begin
	delete from device_token where id in (
		select dv1.id from device_token dv1 
		left join "user" ui1 on ui1.id = dv1.user_id
		where dv1.token = token_in
	) or id in (
		select dv.id from device_token dv 
		left join "user" ui on ui.id = dv.user_id
		where dv.device_id = device_id_in
	);
	insert into device_token (token, user_id, device_id)
        values (token_in, user_id_in, device_id_in);
	return true;
end;

$$;


ALTER FUNCTION public.link_device_v3(user_id_in integer, device_id_in character varying, token_in text) OWNER TO atollogy;

--
-- Name: log_out_v3(integer, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.log_out_v3(user_id_in integer, device_id_in character varying DEFAULT NULL::character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
declare
	success bool;
begin
	delete from device_token
	where user_id = "user_id_in" and device_id = "device_id_in";
	return true;	
end;
$$;


ALTER FUNCTION public.log_out_v3(user_id_in integer, device_id_in character varying) OWNER TO atollogy;

--
-- Name: reset_camera_config_v3(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.reset_camera_config_v3(user_id_in integer, camera_id_in integer, config_type_in integer) RETURNS TABLE(id integer, name character varying, ip_address character varying, photo character varying, camera_id character varying, image_updated_time timestamp without time zone, camera_node json, step_name character varying, interval_time integer, firmware_version character varying, switch json, port integer, power_source json, connected_mode json, location character varying, notes text, image_activities json, image_installations character varying[], facility json, customer json, image_width integer, image_height integer, updated_time timestamp without time zone, is_config_published boolean, online_window_time integer)
    LANGUAGE plpgsql
    AS $$
DECLARE
	_current_camera_id integer;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	select c.id into _current_camera_id 
	from camera c 
	where c.id = camera_id_in and (c.expired_time is null or c.expired_time > now());
	
	if _current_camera_id is null then
		raise exception 'CAMERA_DOES_NOT_EXIST';
	end if;
	
	update camera_image_step cis
	set expired_time = current_timestamp
	where cis.camera_id = camera_id_in and cis.image_step_id in (
		select i.id
		from image_config_type ict
		left join image_step i on i.image_config_type_id = ict.id
		where ict.id = config_type_in
	);
	
	return query
		select c.id, 
		c.name, 
		c.ip_address, 
		c.photo,
		c.camera_id, 
		c.image_updated_time, 
		c.camera_node, 
		c.step_name, 
		c.interval_time, 
		c.firmware_version, 
		c.switch, 
		c.port, 
		c.power_source, 
		c.connected_mode, 
		c."location", 
		c.notes,
		c.image_activities,
		c.image_installations,
		c.facility,
		c.customer,
		c.image_width,
		c.image_height,
		c.updated_time,
		c.is_config_published,
		c.online_window_time
		from get_camera_v4(user_id_in, camera_id_in) c;
END

$$;


ALTER FUNCTION public.reset_camera_config_v3(user_id_in integer, camera_id_in integer, config_type_in integer) OWNER TO atollogy;

--
-- Name: reset_camera_config_v4(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.reset_camera_config_v4(user_id_in integer, camera_id_in integer, config_type_in integer) RETURNS TABLE(id integer, name character varying, ip_address character varying, photo character varying, camera_id character varying, image_updated_time timestamp without time zone, camera_node json, step_name character varying, interval_time integer, firmware_version character varying, switch json, port integer, power_source json, connected_mode json, location character varying, notes text, image_activities json, image_installations character varying[], facility json, customer json, image_width integer, image_height integer, updated_time timestamp without time zone, is_config_published boolean, online_window_time integer, edge_configs json)
    LANGUAGE plpgsql
    AS $$
DECLARE
	_current_camera_id integer;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	select c.id into _current_camera_id 
	from camera c 
	where c.id = camera_id_in and (c.expired_time is null or c.expired_time > now());
	
	if _current_camera_id is null then
		raise exception 'CAMERA_DOES_NOT_EXIST';
	end if;

	if config_type_in = 2 then
		update camera_image_step cis
		set expired_time = current_timestamp
		where cis.camera_id = camera_id_in;
	else 
		update camera_edge_config cec
		set expired_time = current_timestamp, updated_by = user_id_in
		where cec.camera_id = camera_id_in;
	
		update edge_config_step ecs 
		set expired_time = current_timestamp, updated_by = user_id_in
		where ecs.camera_edge_config_id in (
			select cec.id
			from camera_edge_config cec
			where cec.camera_id = camera_id_in
		);
	end if;
	
	
	return query
		select c.id, 
		c.name, 
		c.ip_address, 
		c.photo,
		c.camera_id, 
		c.image_updated_time, 
		c.camera_node, 
		c.step_name, 
		c.interval_time, 
		c.firmware_version, 
		c.switch, 
		c.port, 
		c.power_source, 
		c.connected_mode, 
		c."location", 
		c.notes,
		c.image_activities,
		c.image_installations,
		c.facility,
		c.customer,
		c.image_width,
		c.image_height,
		c.updated_time,
		c.is_config_published,
		c.online_window_time,
		c.edge_configs
		from get_camera_v4(user_id_in, camera_id_in) c;
END

$$;


ALTER FUNCTION public.reset_camera_config_v4(user_id_in integer, camera_id_in integer, config_type_in integer) OWNER TO atollogy;

--
-- Name: update_camera_getway_id_v5(integer, character varying, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_camera_getway_id_v5(user_id_in integer, customer_id_in character varying, data_in json) RETURNS TABLE(camera json)
    LANGUAGE plpgsql
    AS $$
DECLARE
	_role_id integer;
	_customer record;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	update camera c set
		gateway_id = r.gateway_id
	from (
		select *
		from json_to_recordset(data_in)
		as x("gateway_id" varchar, "cam_name" varchar)
	) r
	where c.camera_id = r.cam_name;

	select * into _customer
	from "group" g
	where g.group_type_id = 2 and g.expired_time is null and g.alias_name = customer_id_in;

	return query
		with cameras as (
			select c.id as camera_id, c.name as camera_name, c.camera_id as camera_alias_name, c.gateway_id, (
				select concat('[', array_to_string(array_agg(row_to_json(d.*)), ','), ']')::text from get_image_step_v4(null, c.id) d
			) as image_steps_camera
			from camera c
			where c.expired_time is null and c.group_id = _customer.id
		), images_steps as (
			select c.*, concat('[', array_to_string(array_agg(row_to_json(cis.*)), ','), ']')::text as camera_image_steps
			from cameras c
			left join camera_image_step cis on cis.camera_id = c.camera_id and cis.expired_time is null
			group by c.camera_id, c.camera_name, c.camera_alias_name, c.gateway_id, c.image_steps_camera
			order by c.camera_id asc
		)
		select row_to_json(c.*) as camera
		from images_steps c;
END

$$;


ALTER FUNCTION public.update_camera_getway_id_v5(user_id_in integer, customer_id_in character varying, data_in json) OWNER TO atollogy;

--
-- Name: update_camera_getway_id_v5(integer, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_camera_getway_id_v5(user_id_in integer, customer_id_in character varying, camera_id_in character varying, gateway_id_in character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	update camera c set
		gateway_id = gateway_id_in,
		updated_by = user_id_in,
		updated_time = current_timestamp
	where c.id = (
		select ca.id
		from camera ca
		left join "group" cgr on cgr.id = ca.group_id
		where ca.camera_id = camera_id_in and cgr.alias_name = customer_id_in
	);
	
	return true;
END

$$;


ALTER FUNCTION public.update_camera_getway_id_v5(user_id_in integer, customer_id_in character varying, camera_id_in character varying, gateway_id_in character varying) OWNER TO atollogy;

--
-- Name: update_camera_getway_ids_v5(integer, character varying, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_camera_getway_ids_v5(user_id_in integer, customer_id_in character varying, data_in json) RETURNS TABLE(camera json)
    LANGUAGE plpgsql
    AS $$
DECLARE
	_role_id integer;
	_customer record;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	update camera c set
		gateway_id = r.gateway_id
	from (
		select *
		from json_to_recordset(data_in)
		as x("gateway_id" varchar, "cam_name" varchar)
	) r
	where c.camera_id = r.cam_name;

	select * into _customer
	from "group" g
	where g.group_type_id = 2 and g.expired_time is null and g.alias_name = customer_id_in;

	return query
		with cameras as (
			select c.id as camera_id, c.name as camera_name, c.camera_id as camera_alias_name, c.gateway_id, (
				select concat('[', array_to_string(array_agg(row_to_json(d.*)), ','), ']')::text from get_image_step_v4(null, c.id) d
			) as image_steps_camera
			from camera c
			where c.expired_time is null and c.group_id = _customer.id
		), images_steps as (
			select c.*, concat('[', array_to_string(array_agg(row_to_json(cis.*)), ','), ']')::text as camera_image_steps
			from cameras c
			left join camera_image_step cis on cis.camera_id = c.camera_id and cis.expired_time is null
			group by c.camera_id, c.camera_name, c.camera_alias_name, c.gateway_id, c.image_steps_camera
			order by c.camera_id asc
		)
		select row_to_json(c.*) as camera
		from images_steps c;
END

$$;


ALTER FUNCTION public.update_camera_getway_ids_v5(user_id_in integer, customer_id_in character varying, data_in json) OWNER TO atollogy;

--
-- Name: update_camera_image_v6(integer, integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_camera_image_v6(user_id_in integer, camera_id_in integer, truth_image_in character varying, current_image_in character varying) RETURNS TABLE(camera_id integer, current_image character varying, truth_image character varying)
    LANGUAGE plpgsql
    AS $$
declare
    _camera_image record;
begin
	select ci.* into _camera_image
	from camera_image ci
	where ci.camera_id = camera_id_in;
	
	if _camera_image.camera_id is null then
		insert into camera_image(camera_id, truth_image, current_image, truth_image_expired, current_image_expired, updated_by, updated_time)
		values(camera_id_in, truth_image_in, current_image_in, current_timestamp, current_timestamp, user_id_in, current_timestamp)
		returning * into _camera_image;
	else
		update camera_image ci set
			current_image = coalesce(current_image_in, ci.current_image),
			current_image_expired = coalesce(current_timestamp, ci.current_image_expired),
			truth_image = coalesce(truth_image_in, ci.truth_image),
			truth_image_expired = coalesce(current_timestamp, ci.truth_image_expired),
			updated_by = user_id_in,
			updated_time = current_timestamp 
		where ci.camera_id = camera_id_in;
	end if;
	return query
		select ci.camera_id, ci.current_image, ci.truth_image 
		from camera_image ci 
		where ci.camera_id = camera_id_in;
	
END
$$;


ALTER FUNCTION public.update_camera_image_v6(user_id_in integer, camera_id_in integer, truth_image_in character varying, current_image_in character varying) OWNER TO atollogy;

--
-- Name: update_camera_v2(integer, integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_camera_v2(user_id_in integer, camera_id_in integer, data_in json) RETURNS TABLE(id integer, name character varying, ip_address character varying, photo character varying, camera_id character varying, image_updated_time timestamp without time zone, camera_node json, step_name character varying, interval_time integer, firmware_version character varying, switch json, port integer, power_source json, connected_mode json, location character varying, notes text, image_activities json, image_installations character varying[], facility json, customer json, image_width integer, image_height integer, updated_time timestamp without time zone, is_config_published boolean, online_window_time integer)
    LANGUAGE plpgsql
    AS $$
DECLARE
	_camera record;
	_current_camera_id integer;
begin
	select c.id into _current_camera_id 
	from camera c 
	where c.id = camera_id_in and (c.expired_time is null or c.expired_time > now());
	
	if _current_camera_id is null then
		raise exception 'CAMERA_DOES_NOT_EXIST';
	end if;
	
	select c.* into _camera 
	from camera c 
	where c.id <> camera_id_in and (c.expired_time is null or c.expired_time > now()) and (c.name = data_in ->> 'name')
	limit 1;

	case when (_camera.id is not null) AND (_camera.name = data_in ->> 'name') and (_camera.id != camera_id_in) THEN
		raise exception 'CAMERA_EXISTED';
	else end case;
	
	update camera c set
		name = coalesce((data_in ->> 'name')::varchar, c.name),
		ip_address = coalesce(data_in ->> 'ipAddress', c.ip_address),
		camera_id = coalesce(data_in ->> 'cameraId', c.camera_id),
		camera_node_id = coalesce((data_in ->> 'cameraNodeId')::int8, c.camera_node_id),
		step_name = coalesce(data_in ->> 'stepName', c.step_name),
		interval_time = coalesce((data_in ->> 'intervalTime')::integer, c.interval_time),
		firmware_version = coalesce(data_in ->> 'firmwareVersion', c.firmware_version),
		switch_id = coalesce((data_in ->> 'switchId')::integer, c.switch_id),
		port = coalesce((data_in ->> 'port')::integer, c.port),
		power_source_id = coalesce((data_in ->> 'powerSourceId')::integer, c.power_source_id),
		connected_mode_id = coalesce((data_in ->> 'connectedModeId')::integer, c.connected_mode_id),
		"location" = coalesce(data_in ->> 'location', c."location"),
		notes = coalesce(data_in ->> 'notes', c.notes),
		photo = coalesce(data_in ->> 'photo', c.photo),
		alias_name = coalesce(lower(regexp_replace((data_in ->> 'name')::varchar, '\s', '', 'g')), c.alias_name),
		updated_by = user_id_in,
		updated_time = current_timestamp,
		image_width = coalesce((data_in ->> 'imageWidth')::integer, c.image_width),
		image_height = coalesce((data_in ->> 'imageHeight')::integer, c.image_height),
		is_config_published = coalesce((data_in ->> 'isConfigPublished')::bool, c.is_config_published)
	where c.id = camera_id_in;

	update "group" g
	set updated_by = user_id_in, updated_time = current_timestamp
	where g.id = (
		select cus.id
		from camera c
		left join "group" fac on fac.id = c.group_id
		left join "group" cus on cus.id = fac.parent_group_id
		where c.id = camera_id_in
	);
	
	return query
		select c.id, 
		c.name, 
		c.ip_address, 
		c.photo,
		c.camera_id, 
		c.image_updated_time, 
		c.camera_node, 
		c.step_name, 
		c.interval_time, 
		c.firmware_version, 
		c.switch, 
		c.port, 
		c.power_source, 
		c.connected_mode, 
		c."location", 
		c.notes,
		c.image_activities,
		c.image_installations,
		c.facility,
		c.customer,
		c.image_width,
		c.image_height,
		c.updated_time,
		c.is_config_published,
		c.online_window_time
		from get_camera_v4(user_id_in, camera_id_in) c;
END

$$;


ALTER FUNCTION public.update_camera_v2(user_id_in integer, camera_id_in integer, data_in json) OWNER TO atollogy;

--
-- Name: update_camera_v3(integer, integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_camera_v3(user_id_in integer, camera_id_in integer, data_in json) RETURNS TABLE(id integer, name character varying, ip_address character varying, photo character varying, camera_id character varying, image_updated_time timestamp without time zone, camera_node json, step_name character varying, interval_time integer, firmware_version character varying, switch json, port integer, power_source json, connected_mode json, location character varying, notes text, image_activities json, image_installations character varying[], facility json, customer json, image_width integer, image_height integer, updated_time timestamp without time zone, is_config_published boolean, online_window_time integer, edge_configs json)
    LANGUAGE plpgsql
    AS $$
DECLARE
	_camera record;
	_current_camera_id integer;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	select c.id into _current_camera_id 
	from camera c 
	where c.id = camera_id_in and (c.expired_time is null or c.expired_time > now());
	
	if _current_camera_id is null then
		raise exception 'CAMERA_DOES_NOT_EXIST';
	end if;
	
	select c.* into _camera 
	from camera c 
	where c.id <> camera_id_in and (c.expired_time is null or c.expired_time > now()) and (c.camera_id = data_in ->> 'cameraId')
	limit 1;

	case when (_camera.id is not null) AND (_camera.camera_id = data_in ->> 'cameraId') and (_camera.id != camera_id_in) THEN
		raise exception 'CAMERA_EXISTED';
	else end case;
	
	update camera c set
		name = coalesce((data_in ->> 'name')::varchar, c.name),
		ip_address = coalesce(data_in ->> 'ipAddress', c.ip_address),
		camera_id = coalesce(data_in ->> 'cameraId', c.camera_id),
		camera_node_id = coalesce((data_in ->> 'cameraNodeId')::int8, c.camera_node_id),
		step_name = coalesce(data_in ->> 'stepName', c.step_name),
		interval_time = coalesce((data_in ->> 'intervalTime')::integer, c.interval_time),
		firmware_version = coalesce(data_in ->> 'firmwareVersion', c.firmware_version),
		switch_id = coalesce((data_in ->> 'switchId')::integer, c.switch_id),
		port = coalesce((data_in ->> 'port')::integer, c.port),
		power_source_id = coalesce((data_in ->> 'powerSourceId')::integer, c.power_source_id),
		connected_mode_id = coalesce((data_in ->> 'connectedModeId')::integer, c.connected_mode_id),
		"location" = coalesce(data_in ->> 'location', c."location"),
		notes = coalesce(data_in ->> 'notes', c.notes),
		alias_name = coalesce(lower(regexp_replace((data_in ->> 'name')::varchar, '\s', '', 'g')), c.alias_name),
		updated_by = user_id_in,
		updated_time = current_timestamp,
		image_width = coalesce((data_in ->> 'imageWidth')::integer, c.image_width),
		image_height = coalesce((data_in ->> 'imageHeight')::integer, c.image_height),
		is_config_published = coalesce((data_in ->> 'isConfigPublished')::bool, c.is_config_published),
		online_window_time = coalesce((data_in ->> 'onlineWindowTime')::integer, c.online_window_time)
	where c.id = camera_id_in;

	update "group" g
	set updated_by = user_id_in, updated_time = current_timestamp
	where g.id = (
		select cus.id
		from camera c
		left join "group" fac on fac.id = c.group_id
		left join "group" cus on cus.id = fac.parent_group_id
		where c.id = camera_id_in
	);
	
	return query
		select c.id, 
		c.name, 
		c.ip_address, 
		c.photo,
		c.camera_id, 
		c.image_updated_time, 
		c.camera_node, 
		c.step_name, 
		c.interval_time, 
		c.firmware_version, 
		c.switch, 
		c.port, 
		c.power_source, 
		c.connected_mode, 
		c."location", 
		c.notes,
		c.image_activities,
		c.image_installations,
		c.facility,
		c.customer,
		c.image_width,
		c.image_height,
		c.updated_time,
		c.is_config_published,
		c.online_window_time,
		c.edge_configs
		from get_camera_v4(user_id_in, camera_id_in) c;
END

$$;


ALTER FUNCTION public.update_camera_v3(user_id_in integer, camera_id_in integer, data_in json) OWNER TO atollogy;

--
-- Name: update_customer_v1(integer, integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_customer_v1(user_id_in integer, id_in integer, name_in character varying, photo_in character varying) RETURNS TABLE(id integer, name character varying, photo character varying)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_customer record;
	_current_customer_id integer;
begin
	select cus.id into _current_customer_id from customer cus where cus.id = id_in;
	if _current_customer_id is null then
		RAISE EXCEPTION 'CUSTOMER_DOES_NOT_EXIST';
	end if;
	SELECT cus.* INTO _customer FROM "customer" cus WHERE cus.id <> id_in and cus.expired_time is null and (cus.name = name_in) LIMIT 1;
	CASE WHEN (_customer.id IS NOT NULL) AND (_customer.name = name_in) THEN
		RAISE EXCEPTION 'CUSTOMER_EXISTED';
	ELSE END CASE;
	
	update "customer" cus set
		"name" = coalesce(name_in, cus.name),
		"photo" = coalesce(photo_in, cus.photo),
		"updated_time" = now(),
		"updated_by" = user_id_in
		where cus.id = id_in;
	
	RETURN QUERY 
		SELECT cus."id",
			cus.name,
			cus.photo
		FROM "customer" cus   
		WHERE cus.expired_time is null order by name;
END

$$;


ALTER FUNCTION public.update_customer_v1(user_id_in integer, id_in integer, name_in character varying, photo_in character varying) OWNER TO atollogy;

--
-- Name: update_customer_v3(integer, integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_customer_v3(user_id_in integer, customer_id_in integer, name_in character varying, photo_in character varying) RETURNS TABLE(id integer, name character varying, alias_name character varying, photo character varying, status integer, updated_time timestamp without time zone, old_photo character varying)
    LANGUAGE plpgsql
    AS $$
DECLARE
	_customer record;
	_current_customer record;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 or _role_id = 2 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	select g.* into _current_customer
	from "group" g
	where g.id = customer_id_in and g.group_type_id = 1 and (g.expired_time is null or g.expired_time > now());

	if _current_customer.id is null then
		raise exception 'CUSTOMER_DOES_NOT_EXIST';
	end if;
	
	select g.* into _customer 
	from "group" g
	where g.id <> customer_id_in and (g.expired_time is null or g.expired_time > now()) and g.name = name_in and g.group_type_id = 1
	limit 1;

	case when (_customer.id is not null) then
		raise exception 'CUSTOMER_EXISTED' using detail = 'name=' || _customer.name;
	else end case;
	
	update "group" g set
		name = coalesce(name_in, g.name),
		photo = case
			when (photo_in = '') then null
			else coalesce(photo_in, g.photo)
		end,
		updated_time = current_timestamp,
		updated_by = user_id_in
	where g.id = customer_id_in;

	return query
		select g.id, g.name, g.alias_name, g.photo, g.status, g.updated_time, _current_customer.photo as old_photo
		from "group" g
		where g.id = customer_id_in;

	
END

$$;


ALTER FUNCTION public.update_customer_v3(user_id_in integer, customer_id_in integer, name_in character varying, photo_in character varying) OWNER TO atollogy;

--
-- Name: update_customer_v4(integer, integer, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_customer_v4(user_id_in integer, customer_id_in integer, name_in character varying, alias_name_in character varying, photo_in character varying) RETURNS TABLE(id integer, name character varying, alias_name character varying, photo character varying, status integer, updated_time timestamp without time zone, old_photo character varying)
    LANGUAGE plpgsql
    AS $$
DECLARE
	_customer record;
	_current_customer record;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 or _role_id = 2 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	select g.* into _current_customer
	from "group" g
	where g.id = customer_id_in and g.group_type_id = 1 and (g.expired_time is null or g.expired_time > now());

	if _current_customer.id is null then
		raise exception 'CUSTOMER_DOES_NOT_EXIST';
	end if;
	
	select g.* into _customer 
	from "group" g
	where g.id <> customer_id_in and g.expired_time is null and 
		(g.name = name_in or g.alias_name = alias_name_in) and g.group_type_id = 1
	limit 1;

	case when (_customer.id is not null) then
		raise exception 'CUSTOMER_EXISTED' using detail = 'name=' || _customer.name;
	else end case;
	
	update "group" g set
		name = coalesce(name_in, g.name),
		alias_name = coalesce(alias_name_in, g.alias_name),
		photo = case
			when (photo_in = '') then null
			else coalesce(photo_in, g.photo)
		end,
		updated_time = current_timestamp,
		updated_by = user_id_in
	where g.id = customer_id_in;

	return query
		select g.id, g.name, g.alias_name, g.photo, g.status, g.updated_time, _current_customer.photo as old_photo
		from "group" g
		where g.id = customer_id_in;

	
END

$$;


ALTER FUNCTION public.update_customer_v4(user_id_in integer, customer_id_in integer, name_in character varying, alias_name_in character varying, photo_in character varying) OWNER TO atollogy;

--
-- Name: update_edge_config_v4(integer, integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_edge_config_v4(user_id_in integer, edge_config_id integer, data_in json) RETURNS TABLE(id integer, name character varying, camera_id bigint, image_step json, photo character varying, orientation integer, resolution_type integer, resolution character varying, "interval" integer, exposure_time integer, duration integer, fps integer, image_collector boolean, optimal_brightness integer, threshold integer, capture_gap integer, capture_count integer, regions jsonb, iso integer, white_balance jsonb, steps json, hdr boolean)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_edge_config record;
	_role_id integer;
	_step_update json;
	_step_insert json;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;

	select cec.* into _edge_config from camera_edge_config cec where cec.id = edge_config_id;
	
	update camera_edge_config cec set
		image_step_id = coalesce(("data_in" ->> 'imageStepId')::integer, cec.image_step_id),
		photo = coalesce((data_in ->> 'photo')::varchar, cec.photo),
		updated_time = current_timestamp,
		updated_by = user_id_in,
		orientation = coalesce((data_in ->> 'orientation')::integer, null),
		resolution = coalesce((data_in ->> 'resolution')::varchar, null),
		"interval" = coalesce((data_in ->> 'interval')::integer, null),
		duration = coalesce((data_in ->> 'duration')::integer, null),
		fps = coalesce((data_in ->> 'fps')::integer, null),
		device_name = coalesce((data_in ->> 'deviceName')::varchar, cec.device_name),
		resolution_type = coalesce((data_in ->> 'resolutionType')::integer, cec.resolution_type),
		exposure_time = coalesce((data_in ->> 'exposureTime')::integer, null),
		"name" = coalesce((data_in ->> 'name')::varchar, cec.name),
		optimal_brightness = coalesce((data_in ->> 'optimalBrightness')::integer, null),
		threshold = coalesce((data_in ->> 'threshold')::integer, null),
		capture_gap = coalesce((data_in ->> 'captureGap')::integer, null),
		capture_count = coalesce((data_in ->> 'captureCount')::integer, null),
		regions = coalesce((data_in ->> 'regions')::jsonb, null),
		white_balance = coalesce((data_in ->> 'whiteBalance')::jsonb, null),
		iso = coalesce((data_in ->> 'iso')::integer, null),
		hdr = coalesce((data_in ->> 'hdr')::boolean, null)
	where cec.id = edge_config_id;
	

	update edge_config_step ecs set
		expired_time = current_timestamp,
		updated_by = user_id_in
	where ecs.camera_edge_config_id = edge_config_id 
		and ecs.id not in (
			select r.value::integer from json_array_elements_text(data_in -> 'stepUpdateIds') r
		);
		
	for _step_insert in select * from json_array_elements(data_in -> 'stepNews')
	loop
		insert into edge_config_step(
			camera_edge_config_id, 
			"name",
			created_by,
			image_aggregate,
			image_transform,
			parameter_crop,
			parameter_scale,
			parameter_location,
			parameter_radius,
			parameter_vthreshold
		)
		select 
			edge_config_id,
			(_step_insert ->> 'name')::varchar,
			user_id_in,
			(_step_insert ->> 'imageAggregate')::varchar,
			(_step_insert ->> 'imageTransform')::varchar,
			(_step_insert ->> 'crop')::jsonb,
			(_step_insert ->> 'scale')::float,
			(_step_insert ->> 'location')::jsonb,
			(_step_insert ->> 'radius')::float,
			(_step_insert ->> 'vThreshold')::float;
	end loop;
	
	for _step_update in select * from json_array_elements(data_in -> 'stepUpdates')
	loop
		update edge_config_step ecs set
			"name" = coalesce((_step_update ->> 'name')::varchar, ecs.name),
			updated_by = user_id_in,
			updated_time = current_timestamp,
			image_aggregate = coalesce((_step_update ->> 'imageAggregate')::varchar, ecs.image_aggregate),
			image_transform = coalesce((_step_update ->> 'imageTransform')::varchar, ecs.image_transform),
			parameter_crop = coalesce((_step_update ->> 'crop')::jsonb, null),
			parameter_scale = coalesce((_step_update ->> 'scale')::float, null),
			parameter_location = coalesce((_step_update ->> 'location')::jsonb, null),
			parameter_radius = coalesce((_step_update ->> 'radius')::float, null),
			parameter_vthreshold = coalesce((_step_update ->> 'vThreshold')::float, null)
		where ecs.id = (_step_update ->> 'id')::integer;
	end loop;

	update camera c 
	set status = 2, updated_by = user_id_in, updated_time = current_timestamp, is_config_published = false
	where c.id = _edge_config.camera_id;

	update "group" g
	set updated_by = user_id_in, updated_time = current_timestamp
	where g.id = (
		select cus.id
		from camera c
		left join "group" fac on fac.id = c.group_id
		left join "group" cus on cus.id = fac.parent_group_id
		where c.id = _edge_config.camera_id
	);

	return query select 
		g.id, g.name, g.camera_id, g.image_step, g.photo, g.orientation, g.resolution_type, g.resolution, g."interval", g.exposure_time, g.duration, g.fps, g.image_collector, g.optimal_brightness, g.threshold, g.capture_gap, g.capture_count, g.regions, g.iso, g.white_balance, g.steps, g.hdr
	from get_edge_config_v4(user_id_in, edge_config_id) g;
END
$$;


ALTER FUNCTION public.update_edge_config_v4(user_id_in integer, edge_config_id integer, data_in json) OWNER TO atollogy;

--
-- Name: update_enable_image_step_v4(integer, integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_enable_image_step_v4(user_id_in integer, image_step_id_id integer, data_in json) RETURNS TABLE(id integer, name character varying, image_step json, photo character varying, region_filter boolean, box_name character varying, box_x_position integer, box_y_position integer, box_width integer, box_height integer, opacity numeric, brightness_track boolean, annotate_after_filter boolean, index integer, config jsonb, enable boolean, subjects jsonb, brightness jsonb, input_image_indexes jsonb, input_step character varying, color_space character varying, regions jsonb, input_crop jsonb, image_complexity_threshold integer)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_image_step record;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	select * into _image_step from camera_image_step cis where cis.id = image_step_id_id;

	update camera_image_step c set
		"enable" = coalesce(("data_in" ->> 'enable')::bool, c."enable")
	where c.id = image_step_id_id;
		
	update camera c 
	set status = 2, updated_by = user_id_in, updated_time = current_timestamp, is_config_published = false
	where c.id = _image_step.camera_id;

	update "group" g
	set updated_by = user_id_in, updated_time = current_timestamp
	where g.id = (
		select cus.id
		from camera c
		left join "group" fac on fac.id = c.group_id
		left join "group" cus on cus.id = fac.parent_group_id
		where c.id = _image_step.camera_id
	);


	return query select
		cis.id,
		cis."name",
		json_build_object('id', ims.id, 'name', ims."name", 'imageStepId', ims.image_step_id) as image_step,
		cis.photo,
		cis.region_filter,
		cis.box_name,
		cis.box_x_position,
		cis.box_y_position,
		cis.box_width,
		cis.box_height,
		cis.opacity,
		cis.brightness_track,
		cis.annotate_after_filter,
		cis."index",
		cis.config,
		cis."enable",
		cis.subjects, 
		cis.brightness,
		cis.input_image_indexes, 
		cis.input_step,
		cis.color_space,
		cis.regions,
		cis.input_crop,
		cis.image_complexity_threshold
		from camera_image_step cis
		left join image_step ims on ims.id = cis.image_step_id
		where cis.id = _image_step.id;
END
$$;


ALTER FUNCTION public.update_enable_image_step_v4(user_id_in integer, image_step_id_id integer, data_in json) OWNER TO atollogy;

--
-- Name: update_facility_v1(integer, integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_facility_v1(user_id_in integer, id_in integer, name_in character varying, photo_in character varying) RETURNS TABLE(id integer, customer_id integer, name character varying, photo character varying)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_facility record;
	_current_facility record;
begin
	select fac.* into _current_facility from facility fac where fac.id = id_in;
	if _current_facility.id is null then
		RAISE EXCEPTION 'FACILITY_DOES_NOT_EXIST';
	end if;
	SELECT fac.* INTO _facility FROM "facility" fac WHERE fac.id <> id_in and fac.expired_time is null and (fac.name = name_in) and fac.customer_id = _current_facility.customer_id LIMIT 1;
	CASE WHEN (_facility.id IS NOT NULL) AND (_facility.name = name_in) THEN
		RAISE EXCEPTION 'FACILITY_EXISTED';
	ELSE END CASE;
	
	update "facility" fac set
		"name" = coalesce(name_in, fac.name),
		"photo" = coalesce(photo_in, fac.photo),
		"updated_time" = now(),
		"updated_by" = user_id_in
		where fac.id = id_in;
	
	RETURN QUERY 
		SELECT fac."id",
			fac.customer_id,
			fac.name,
			fac.photo
		FROM "facility" fac   
		WHERE fac.expired_time is null and fac.customer_id = _current_facility.customer_id order by name;
END

$$;


ALTER FUNCTION public.update_facility_v1(user_id_in integer, id_in integer, name_in character varying, photo_in character varying) OWNER TO atollogy;

--
-- Name: update_facility_v3(integer, integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_facility_v3(user_id_in integer, facility_id_in integer, name_in character varying, photo_in character varying) RETURNS TABLE(id integer, name character varying, photo character varying, status integer, updated_time timestamp without time zone, old_photo character varying)
    LANGUAGE plpgsql
    AS $$
DECLARE
	_facility record;
	_current_facility record;
begin
	select fac.* into _current_facility
	from "group" fac
	where fac.id = facility_id_in and fac.group_type_id = 2 and (fac.expired_time is null or fac.expired_time > now());

	if _current_facility.id is null then
		raise exception 'FACILITY_DOES_NOT_EXIST';
	end if;
	
	select fac.* into _facility 
	from "group" fac
	where fac.id <> facility_id_in and (fac.expired_time is null or fac.expired_time > now()) and (lower(fac.alias_name) = lower(regexp_replace(name_in, '\s', '', 'g'))) and fac.group_type_id = 2
	limit 1;

	case when (_facility.id is not null) then
		raise exception 'FACILITY_EXISTED' using detail = 'name=' || _customer.name;
	else end case;
	
	update "group" fac set
		name = coalesce(name_in, fac.name),
		photo = case
			when (photo_in = '') then null
			else coalesce(photo_in, fac.photo)
		end,
		alias_name = coalesce(regexp_replace(name_in, '\s', '', 'g'), fac.alias_name),
		updated_time = current_timestamp,
		updated_by = user_id_in
	where fac.id = facility_id_in;

	return query
		select fac.id, fac.name, fac.photo, fac.status, fac.updated_time, _current_facility.photo as old_photo
		from "group" fac
		where fac.id = facility_id_in;

END

$$;


ALTER FUNCTION public.update_facility_v3(user_id_in integer, facility_id_in integer, name_in character varying, photo_in character varying) OWNER TO atollogy;

--
-- Name: update_facility_v4(integer, integer, character varying, character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_facility_v4(user_id_in integer, facility_id_in integer, name_in character varying, photo_in character varying, raw_offset_in integer, dst_offset_in integer) RETURNS TABLE(id integer, name character varying, alias_name character varying, photo character varying, status integer, updated_time timestamp without time zone, old_photo character varying, raw_offset integer, dst_offset integer, customer_id integer)
    LANGUAGE plpgsql
    AS $$
DECLARE
	_facility record;
	_current_facility record;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	select fac.* into _current_facility
	from "group" fac
	where fac.id = facility_id_in and fac.group_type_id = 2 and (fac.expired_time is null or fac.expired_time > now());

	if _current_facility.id is null then
		raise exception 'FACILITY_DOES_NOT_EXIST';
	end if;
	
	select fac.* into _facility 
	from "group" fac
	where fac.id <> facility_id_in and (fac.expired_time is null or fac.expired_time > now()) and (lower(fac.alias_name) = lower(regexp_replace(name_in, '\s', '', 'g'))) and fac.group_type_id = 2 and fac.parent_group_id = _current_facility.parent_group_id
	limit 1;

	case when (_facility.id is not null) then
		raise exception 'FACILITY_EXISTED' using detail = 'name=' || _facility.name;
	else end case;
	
	update "group" fac set
		name = coalesce(name_in, fac.name),
		photo = case
			when (photo_in = '') then null
			else coalesce(photo_in, fac.photo)
		end,
		updated_time = current_timestamp,
		updated_by = user_id_in,
		raw_offset = coalesce(raw_offset_in, fac.raw_offset),
		dst_offset = coalesce(dst_offset_in, fac.dst_offset)
	where fac.id = facility_id_in;

	return query
		select fac.id, fac.name, fac.alias_name, fac.photo, fac.status, fac.updated_time, _current_facility.photo as old_photo, fac.raw_offset, fac.dst_offset, fac.parent_group_id as customer_id
		from "group" fac
		where fac.id = facility_id_in;

END

$$;


ALTER FUNCTION public.update_facility_v4(user_id_in integer, facility_id_in integer, name_in character varying, photo_in character varying, raw_offset_in integer, dst_offset_in integer) OWNER TO atollogy;

--
-- Name: update_facility_v4(integer, integer, character varying, character varying, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_facility_v4(user_id_in integer, facility_id_in integer, name_in character varying, photo_in character varying, raw_offset_in integer, dst_offset_in integer, customer_id_in integer) RETURNS TABLE(id integer, name character varying, alias_name character varying, photo character varying, status integer, updated_time timestamp without time zone, old_photo character varying, raw_offset integer, dst_offset integer, customer_id integer)
    LANGUAGE plpgsql
    AS $$
declare
	_facility_existed record;
	_facility record;
	_customer record;
	_current_facility record;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	select fac.* into _current_facility
	from "group" fac
	where fac.id = facility_id_in and fac.group_type_id = 2 and (fac.expired_time is null or fac.expired_time > now());

	if _current_facility.id is null then
		raise exception 'FACILITY_DOES_NOT_EXIST';
	end if;

	if customer_id_in is not null and customer_id_in <> _current_facility.parent_group_id then
		select cus.* into _customer
		from "group" cus
		where cus.id = customer_id_in and cus.group_type_id = 1 and cus.expired_time is null;
	
		if  _customer.id is null then
			raise exception 'CUSTOMER_DOES_NOT_EXIST';
		end if;
	
		select fac.* into _facility_existed
		from "group" fac
		where fac.parent_group_id = customer_id_in and fac.alias_name = _current_facility.alias_name and fac.expired_time is null;
		
		if _facility_existed.id is not null then
			raise exception 'FACILITY_EXISTED' using detail = 'name=' || _facility_existed.name;
		end if;
	end if;
	
	select fac.* into _facility 
	from "group" fac
	where fac.id <> facility_id_in and (fac.expired_time is null or fac.expired_time > now()) and (lower(fac.alias_name) = lower(regexp_replace(name_in, '\s', '', 'g'))) and fac.group_type_id = 2 and fac.parent_group_id = _current_facility.parent_group_id
	limit 1;

	case when (_facility.id is not null) then
		raise exception 'FACILITY_EXISTED' using detail = 'name=' || _facility.name;
	else end case;
	
	update "group" fac set
		name = coalesce(name_in, fac.name),
		photo = case
			when (photo_in = '') then null
			else coalesce(photo_in, fac.photo)
		end,
		updated_time = current_timestamp,
		updated_by = user_id_in,
		raw_offset = coalesce(raw_offset_in, fac.raw_offset),
		dst_offset = coalesce(dst_offset_in, fac.dst_offset),
		parent_group_id = coalesce(customer_id_in, fac.parent_group_id)
	where fac.id = facility_id_in;

	return query
		select fac.id, fac.name, fac.alias_name, fac.photo, fac.status, fac.updated_time, _current_facility.photo as old_photo, fac.raw_offset, fac.dst_offset, fac.parent_group_id as customer_id
		from "group" fac
		where fac.id = facility_id_in;

END

$$;


ALTER FUNCTION public.update_facility_v4(user_id_in integer, facility_id_in integer, name_in character varying, photo_in character varying, raw_offset_in integer, dst_offset_in integer, customer_id_in integer) OWNER TO atollogy;

--
-- Name: update_facility_v4(integer, integer, character varying, character varying, integer, integer, integer, integer[]); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_facility_v4(user_id_in integer, facility_id_in integer, name_in character varying, photo_in character varying, raw_offset_in integer, dst_offset_in integer, customer_id_in integer, report_type_ids_in integer[]) RETURNS TABLE(id integer, name character varying, alias_name character varying, photo character varying, status integer, updated_time timestamp without time zone, old_photo character varying, raw_offset integer, dst_offset integer, customer_id integer, report_types json)
    LANGUAGE plpgsql
    AS $$
declare
	_facility_existed record;
	_facility record;
	_customer record;
	_current_facility record;
	_role_id integer;
begin
	
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;	
	if report_type_ids_in is not null and _role_id <> 1 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;

	select fac.* into _current_facility
	from "group" fac
	where fac.id = facility_id_in and fac.group_type_id = 2 and (fac.expired_time is null or fac.expired_time > now());

	if _current_facility.id is null then
		raise exception 'FACILITY_DOES_NOT_EXIST';
	end if;

	if customer_id_in is not null and customer_id_in <> _current_facility.parent_group_id then
		select cus.* into _customer
		from "group" cus
		where cus.id = customer_id_in and cus.group_type_id = 1 and cus.expired_time is null;
	
		if  _customer.id is null then
			raise exception 'CUSTOMER_DOES_NOT_EXIST';
		end if;
	
		select fac.* into _facility_existed
		from "group" fac
		where fac.parent_group_id = customer_id_in and fac.alias_name = _current_facility.alias_name and fac.expired_time is null;
		
		if _facility_existed.id is not null then
			raise exception 'FACILITY_EXISTED' using detail = 'name=' || _facility_existed.name;
		end if;
	end if;
	
	select fac.* into _facility 
	from "group" fac
	where fac.id <> facility_id_in and (fac.expired_time is null or fac.expired_time > now()) and (lower(fac.alias_name) = lower(regexp_replace(name_in, '\s', '', 'g'))) and fac.group_type_id = 2 and fac.parent_group_id = _current_facility.parent_group_id
	limit 1;

	case when (_facility.id is not null) then
		raise exception 'FACILITY_EXISTED' using detail = 'name=' || _facility.name;
	else end case;
	
	update "group" fac set
		name = coalesce(name_in, fac.name),
		photo = case
			when (photo_in = '') then null
			else coalesce(photo_in, fac.photo)
		end,
		updated_time = current_timestamp,
		updated_by = user_id_in,
		raw_offset = coalesce(raw_offset_in, fac.raw_offset),
		dst_offset = coalesce(dst_offset_in, fac.dst_offset),
		parent_group_id = coalesce(customer_id_in, fac.parent_group_id)
	where fac.id = facility_id_in;

	update facility_report_type_map frtm set
		"check" = false
	where frtm.facility_id = facility_id_in and not(frtm.report_type_id = any(report_type_ids_in));

	update facility_report_type_map frtm set
		updated_by = user_id_in,
		updated_time = current_timestamp,
		"check" = true
	where frtm.facility_id = facility_id_in and frtm.report_type_id = any(report_type_ids_in);

	return query
		select fac.id, fac.name, fac.alias_name, fac.photo, fac.status, fac.updated_time, _current_facility.photo as old_photo, fac.raw_offset, fac.dst_offset, fac.parent_group_id as customer_id,
		(
			select concat('[', array_to_string(array_agg(json_build_object('facilityId', r.facility_id, 'reportTypeId', r.report_type_id, 'check', r.check, 'reportTypeName', r.name)), ','), ']')::json
			from (
			   	select frtm.*, rt.*
				from facility_report_type_map frtm
				left join report_type rt on rt.id = frtm.report_type_id
				where frtm.facility_id = fac.id
			) r
		) as report_types
		from "group" fac
		where fac.id = facility_id_in;

END

$$;


ALTER FUNCTION public.update_facility_v4(user_id_in integer, facility_id_in integer, name_in character varying, photo_in character varying, raw_offset_in integer, dst_offset_in integer, customer_id_in integer, report_type_ids_in integer[]) OWNER TO atollogy;

--
-- Name: update_facility_v4(integer, integer, character varying, character varying, integer, integer, integer, integer[], integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_facility_v4(user_id_in integer, facility_id_in integer, name_in character varying, photo_in character varying, raw_offset_in integer, dst_offset_in integer, customer_id_in integer, report_type_ids_in integer[], cycle_time_report_in integer) RETURNS TABLE(id integer, name character varying, alias_name character varying, photo character varying, status integer, updated_time timestamp without time zone, old_photo character varying, raw_offset integer, dst_offset integer, customer_id integer, report_types json, cycle_time_report integer, customer json)
    LANGUAGE plpgsql
    AS $$
declare
	_facility_existed record;
	_facility record;
	_customer record;
	_current_facility record;
	_role_id integer;
begin
	
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;	
	if report_type_ids_in is not null and _role_id <> 1 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;

	select fac.* into _current_facility
	from "group" fac
	where fac.id = facility_id_in and fac.group_type_id = 2 and (fac.expired_time is null or fac.expired_time > now());

	if _current_facility.id is null then
		raise exception 'FACILITY_DOES_NOT_EXIST';
	end if;

	if customer_id_in is not null and customer_id_in <> _current_facility.parent_group_id then
		select cus.* into _customer
		from "group" cus
		where cus.id = customer_id_in and cus.group_type_id = 1 and cus.expired_time is null;
	
		if  _customer.id is null then
			raise exception 'CUSTOMER_DOES_NOT_EXIST';
		end if;
	
		select fac.* into _facility_existed
		from "group" fac
		where fac.parent_group_id = customer_id_in and fac.alias_name = _current_facility.alias_name and fac.expired_time is null and fac.id <> facility_id_in;
		
		if _facility_existed.id is not null then
			raise exception 'FACILITY_EXISTED' using detail = 'name=' || _facility_existed.name;
		end if;
	end if;
	
	select fac.* into _facility 
	from "group" fac
	where fac.id <> facility_id_in and (fac.expired_time is null or fac.expired_time > now()) and (lower(fac.name) = lower(name_in)) and fac.group_type_id = 2 and fac.parent_group_id = _current_facility.parent_group_id and fac.id <> facility_id_in
	limit 1;

	case when (_facility.id is not null) then
		raise exception 'FACILITY_EXISTED' using detail = 'name=' || _facility.name;
	else end case;
	
	update "group" fac set
		name = coalesce(name_in, fac.name),
		photo = case
			when (photo_in = '') then null
			else coalesce(photo_in, fac.photo)
		end,
		updated_time = current_timestamp,
		updated_by = user_id_in,
		raw_offset = coalesce(raw_offset_in, fac.raw_offset),
		dst_offset = coalesce(dst_offset_in, fac.dst_offset),
		parent_group_id = coalesce(customer_id_in, fac.parent_group_id),
		cycle_time_report = coalesce(cycle_time_report_in, fac.cycle_time_report)
	where fac.id = facility_id_in;

	update facility_report_type_map frtm set
		"check" = false
	where frtm.facility_id = facility_id_in and not(frtm.report_type_id = any(report_type_ids_in));

	update facility_report_type_map frtm set
		updated_by = user_id_in,
		updated_time = current_timestamp,
		"check" = true
	where frtm.facility_id = facility_id_in and frtm.report_type_id = any(report_type_ids_in);

	return query
		select fac.id, fac.name, fac.alias_name, fac.photo, fac.status, fac.updated_time, _current_facility.photo as old_photo, fac.raw_offset, fac.dst_offset, fac.parent_group_id as customer_id,
		(
			select concat('[', array_to_string(array_agg(json_build_object('facilityId', r.facility_id, 'reportTypeId', r.report_type_id, 'check', r.check, 'reportTypeName', r.name)), ','), ']')::json
			from (
			   	select frtm.*, rt.*
				from facility_report_type_map frtm
				left join report_type rt on rt.id = frtm.report_type_id
				where frtm.facility_id = fac.id
			) r
		) as report_types,
		fac.cycle_time_report,
		json_build_object('id', cus.id, 'name', cus.name, 'aliasName', cus.alias_name) as customer
		from "group" fac
		left join "group" cus on cus.id = fac.parent_group_id
		where fac.id = facility_id_in;

END

$$;


ALTER FUNCTION public.update_facility_v4(user_id_in integer, facility_id_in integer, name_in character varying, photo_in character varying, raw_offset_in integer, dst_offset_in integer, customer_id_in integer, report_type_ids_in integer[], cycle_time_report_in integer) OWNER TO atollogy;

--
-- Name: update_image_step_index_v3(integer, integer, text); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_image_step_index_v3(in_user_id integer, image_step_id_in integer, in_action text) RETURNS TABLE(id integer, name character varying, image_step json, photo character varying, region_filter boolean, box_name character varying, box_x_position integer, box_y_position integer, box_width integer, box_height integer, opacity numeric, brightness_track boolean, annotate_after_filter boolean, index integer, config jsonb, enable boolean, subjects jsonb, brightness jsonb, input_image_indexes jsonb, input_step character varying, color_space character varying, regions jsonb, input_crop jsonb, image_complexity_threshold integer)
    LANGUAGE plpgsql
    AS $$
declare 
	_current_image_step record;
	_image_step_swap record;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = in_user_id;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	select * into _current_image_step from camera_image_step c where c.id = image_step_id_in;
	
	if (in_action = 'up') then	
		select cis.* into _image_step_swap
		from camera_image_step cis
		where cis."index" < _current_image_step.index
			and (cis.expired_time is null or cis.expired_time > now())
			and cis.camera_id = _current_image_step.camera_id
			and case when _current_image_step.image_step_id in (1, 2) then cis.image_step_id in (1, 2)
				else cis.image_step_id in (5, 6)
			end
		order by cis."index" desc, cis.id desc limit 1;
	else 
		select cis.* into _image_step_swap
		from camera_image_step cis 
		where cis."index" > _current_image_step.index
			and (cis.expired_time is null or cis.expired_time > now())
			and cis.camera_id = _current_image_step.camera_id
			and case when _current_image_step.image_step_id in (1, 2) then cis.image_step_id in (1, 2)
				else cis.image_step_id in (5, 6)
			end
		order by cis."index" asc, cis.id asc limit 1;
	end if;

	if _image_step_swap.id is null then
		_image_step_swap = _current_image_step;
	end if;
	
	
	update camera_image_step c set "index" = _current_image_step.index where c.id = _image_step_swap.id;
	update camera_image_step c set "index" = _image_step_swap.index where c.id = _current_image_step.id;

	if _current_image_step.image_step_id in (1, 2, 7, 8, 9) then
		update camera c 
		set is_config_published = false
		where c.id = _current_image_step.camera_id;
	end if;
		
	return query select
		cis.id,
		cis."name",
		json_build_object('id', ims.id, 'name', ims."name", 'imageStepId', ims.image_step_id) as image_step,
		cis.photo,
		cis.region_filter,
		cis.box_name,
		cis.box_x_position,
		cis.box_y_position,
		cis.box_width,
		cis.box_height,
		cis.opacity,
		cis.brightness_track,
		cis.annotate_after_filter,
		cis."index",
		cis.config,
		cis."enable",
		cis.subjects, 
		cis.brightness,
		cis.input_image_indexes, 
		cis.input_step,
		cis.color_space,
		cis.regions,
		cis.input_crop,
		cis.image_complexity_threshold
		from camera_image_step cis
		left join image_step ims on ims.id = cis.image_step_id
		where cis.id = image_step_id_in;

end;
$$;


ALTER FUNCTION public.update_image_step_index_v3(in_user_id integer, image_step_id_in integer, in_action text) OWNER TO atollogy;

--
-- Name: update_image_step_v3(integer, integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_image_step_v3(user_id_in integer, image_step_id_id integer, data_in json) RETURNS TABLE(id integer, name character varying, image_step json, photo character varying, region_filter boolean, box_name character varying, box_x_position integer, box_y_position integer, box_width integer, box_height integer, opacity numeric, brightness_track boolean, annotate_after_filter boolean, index integer, config jsonb)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_image_step_existed record;
	_image_step record;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	select c.* into _image_step
	from camera_image_step c
	where c.id = image_step_id_id;

	select c.* into _image_step_existed 
	from camera_image_step c 
	where (c.expired_time is null or c.expired_time > now()) and (lower(c.name) = lower(data_in ->> 'name')) and c.id <> image_step_id_id and c.camera_id = _image_step.camera_id
	limit 1;

	case when (_image_step_existed.id is not null) THEN
		raise exception 'IMAGE_STEP_EXISTED' using detail = 'name=' || _image_step_existed.name;
	else end case;

	update camera_image_step c set
		"name" = coalesce((data_in ->> 'name')::varchar, c."name"),
		box_name = coalesce((data_in ->> 'boxName')::varchar, c.box_name),
		photo = coalesce((data_in ->> 'photo')::varchar, c.photo),
		region_filter = coalesce((data_in ->> 'regionFilter')::bool, c.region_filter),
		box_x_position = coalesce((data_in ->> 'boxXPosition')::integer, c.box_x_position),
		box_y_position = coalesce((data_in ->> 'boxYPosition')::integer, c.box_y_position),
		box_width = coalesce((data_in ->> 'boxWidth')::integer, c.box_width),
		box_height = coalesce((data_in ->> 'boxHeight')::integer, c.box_height),
		opacity = coalesce((data_in ->> 'opacity')::numeric, c.opacity),
		brightness_track = coalesce((data_in ->> 'brightnessTrack')::bool, c.brightness_track),
		updated_time = current_timestamp,
		config = coalesce(("data_in" ->> 'config')::jsonb, c.config)
	where c.id = image_step_id_id;
		
	update camera c 
	set status = 2, updated_by = user_id_in, updated_time = current_timestamp, is_config_published = false
	where c.id = _image_step.camera_id;

	update "group" g
	set updated_by = user_id_in, updated_time = current_timestamp
	where g.id = (
		select cus.id
		from camera c
		left join "group" fac on fac.id = c.group_id
		left join "group" cus on cus.id = fac.parent_group_id
		where c.id = _image_step.camera_id
	);


	return query select
		cis.id,
		cis."name",
		json_build_object('id', ims.id, 'name', ims."name", 'imageStepId', ims.image_step_id) as image_step,
		cis.photo,
		cis.region_filter,
		cis.box_name,
		cis.box_x_position,
		cis.box_y_position,
		cis.box_width,
		cis.box_height,
		cis.opacity,
		cis.brightness_track,
		cis.annotate_after_filter,
		cis."index",
		cis.config
		from camera_image_step cis
		left join image_step ims on ims.id = cis.image_step_id
		where cis.id = _image_step.id;
END
$$;


ALTER FUNCTION public.update_image_step_v3(user_id_in integer, image_step_id_id integer, data_in json) OWNER TO atollogy;

--
-- Name: update_image_step_v4(integer, integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_image_step_v4(user_id_in integer, image_step_id_id integer, data_in json) RETURNS TABLE(id integer, name character varying, image_step json, photo character varying, region_filter boolean, box_name character varying, box_x_position integer, box_y_position integer, box_width integer, box_height integer, opacity numeric, brightness_track boolean, annotate_after_filter boolean, index integer, config jsonb, enable boolean, subjects jsonb, brightness jsonb, input_image_indexes jsonb, input_step character varying, color_space character varying, regions jsonb, input_crop jsonb, image_complexity_threshold integer, "verbose" boolean)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_image_step_existed record;
	_image_step record;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	select c.* into _image_step
	from camera_image_step c
	where c.id = image_step_id_id;

	select c.* into _image_step_existed 
	from camera_image_step c 
	where c.expired_time is null and (lower(c.name) = lower(data_in ->> 'name')) and c.id <> image_step_id_id and c.camera_id = _image_step.camera_id
	limit 1;

	case when (_image_step_existed.id is not null) THEN
		raise exception 'IMAGE_STEP_EXISTED' using detail = 'name=' || _image_step_existed.name;
	else end case;

	update camera_image_step c set
		image_step_id = coalesce((data_in ->> 'imageStepId')::integer, c.image_step_id),
		"name" = coalesce((data_in ->> 'name')::varchar, c."name"),
		box_name = coalesce((data_in ->> 'boxName')::varchar, c.box_name),
		photo = coalesce((data_in ->> 'photo')::varchar, c.photo),
		region_filter = coalesce((data_in ->> 'regionFilter')::bool, false),
		box_x_position = coalesce((data_in ->> 'boxXPosition')::integer, null),
		box_y_position = coalesce((data_in ->> 'boxYPosition')::integer, null),
		box_width = coalesce((data_in ->> 'boxWidth')::integer, null),
		box_height = coalesce((data_in ->> 'boxHeight')::integer, null),
		opacity = coalesce((data_in ->> 'opacity')::numeric, null),
		brightness_track = coalesce((data_in ->> 'brightnessTrack')::bool, null),
		updated_time = current_timestamp,
		config = coalesce(("data_in" ->> 'config')::jsonb, c.config),
		"enable" = coalesce(("data_in" ->> 'enable')::bool, c."enable"),
		subjects = coalesce(("data_in" ->> 'subjects')::jsonb, c.subjects),
		brightness = coalesce(("data_in" ->> 'brightness')::jsonb, null),
		input_image_indexes = coalesce(("data_in" ->> 'inputImageIndexes')::jsonb, null),
		input_step = coalesce("data_in" ->> 'inputStep', null),
		color_space = coalesce("data_in" ->> 'colorSpace', null),
		regions = coalesce(("data_in" ->> 'regions')::jsonb, null),
		input_crop = coalesce(("data_in" ->> 'inputCrop')::jsonb, null),
		image_complexity_threshold = coalesce(("data_in" ->> 'imageComplexityThreshold')::integer, null),
		"verbose" = coalesce(("data_in" ->> 'verbose')::bool, null)
		
	where c.id = image_step_id_id;
		
	update camera c 
	set status = 2, updated_by = user_id_in, updated_time = current_timestamp, is_config_published = false
	where c.id = _image_step.camera_id;

	update "group" g
	set updated_by = user_id_in, updated_time = current_timestamp
	where g.id = (
		select cus.id
		from camera c
		left join "group" fac on fac.id = c.group_id
		left join "group" cus on cus.id = fac.parent_group_id
		where c.id = _image_step.camera_id
	);


	return query select
		cis.id,
		cis."name",
		json_build_object('id', ims.id, 'name', ims."name", 'imageStepId', ims.image_step_id) as image_step,
		cis.photo,
		cis.region_filter,
		cis.box_name,
		cis.box_x_position,
		cis.box_y_position,
		cis.box_width,
		cis.box_height,
		cis.opacity,
		cis.brightness_track,
		cis.annotate_after_filter,
		cis."index",
		cis.config,
		cis."enable",
		cis.subjects, 
		cis.brightness,
		cis.input_image_indexes, 
		cis.input_step,
		cis.color_space,
		cis.regions,
		cis.input_crop,
		cis.image_complexity_threshold,
		cis."verbose"
		from camera_image_step cis
		left join image_step ims on ims.id = cis.image_step_id
		where cis.id = _image_step.id;
END
$$;


ALTER FUNCTION public.update_image_step_v4(user_id_in integer, image_step_id_id integer, data_in json) OWNER TO atollogy;

--
-- Name: update_order_of_image_step_v4(json, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_order_of_image_step_v4(image_step_ids_in json, user_id_in integer) RETURNS TABLE(id integer, name character varying, image_step json, photo character varying, region_filter boolean, box_name character varying, box_x_position integer, box_y_position integer, box_width integer, box_height integer, opacity numeric, brightness_track boolean, annotate_after_filter boolean, index integer, config jsonb, enable boolean, subjects jsonb, brightness jsonb, input_image_indexes jsonb, input_step character varying, color_space character varying, regions jsonb, input_crop jsonb, image_complexity_threshold integer)
    LANGUAGE plpgsql
    AS $$
declare
	_role_id integer;
	_camera_id integer;
	_image_step_ids json;
	_image_step_id varchar;
	_order_index integer := 1;
	_image_step_config_type integer;

begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id = 4 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	

	select cis.camera_id, si.image_config_type_id into _camera_id, _image_step_config_type
	from camera_image_step cis 
	left join image_step si on si.id = cis.image_step_id
	where cis.id in (select value::integer from json_array_elements_text(image_step_ids_in) limit 1);

	select json_agg(cis.id) into _image_step_ids 
	from camera_image_step cis
	left join image_step si on si.id = cis.image_step_id
	where cis.camera_id = _camera_id and cis.expired_time is null and si.image_config_type_id = _image_step_config_type;

	if not (_image_step_ids::jsonb @> image_step_ids_in::jsonb and image_step_ids_in::jsonb @> _image_step_ids::jsonb) then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	for _image_step_id in select * from json_array_elements_text(image_step_ids_in)
	loop
		update camera_image_step cis set "index" = _order_index where cis.id = _image_step_id::integer;
		_order_index := _order_index +1;
	end loop;

	return query select
		cis.id,
		cis."name",
		json_build_object('id', ims.id, 'name', ims."name", 'imageStepId', ims.image_step_id) as image_step,
		cis.photo,
		cis.region_filter,
		cis.box_name,
		cis.box_x_position,
		cis.box_y_position,
		cis.box_width,
		cis.box_height,
		cis.opacity,
		cis.brightness_track,
		cis.annotate_after_filter,
		cis."index",
		cis.config,
		cis."enable",
		cis.subjects, 
		cis.brightness,
		cis.input_image_indexes, 
		cis.input_step,
		cis.color_space,
		cis.regions,
		cis.input_crop,
		cis.image_complexity_threshold
		from camera_image_step cis
		left join image_step ims on ims.id = cis.image_step_id
		where cis.camera_id = _camera_id and cis.expired_time is null
		order by cis."index" asc;
END;

$$;


ALTER FUNCTION public.update_order_of_image_step_v4(image_step_ids_in json, user_id_in integer) OWNER TO atollogy;

--
-- Name: update_password_v1(integer, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_password_v1(user_id_in integer, password_in character varying) RETURNS TABLE(info json, username character varying, password character varying, role_id integer, role_name character varying, account_type_id integer, phone_number character varying, phone_country_code character varying, receive_email_notification boolean, customers json)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_user record;
	_customers json := null;
begin
	SELECT u.* INTO _user FROM "user" u WHERE (u.id = user_id_in);
	CASE WHEN (_user.id IS NULL) THEN
		RAISE EXCEPTION 'USER_DOES_NOT_EXIST';
	ELSE END CASE;
	
	update "user" u set "password" = password_in where u.id = user_id_in;

	SELECT us.* INTO _user 
	from "user" us 
	left join customer_user cu on cu.user_id = us.id
	WHERE us.id = user_id_in;
	
	if _user.role_id in (2, 3, 5) then
		select concat('[', array_to_string(array_agg(row_to_json(c.*)), ','), ']')::json into _customers
		from get_customers_v4(_user.id::integer) c;
	end if;

--	if _user.role_id = 2 then
--		select concat('[', array_to_string(array_agg(row_to_json(c.*)), ','), ']')::json into _customers
--		from get_customers_v3(_user.id::integer) c;
--	end if;
	
	RETURN QUERY
		select to_json(u.*) as info, 
			u.username,
			u."password",
			u."role_id" as "role_id",
			ur.name as "role_name",
			u.account_type_id as account_type_id,
			u.phone_number,
			u.phone_country_code,
			u.receive_email_notification,
			_customers as customers
		from "user" u 
		left join user_role ur on ur.id = u.role_id
		where u.id = user_id_in;
END

$$;


ALTER FUNCTION public.update_password_v1(user_id_in integer, password_in character varying) OWNER TO atollogy;

--
-- Name: update_status_user_by_admin_v4(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_status_user_by_admin_v4(user_id_in integer, user_delete_id integer, status_in integer) RETURNS TABLE(id bigint, hashed_username character varying)
    LANGUAGE plpgsql
    AS $$

declare
	_user_info record;
begin
	select u.* into _user_info from "user" u where u.id = user_id_in;
	if _user_info.role_id <> 1 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
--	update "user" u set expired_time = current_timestamp where u.id = user_delete_id;
	update "user" u set status = status_in where u.id = user_delete_id;
	return query select u.id, u.hashed_username from "user" u where u.id = user_delete_id;
END

$$;


ALTER FUNCTION public.update_status_user_by_admin_v4(user_id_in integer, user_delete_id integer, status_in integer) OWNER TO atollogy;

--
-- Name: update_truth_image_v5(integer, integer, jsonb); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_truth_image_v5(user_id_in integer, camera_id_in integer, data_in jsonb) RETURNS TABLE(id bigint)
    LANGUAGE plpgsql
    AS $$
declare
	_info record;
begin
	insert into camera_truth_image_map(camera_id, data_image, created_by)
	values(camera_id_in, data_in, user_id_in)
	returning * into _info;

	return query select _info.id;
END

$$;


ALTER FUNCTION public.update_truth_image_v5(user_id_in integer, camera_id_in integer, data_in jsonb) OWNER TO atollogy;

--
-- Name: update_truth_image_v6(integer, integer, jsonb, character varying); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_truth_image_v6(user_id_in integer, camera_id_in integer, data_in jsonb, truth_image_in character varying) RETURNS TABLE(id bigint)
    LANGUAGE plpgsql
    AS $$
declare
	_info record;
begin
	insert into camera_truth_image_map(camera_id, data_image, created_by)
	values(camera_id_in, data_in, user_id_in)
	returning * into _info;

	perform update_camera_image_v6(user_id_in, camera_id_in, truth_image_in, null);

	return query select _info.id;
END

$$;


ALTER FUNCTION public.update_truth_image_v6(user_id_in integer, camera_id_in integer, data_in jsonb, truth_image_in character varying) OWNER TO atollogy;

--
-- Name: update_user_info_v1(integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_user_info_v1(user_id_in integer, user_profile_ip json) RETURNS TABLE(info json, username character varying, password character varying, role_id integer, role_name character varying, account_type_id integer, phone_number character varying, phone_country_code character varying, receive_email_notification boolean, customers json, old_photo character varying)
    LANGUAGE plpgsql
    AS $$

declare
	_user_info record;
	_customers json := null;
	_user record;
begin
	select u.* into _user_info from "user" u where u.id = user_id_in;
		
	update "user" u set
		first_name = coalesce("user_profile_ip" ->> 'firstName', u.first_name),
		last_name = coalesce("user_profile_ip" ->> 'lastName', u.last_name),
		phone_number = coalesce("user_profile_ip" ->> 'phoneNumber', u.phone_number),
		photo = case
			when ("user_profile_ip" ->> 'avatar' = '') then null
			else coalesce("user_profile_ip" ->> 'avatar', u.photo)
		end,
		updated_time = current_timestamp
	where u.id = user_id_in
	returning * into _user;
	
	if _user.role_id in (2, 3, 5) then
		select concat('[', array_to_string(array_agg(row_to_json(c.*)), ','), ']')::json into _customers
		from get_customers_v4(user_id_in::integer) c;
	end if;

	RETURN QUERY
		select to_json(u.*) as info, 
			u.username,
			u."password",
			u."role_id" as "role_id",
			ur.name as "role_name",
			u.account_type_id as account_type_id,
			u.phone_number,
			u.phone_country_code,
			u.receive_email_notification,
			_customers as customers,
			_user_info.photo as old_photo
		from "user" u 
		left join user_role ur on ur.id = u.role_id
		where u.id = user_id_in and u.expired_time is null;
END

$$;


ALTER FUNCTION public.update_user_info_v1(user_id_in integer, user_profile_ip json) OWNER TO atollogy;

--
-- Name: update_user_setting_v4(integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_user_setting_v4(user_id_in integer, data_in json) RETURNS TABLE(id bigint, receive_email_notification boolean)
    LANGUAGE plpgsql
    AS $$

declare
	
begin
	
	update "user" u set 
		receive_email_notification = coalesce((data_in ->> 'receiveEmailNotification')::bool, u.receive_email_notification),
		updated_time = current_timestamp
	where u.id = user_id_in;

	return query
 		select u.id, u.receive_email_notification
		from "user" u
		where u.id = user_id_in;
	
END

$$;


ALTER FUNCTION public.update_user_setting_v4(user_id_in integer, data_in json) OWNER TO atollogy;

--
-- Name: update_user_v1(integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_user_v1(user_id_in integer, user_profile_ip json) RETURNS TABLE(id bigint, username character varying, status integer, first_name character varying, last_name character varying, photo character varying, role_id integer, role_name character varying, account_type_id integer, account_type_name character varying, phone_country_code character varying, customers json, created_time timestamp without time zone)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_user record;
	_customer record;
	_facility_ids jsonb;
begin
	select u.* into _user 
	from "user" u 
	where (u.username = ("user_profile_ip" ->> 'username')) and u.id <> user_id_in
	limit 1;
	case when (_user.id is not null) and (_user.username = ("user_profile_ip" ->> 'username')) then
		raise exception 'EMAIL_EXISTED';
	else end case;

	if (("user_profile_ip" ->> 'roleId')::int4 = 2) then
		select g.* into _customer from "group" g where g.group_type_id = 1 and g.id = ("user_profile_ip" ->> 'customerId')::int4;
		if _customer.id is null then
			raise exception 'CUSTOMER_DOES_NOT_EXIST';
		end if;
	end if;

	if (("user_profile_ip" ->> 'roleId')::int4 = 3) then
		select json_agg(g.id) into _facility_ids
		from "group" g
		where g.group_type_id = 2 and g.id = any(
			select r::text::integer from json_array_elements((user_profile_ip ->> 'facilityIds')::json) r
		);
		
		if not (_facility_ids::jsonb @> (user_profile_ip ->> 'facilityIds')::jsonb and (user_profile_ip ->> 'facilityIds')::jsonb @> _facility_ids::jsonb) then
			raise exception 'YOU_DONT_HAVE_PERMISSION';
		end if;
	end if;

	update "user" u set
		first_name = coalesce("user_profile_ip" ->> 'firstName', u.first_name),
		last_name = coalesce("user_profile_ip" ->> 'lastName', u.last_name),
		phone_number = coalesce("user_profile_ip" ->> 'phoneNumber', u.phone_number),
		role_id = coalesce(("user_profile_ip" ->> 'roleId')::int4, u.role_id),
		updated_time = current_timestamp
	where u.id = user_id_in
	returning * into _user;

	if _user.role_id = 2 then
		delete from customer_user cu
		where cu.user_id = user_id_in;

		insert into public.customer_user(user_id, customer_id, created_by)
		values(_user.id, 
				("user_profile_ip" ->> 'customerId')::int4,
				 ("user_profile_ip" ->> 'userId')::int4
		);
	end if;

	if _user.role_id = 3 then
		delete from facility_user fu
		where fu.user_id = user_id_in;
	
		insert into facility_user(user_id, facility_id, created_by)
		select _user.id,
				r::text::integer,
				("user_profile_ip" ->> 'userId')::int4
		from json_array_elements((user_profile_ip ->> 'facilityIds')::json) r;
	end if;
	
	RETURN QUERY 
		select u.id, u.username, u.status, u.first_name, u.last_name, u.photo, u.role_id, ur.name as role_name, u.account_type_id, ac.name as account_type_name, u.phone_country_code,
		(
			select concat('[', array_to_string(array_agg(row_to_json(r.*)), ','), ']')::json 
			from get_customers_v4(u.id::integer) r
		) as customers,
		u.created_time
		from "user" u
		left join user_role ur on ur.id = u.role_id
		left join account_type ac on ac.id = u.account_type_id
		left join customer_user cu on cu.user_id = u.id
		left join "group" g on g.id = cu.customer_id
		where u.expired_time is null and u."id" = _user."id";
END

$$;


ALTER FUNCTION public.update_user_v1(user_id_in integer, user_profile_ip json) OWNER TO atollogy;

--
-- Name: update_user_v1(integer, integer, json); Type: FUNCTION; Schema: public; Owner: atollogy
--

CREATE FUNCTION public.update_user_v1(user_id_in integer, user_id_update_in integer, user_profile_ip json) RETURNS TABLE(id bigint, username character varying, status integer, first_name character varying, last_name character varying, photo character varying, role_id integer, role_name character varying, account_type_id integer, account_type_name character varying, phone_country_code character varying, customers json, created_time timestamp without time zone, phone_number character varying)
    LANGUAGE plpgsql
    AS $$

DECLARE
	_user record;
	_customer record;
	_facility_ids jsonb;
	_role_id integer;
begin
	select u.role_id into _role_id
	from "user" u
	where u.id = user_id_in;
	if _role_id <> 1 then
		raise exception 'YOU_DONT_HAVE_PERMISSION';
	end if;
	
	select u.* into _user 
	from "user" u 
	where (u.username = ("user_profile_ip" ->> 'username')) and u.id <> user_id_update_in and u.expired_time is null
	limit 1;
	case when (_user.id is not null) and (_user.username = ("user_profile_ip" ->> 'username')) then
		raise exception 'EMAIL_EXISTED';
	else end case;

	if (("user_profile_ip" ->> 'roleId')::int4 = 2) then
		select g.* into _customer from "group" g 
		where g.group_type_id = 1 and g.id = ("user_profile_ip" ->> 'customerId')::int4 and g.expired_time is null;
		if _customer.id is null then
			raise exception 'CUSTOMER_DOES_NOT_EXIST';
		end if;
	end if;

	if (("user_profile_ip" ->> 'roleId')::int4 = 3 or ("user_profile_ip" ->> 'roleId')::int4 = 5) then
		select json_agg(g.id) into _facility_ids
		from "group" g
		where g.group_type_id = 2 and g.expired_time is null
		and g.id = any(
			select r::text::integer from json_array_elements((user_profile_ip ->> 'facilityIds')::json) r
		);
		
		if not (_facility_ids::jsonb @> (user_profile_ip ->> 'facilityIds')::jsonb and (user_profile_ip ->> 'facilityIds')::jsonb @> _facility_ids::jsonb) then
			raise exception 'YOU_DONT_HAVE_PERMISSION';
		end if;
	end if;

	update "user" u set
		first_name = coalesce("user_profile_ip" ->> 'firstName', u.first_name),
		last_name = coalesce("user_profile_ip" ->> 'lastName', u.last_name),
		phone_number = coalesce("user_profile_ip" ->> 'phoneNumber', u.phone_number),
		role_id = coalesce(("user_profile_ip" ->> 'roleId')::int4, u.role_id),
		updated_time = current_timestamp
	where u.id = user_id_update_in
	returning * into _user;

	delete from customer_user cu
	where cu.user_id = user_id_update_in;

	delete from facility_user fu
	where fu.user_id = user_id_update_in;

	if _user.role_id = 2 then
		insert into public.customer_user(user_id, customer_id, created_by)
		values(_user.id, 
				("user_profile_ip" ->> 'customerId')::int4,
				 ("user_profile_ip" ->> 'userId')::int4
		);
	end if;

	if _user.role_id = 3 or _user.role_id = 5 then
		insert into facility_user(user_id, facility_id, created_by)
		select _user.id,
				r::text::integer,
				("user_profile_ip" ->> 'userId')::int4
		from json_array_elements((user_profile_ip ->> 'facilityIds')::json) r;
	end if;
	
	RETURN QUERY 
		select u.id, u.username, u.status, u.first_name, u.last_name, u.photo, u.role_id, ur.name as role_name, u.account_type_id, ac.name as account_type_name, u.phone_country_code,
		(
			select concat('[', array_to_string(array_agg(row_to_json(r.*)), ','), ']')::json 
			from get_customers_v4(u.id::integer) r
		) as customers,
		u.created_time,
		u.phone_number
		from "user" u
		left join user_role ur on ur.id = u.role_id
		left join account_type ac on ac.id = u.account_type_id
		left join customer_user cu on cu.user_id = u.id
		left join "group" g on g.id = cu.customer_id
		where u.expired_time is null and u."id" = _user."id";
END

$$;


ALTER FUNCTION public.update_user_v1(user_id_in integer, user_id_update_in integer, user_profile_ip json) OWNER TO atollogy;

--
-- PostgreSQL database dump complete
--

