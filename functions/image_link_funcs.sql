

CREATE EXTENSION IF NOT EXISTS "ltree";
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

-- permalink function

--DROP FUNCTION IF EXISTS fn_permalink(text);

CREATE OR REPLACE FUNCTION fn_permalink(text)
RETURNS text AS
$$
DECLARE
    s3_path ALIAS FOR $1;
    --
    plink text;
    parts text[];
    fname text;
    fparts text[];
    gwid text;
    ctime text;
    cgr text;
    env text;
    sname text;
    filename text;
BEGIN
    select into parts (select string_to_array(s3_path, '/'));
    select into sname (select parts[7]);
    select into fname (select parts[(select array_upper(parts, 1))]);
    select into fparts (select string_to_array(fname, '_'));
    select into gwid (select fparts[1]);
    select into ctime (select fparts[3]);
    select into cgr (select split_part(parts[5], '_', 2));
    select into env (select split_part(parts[5], '_', 3));
    select into filename (select fparts[(select array_upper(fparts, 1))]);
    select into plink (select concat('https://media.atollogy.com/', cgr, '/', gwid,  '/video0/', ctime, '/', sname,  '/', filename));
    return plink;
END;
$$ LANGUAGE 'plpgsql';


-- portal_link function

--DROP FUNCTION IF EXISTS fn_portal_link(text);

CREATE OR REPLACE FUNCTION fn_portal_link(text)
RETURNS text AS
$$
DECLARE
    s3_path ALIAS FOR $1;
    --
    plink text;
    parts text[];
    fname text;
    fparts text[];
    gwid text;
    ctime text;
    cgr text;
    env text;
    sname text;
    filename text;
    url_base text;
BEGIN
    select into parts (select string_to_array(s3_path, '/'));
    select into sname (select parts[7]);
    select into fname (select parts[(select array_upper(parts, 1))]);
    select into fparts (select string_to_array(fname, '_'));
    select into gwid (select fparts[1]);
    select into ctime (select fparts[3]);
    select into cgr (select split_part(parts[5], '_', 2));
    select into env (select split_part(parts[5], '_', 3));
    select into filename (select fparts[(select array_upper(fparts, 1))]);
    select into url_base (select case
                                    when env = 'prd' then 'https://atollogy.com/portal/image/'
                                    else concat('https://', env, '.at0l.io/portal/image/')
                                 end);
    select into plink (select concat(url_base, cgr, '/', gwid,  '/video0/', ctime, '/', sname,  '/', filename));
    return plink;
END;
$$ LANGUAGE 'plpgsql';


-- image_path function

--DROP FUNCTION IF EXISTS fn_image_path(text);

CREATE OR REPLACE FUNCTION fn_image_path(text)
RETURNS text AS
$$
DECLARE
    s3_path ALIAS FOR $1;
    --
    ppath text;
    parts text[];
    fname text;
    fparts text[];
    gwid text;
    ctime text;
    cgr text;
    env text;
    sname text;
    filename text;
    url_base text;
BEGIN
    select into parts (select string_to_array(s3_path, '/'));
    select into sname (select parts[7]);
    select into fname (select parts[(select array_upper(parts, 1))]);
    select into fparts (select string_to_array(fname, '_'));
    select into gwid (select fparts[1]);
    select into ctime (select fparts[3]);
    select into cgr (select split_part(parts[5], '_', 2));
    select into env (select split_part(parts[5], '_', 3));
    select into filename (select fparts[(select array_upper(fparts, 1))]);
    select into url_base (select '/portal/image/');
    select into ppath (select concat(url_base, cgr, '/', gwid,  '/video0/', ctime, '/', sname,  '/', filename));
    return ppath;
END;
$$ LANGUAGE 'plpgsql';