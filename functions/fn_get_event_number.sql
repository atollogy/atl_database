
CREATE EXTENSION IF NOT EXISTS "ltree";
CREATE EXTENSION IF NOT EXISTS "pgcrypto";
CREATE EXTENSION IF NOT EXISTS "pg_trgm";


-- drop type occurrence cascade;

CREATE table IF NOT EXISTS occurrence
(
    onum bigint not null,
    opath ltree not null,
    otype ltree not null,
    ointerval interval not null,
    start_time timestamptz,
    end_time timestamptz,
    occurrence_id uuid default gen_random_uuid(),
    insert_time timestamptz default now(),
    constraint occurrence_pkey
        primary key (onum, opath, otype, ointerval),
    constraint occurrence_check
        check (end_time > start_time)
);

alter table occurrence owner to atollogy;

create index idx_opath
    on occurrence using gist (opath);

create index idx_otype
    on occurrence using gist (otype);

create index idx_occurrence_validity
    on occurrence (onum, opath, otype, ointerval, start_time, end_time);


-- fn_get_occurrence  -------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS fn_get_occurrence(text,text,timestamp with time zone,interval);

CREATE OR REPLACE FUNCTION fn_get_occurrence(text, text, timestamptz, interval)
RETURNS occurrence AS
$$
DECLARE
    o_opath ALIAS FOR $1;
    o_otype ALIAS FOR $2;
    o_stime ALIAS FOR $3;
    o_ointerval ALIAS FOR $4;
    --
    o_onum bigint;
    o_etime timestamptz;
    --
    eoc occurrence;
    poc occurrence;
    noc occurrence;
    --
BEGIN
--     raise notice '%, %, %, %', opath, otype, otime, ointerval;
    o_onum := floor(EXTRACT(EPOCH FROM o_otime)/floor(extract(EPOCH from o_ointerval)));
    o_opath := text2ltree(o_opath);
    o_otype := text2ltree(o_otype);
    o_etime := o_stime + o_ointerval;
    --
    eoc := (select oc from occurrence oc
                    where oc.opath = o_opath and
                             oc.otype = o_otype and
                             oc.ointerval = o_ointerval and
                             oc.onum = o_onum);
--     raise notice '%', eoc;
    --
    poc := (select oc from occurrence oc
                    where oc.opath = o_opath and
                             oc.otype = o_otype and
                             oc.ointerval = o_ointerval and
                             oc.onum = o_onum - 1);
--     raise notice '%', poc;
    --
    noc := (select oc from occurrence oc
                    where oc.opath = o_opath and
                             oc.otype = o_otype and
                             oc.ointerval = o_ointerval and
                             oc.onum = o_onum + 1);
--     raise notice '%', noc;
    --

    if eoc.onum is null then
        if poc.onum is not null and poc.end_time > o_stime then
            insert into occurrence (onum, opath, otype, ointerval, start_time, end_time)
                   values (o_onum, o_opath, o_otype, o_ointerval, poc.end_time, poc.end_time + o_ointerval);
            return poc;
        else
            insert into occurrence (onum, opath, otype, ointerval, start_time, end_time)
                   values (o_onum, o_opath, o_otype, o_ointerval, o_stime, o_etime);
            eoc := (select oc
                    from occurrence oc
                    where oc.onum = o_onum and
                          oc.opath = o_opath and
                          oc.otype = o_otype and
                          oc.ointerval = o_ointerval);
            if noc.onum is null then
                insert into occurrence (onum, opath, otype, ointerval, start_time, end_time)
                       values (eoc.onum + 1, eoc.opath, eoc.otype, eoc.ointerval, eoc.end_time, eoc.end_time + ointerval);
            end if;
            return eoc;
        end if;
    elsif eoc.onum is not null then
        if poc.onum is not null and poc.end_time > o_stime then
            return poc;
        elsif eoc.start_time > o_stime then
            if poc is not null and poc.end_time < o_stime then
                update occurrence oc set start_time = o_stime
                    where oc.onum = eoc.onum and
                          oc.opath = eoc.opath and
                          oc.otype = eoc.otype and
                          oc.ointerval = eoc.ointerval;
                eoc := (select oc
                        from occurrence oc
                        where oc.onum = eoc.onum and
                              oc.opath = eoc.opath and
                              oc.otype = eoc.otype and
                              oc.ointerval = eoc.ointerval);
            end if;
            if noc.onum is null then
                insert into occurrence (onum, opath, otype, ointerval, start_time, end_time)
                        values (eoc.onum + 1, eoc.opath, eoc.otype, eoc.ointerval, eoc.end_time, eoc.end_time + eoc.ointerval);
            end if;
            return eoc;
        else
            if noc.onum is null then
                insert into occurrence (onum, opath, otype, ointerval, start_time, end_time)
                    values (eoc.onum + 1, eoc.opath, eoc.otype, eoc.ointerval, eoc.end_time, eoc.end_time + eoc.ointerval);
            end if;
            return eoc;
        end if;
    end if;
END;
$$ LANGUAGE 'plpgsql';

-- fn_get_occurrence_from_step  -------------------------------------------------------------------------------

-- drop function fn_get_occurrence_from_step(step image_step);

CREATE OR REPLACE FUNCTION fn_get_occurrence_from_step(step image_step)
RETURNS occurrence AS
$$
BEGIN
    if step.collection_interval is not null and extract(epoch from step.collection_interval) != 0 then
        return  fn_get_occurrence(
                            array_to_string(array[step.gateway_id, step.camera_id, step.step_name]::text[], '.'),
                            step.step_function,
                            step.collection_time,
                            step.collection_interval
                );
    elsif step.step_function in  ('lpr', 'lpc') then
        return  fn_get_occurrence(
                            array_to_string(array[step.gateway_id, step.camera_id, step.step_name]::text[], '.'),
                            step.step_function,
                            step.collection_time,
                            interval '4 seconds'
                );
    else
        return  fn_get_occurrence(
                            array_to_string(array[step.gateway_id, step.camera_id, step.step_name]::text[], '.'),
                            step.step_function,
                            step.collection_time,
                            interval '60 seconds'
                );
    end if;
END;
$$ LANGUAGE 'plpgsql';
