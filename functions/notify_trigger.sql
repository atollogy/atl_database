
CREATE OR REPLACE FUNCTION jsonb_array_to_text_array(p_input jsonb)
 RETURNS text[]
 LANGUAGE sql
 IMMUTABLE
AS $function$
  SELECT array_agg(ary)::text[] FROM jsonb_array_elements_text(p_input) AS ary;
$function$;


CREATE OR REPLACE FUNCTION isnumeric(text)
  RETURNS BOOLEAN
  LANGUAGE plpgsql
  IMMUTABLE
AS $function$
  DECLARE
    x NUMERIC;
  BEGIN
      x = $1::NUMERIC;
      RETURN TRUE;
  EXCEPTION WHEN others THEN
      RETURN FALSE;
  END;
$function$


CREATE OR REPLACE FUNCTION notify_trigger()
  RETURNS trigger
  LANGUAGE plpgsql
  IMMUTABLE
AS $trigger$
    DECLARE
      rec RECORD;
      payload TEXT;
      column_name TEXT;
      column_value TEXT;
      payload_items JSONB;
    BEGIN
      -- Set record row depending on operation
      CASE TG_OP
      WHEN 'INSERT', 'UPDATE' THEN
         rec := NEW;
      WHEN 'DELETE' THEN
         rec := OLD;
      ELSE
         RAISE EXCEPTION 'Unknown TG_OP: "%". Should not occur!', TG_OP;
      END CASE;

      -- Get required fields
      FOREACH column_name IN ARRAY (
              SELECT column_name
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = TG_TABLE_NAME) LOOP
        EXECUTE format('SELECT $1.%I::TEXT', column_name)
        INTO column_value
        USING rec;
        payload_json := (SELECT json_agg(n)::text INTO data FROM json_each_text(to_json(rec))) n;
      END LOOP;

      -- Build the payload
      payload := json_build_object(
        'timestamp',CURRENT_TIMESTAMP,
        'operation',TG_OP,
        'schema',TG_TABLE_SCHEMA,
        'table',TG_TABLE_NAME,
        'data',payload_json
      );

      -- Notify the channel
      PERFORM pg_notify(concat('change_events_', TG_TABLE_NAME), payload);

      RETURN rec;
    END;
$trigger$;

