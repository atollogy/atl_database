
-- fn_create_constraint_if_not_exists  ------------------------------------------------------------
create or replace function fn_create_constraint_if_not_exists (text, text, text)
returns void AS
$$
DECLARE
    t_name ALIAS FOR $1;
    c_name ALIAS FOR $2;
    constraint_sql ALIAS FOR $3;
begin
    -- Look for our constraint
    if not exists (select constraint_name
                   from information_schema.constraint_column_usage
                   where table_name = t_name  and constraint_name = c_name) then
        execute constraint_sql;
    end if;
end;
$$ language 'plpgsql';

-- fn_update_resolver  ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fn_update_resolver(text)
RETURNS VOID AS
$$
DECLARE
    s3_path ALIAS FOR $1;
    --
    parts text[];
    fparts text[];
    _uid text;
    _dname text;
    _sensor text;
    _sname text;
    _ftype text;
    _prepath text;
    _epath ltree;
    _etype ltree;
    _hostname text;
    _cgr text;
    _env text;
    _fqdn text;
    filename text;
    fname text;
    path_base text;
    _exists text;
BEGIN
    parts := string_to_array(s3_path, '/');
    _dname := parts[5];
    if _dname is null then
        return;
    end if;
    _sensor := parts[6];
    _sname := parts[7];
    path_base := parts[4];
    _ftype := path_base;
    fname :=  parts[array_upper(parts, 1)];
    fparts := string_to_array(fname, '_');
    filename := fparts[array_upper(fparts, 1)];

    if fparts[1] like '%-%' then
        _uid := (string_to_array(fparts[1], '-'))[5];
    else
        _uid := fparts[1];
    end if;
--     raise notice '%', _uid;

    _hostname = split_part(_dname, '_', 1);
    _cgr := split_part(_dname, '_', 2);
    _env := split_part(_dname, '_', 3);
    _fqdn := concat(_hostname, '.', _cgr, '.', _env, '.at0l.io');

    if path_base = 'image_readings' then
        _ftype := split_part(filename, '.', 1);
    elsif path_base = 'beacons' then
        _ftype := 'ble_capture';
    end if;

    _etype := text2ltree(_ftype);
    _prepath := array_to_string(array[_uid, _dname, _sensor, _sname], '.');
    _epath := text2ltree(_prepath);

    _exists := (select uid
                from resolver
                where uid = _uid and
                      dname = _dname and
                      sensor = _sensor and
                      sname = _sname and
                      ftype = _ftype);

    if _exists is null then
        insert into resolver (uid, dname, sensor, sname, ftype, epath, etype, hostname, cgr, env, fqdn)
            values (_uid, _dname, _sensor, _sname, _ftype, _epath, _etype, _hostname, _cgr, _env, _fqdn);
    end if;
    EXCEPTION
        WHEN others then
            return;
END;
$$ LANGUAGE 'plpgsql';

-- fn_update_resolver_from_array  -----------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_update_resolver_from_array(text[])
RETURNS VOID AS
$$
DECLARE
    s3_path_list ALIAS FOR $1;
    array_len int;
BEGIN
    select into array_len (select cardinality(s3_path_list));
    if array_len is not null and array_len > 0 then
        for cnt in array_lower(s3_path_list, 1)..array_upper(s3_path_list, 1) loop
            perform fn_update_resolver(s3_path_list[cnt]);
        end loop;
    end if;
EXCEPTION
    WHEN others then
        return;
END;
$$ LANGUAGE 'plpgsql';

-- fn_set_label  ----------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_set_label(text, int, text)
RETURNS VOID AS
$$
DECLARE
    _uid ALIAS FOR $1;
    _lid ALIAS FOR $2;
    _label ALIAS FOR $3;
BEGIN
    if _lid = 1 then
        update resolver set label_1 = _label where uid = _uid;
    elsif _lid = 2 then
        update resolver set label_2 = _label where uid = _uid;
    elsif _lid = 3 then
        update resolver set label_3 = _label where uid = _uid;
    end if;
END;
$$ LANGUAGE 'plpgsql';

-- fn_set_alt_label  ------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_set_alt_label(text, text, text, text, text, int, text)
RETURNS VOID AS
$$
DECLARE
    _uid ALIAS FOR $1;
    _dname ALIAS FOR $2;
    _sensor ALIAS FOR $3;
    _sname ALIAS FOR $4;
    _ftype ALIAS FOR $5;
    _lid ALIAS FOR $6;
    _label ALIAS FOR $7;
BEGIN
    if _lid = 1 then
        update resolver
            set alt_label_1 = _label
        where uid = _uid and
        dname = _dname and
        sensor = _sensor and
        sname = _sname and
        ftype = _ftype;
    elsif _lid = 2 then
        update resolver
            set alt_label_2 = _label
        where uid = _uid and
        dname = _dname and
        sensor = _sensor and
        sname = _sname and
        ftype = _ftype;
    elsif _lid = 3 then
        update resolver
            set alt_label_3 = _label
        where uid = _uid and
        dname = _dname and
        sensor = _sensor and
        sname = _sname and
        ftype = _ftype;
    end if;
END;
$$ LANGUAGE 'plpgsql';

-- fn_uid_name  -----------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_uid_name(_uid text)
RETURNS text AS
$$
BEGIN
    return (select dname from resolver where uid = _uid limit 1);
END;
$$ LANGUAGE 'plpgsql';

-- fn_name_uid  -----------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_name_uid(_dname text)
RETURNS text AS
$$
BEGIN
    return (select uid from resolver where dname = _dname limit 1);
END;
$$ LANGUAGE 'plpgsql';

-- fn_uid_label  ----------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_uid_label(_uid text, _lid int)
RETURNS text AS
$$
DECLARE
    res text;
BEGIN
    select into res  (
        select case
            when _lid = 1 then (select label_1 from resolver where uid = _uid limit 1)
            when _lid = 2 then (select label_2 from resolver where uid = _uid limit 1)
            when _lid = 3 then (select label_1 from resolver where uid = _uid limit 1)
            else (select label_1 from resolver where uid = _uid limit 1)
        end);
    return res;
END;
$$ LANGUAGE 'plpgsql';

-- fn_uid_label1  ---------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_uid_label1(_uid text)
RETURNS text AS
$$
BEGIN
    return (select fn_uid_label(_uid, 1));
END;
$$ LANGUAGE 'plpgsql';

-- fn_uid_label  ---------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_uid_label2(_uid text)
RETURNS text AS
$$
BEGIN
    return (select fn_uid_label(_uid, 2));
END;
$$ LANGUAGE 'plpgsql';

-- fn_uid_label3  ---------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_uid_label3(_uid text)
RETURNS text AS
$$
BEGIN
    return (select fn_uid_label(_uid, 3));
END;
$$ LANGUAGE 'plpgsql';

-- fn_uid_alt_label  ------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_uid_alt_label(_uid text, _sname text, lid int)
RETURNS text AS
$$
DECLARE
    res  text;
BEGIN
    select into res  (
        select case
            when _lid = 1 then (
                    select alt_label_1
                    from resolver
                    where uid = _uid and
                          sname = _sname limit 1)
            when _lid = 2 then (
                    select alt_label_2
                    from resolver
                    where uid = _uid and
                          sname = _sname limit 1)
            when _lid = 3 then (
                    select alt_label_3
                    from resolver
                    where uid = _uid and
                          sname = _sname limit 1)
            else (select alt_label_1
                  from resolver
                  where uid = _uid and
                        sname = _sname limit 1)
        end);
    return res;
END;
$$ LANGUAGE 'plpgsql';

-- permalink --------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_permalink(text)
RETURNS text AS
$$
DECLARE
    s3_path ALIAS FOR $1;
    --
    plink text;
    parts text[];
    fname text;
    fparts text[];
    gwid text;
    ctime text;
    cgr text;
    env text;
    sname text;
    filename text;
BEGIN
    select into parts (select string_to_array(s3_path, '/'));
    select into sname (select parts[7]);
    select into fname (select parts[(select array_upper(parts, 1))]);
    select into fparts (select string_to_array(fname, '_'));
    select into gwid (select fparts[1]);
    select into ctime (select fparts[3]);
    select into cgr (select split_part(parts[5], '_', 2));
    select into env (select split_part(parts[5], '_', 3));
    select into filename (select fparts[(select array_upper(fparts, 1))]);
    select into plink (select concat('https://media.atollogy.com/', cgr, '/', gwid,  '/video0/', ctime, '/', sname,  '/', filename));
    return plink;
END;
$$ LANGUAGE 'plpgsql';

-- fn_permalink_from_image_reference  --------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_permalink_from_image_reference(img_ref image_reference)
RETURNS text AS
$$
DECLARE
    nodename text;
    cgr text;
BEGIN
    nodename := fn_uid_name((img_ref).gateway_id);
    cgr := split_part(nodename, '_', 2);
    return (select concat('https://media.atollogy.com/', cgr, '/', (img_ref).gateway_id,  '/video0/', (extract(epoch from (img_ref).collection_time)), '/', (img_ref).step_name,  '/', (img_ref).name));
END;
$$ LANGUAGE 'plpgsql';

-- fn_permalink_from_json_image_reference  --------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_permalink_from_json_image_reference(img_ref json)
RETURNS text AS
$$
DECLARE
    nodename text;
    cgr text;
    link text;
BEGIN
    img_ref := json_populate_record(null::image_reference, img_ref::json);
    link := fn_permalink_from_image_reference(img_ref);
    return link;
END;
$$ LANGUAGE 'plpgsql';

-- fn_permalink_from_jsonb_image_reference  --------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_permalink_from_jsonb_image_reference(img_ref jsonb)
RETURNS text AS
$$
DECLARE
    nodename text;
    cgr text;
    link text;
BEGIN
    img_ref := json_populate_record(null::image_reference, img_ref::json);
    link := fn_permalink_from_image_reference(img_ref);
    return link;
END;
$$ LANGUAGE 'plpgsql';

-- fn_portal_link  --------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_portal_link(text)
RETURNS text AS
$$
DECLARE
    s3_path ALIAS FOR $1;
    --
    plink text;
    parts text[];
    fname text;
    fparts text[];
    gwid text;
    ctime text;
    cgr text;
    env text;
    sname text;
    filename text;
    url_base text;
BEGIN
    select into parts (select string_to_array(s3_path, '/'));
    select into sname (select parts[7]);
    select into fname (select parts[(select array_upper(parts, 1))]);
    select into fparts (select string_to_array(fname, '_'));
    select into gwid (select fparts[1]);
    select into ctime (select fparts[3]);
    select into cgr (select split_part(parts[5], '_', 2));
    select into env (select split_part(parts[5], '_', 3));
    select into filename (select fparts[(select array_upper(fparts, 1))]);
    select into url_base (select case
                                    when env = 'prd' then 'https://atollogy.com/portal/image/'
                                    else concat('https://', env, '.at0l.io/portal/image/')
                                 end);
    select into plink (select concat(url_base, cgr, '/', gwid,  '/video0/', ctime, '/', sname,  '/', filename));
    return plink;
END;
$$ LANGUAGE 'plpgsql';

-- fn_image_path  ---------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_image_path(text)
RETURNS text AS
$$
DECLARE
    s3_path ALIAS FOR $1;
    --
    ppath text;
    parts text[];
    fname text;
    fparts text[];
    gwid text;
    ctime text;
    cgr text;
    env text;
    sname text;
    filename text;
    url_base text;
BEGIN
    select into parts (select string_to_array(s3_path, '/'));
    select into sname (select parts[7]);
    select into fname (select parts[(select array_upper(parts, 1))]);
    select into fparts (select string_to_array(fname, '_'));
    select into gwid (select fparts[1]);
    select into ctime (select fparts[3]);
    select into cgr (select split_part(parts[5], '_', 2));
    select into env (select split_part(parts[5], '_', 3));
    select into filename (select fparts[(select array_upper(fparts, 1))]);
    select into url_base (select '/portal/image/');
    select into ppath (select concat(url_base, cgr, '/', gwid,  '/video0/', ctime, '/', sname,  '/', filename));
    return ppath;
END;
$$ LANGUAGE 'plpgsql';

-- container_number  -----------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION container_number(target text)
RETURNS text AS
$$
DECLARE
    bstring text;
    carrier text;
    cnum text;
    checksum text;
BEGIN
    if target is null or length(target) < 10 then
        return target;
    end if;
    bstring := substring(replace(target, ' ', '') from '[a-zA-Z]{3}[uUjJzZ][0-9]{6,7}');
    carrier := substring(bstring from '([a-zA-Z]{3}[uUjJzZ])[0-9]{6}');
    cnum := substring(bstring from '[a-zA-Z]{4}([0-9]{6})');
    if length(bstring) >= 11 then
        checksum := substring(bstring, 11, 1);
    else
        checksum := '-';
    end if;
    return concat(carrier, ' ', cnum, ' ', checksum);
EXCEPTION
    WHEN OTHERS THEN
        return target;
END;
$$ LANGUAGE 'plpgsql';


-- to_datetime  -----------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION to_datetime(target text, tz text default 'UTC')
RETURNS timestamptz AS
$$
DECLARE
    new_timestamp timestamptz;
BEGIN
    return target::TIMESTAMP AT TIME ZONE tz;
EXCEPTION when others then
    select into new_timestamp (select to_timestamp(target::float));
    return new_timestamp::TIMESTAMP AT TIME ZONE tz;
END;
$$ LANGUAGE 'plpgsql';

-- fn_avg_distance  -------------------------------------------------------------------------------
-- array levenshtein distance - takes 1 or 2 arrays returning the average edit distance between all items

CREATE OR REPLACE FUNCTION fn_avg_distance(text[], text[])
RETURNS float AS
$$
DECLARE
    labels1 ALIAS FOR $1;
    labels2 ALIAS FOR $2;
    --
    distances int[];
BEGIN
    if labels2 is null or cardinality(labels2) = 0 then
        select into labels2 (select reverse(labels1));
    end if;
    for i1 in 1 .. array_upper(labels1, 1) loop
        for i2 in 1 .. array_upper(labels2, 1) loop
            if labels1[i1] not like labels2[i2] then
                distances := distances || levenshtein(labels1[i1], labels2[i2]);
            end if;
        end loop;
    end loop;
    return (select avg(distances));
END;
$$ LANGUAGE 'plpgsql';

-- fn_score_distance  ------------------------------------------------------------------------------

-- select ts_rank(vector to_tsvector(), query tsquery)
--     where topxy = 1;

CREATE OR REPLACE FUNCTION fn_score_distance(text[], text[])
RETURNS float AS
$$
DECLARE
    labels1 ALIAS FOR $1;
    labels2 ALIAS FOR $2;
    --
    distances int[];
    card int;
    sum_dist float default 0.0;
    total_score float default 0.0;
BEGIN
    if labels2 is null or cardinality(labels2) = 0 then
        select into labels2 (select reverse(labels1));
    end if;
    select into card (select (cardinality(labels1) * cardinality(labels2)));
    raise notice 'card: %', card;
    for i1 in 1 .. array_upper(labels1, 1) loop
        for i2 in 1 .. array_upper(labels2, 1) loop
            if labels1[i1] not like labels2[i2] then
                sum_dist := sum_dist + (levenshtein(labels1[i1], labels2[i2]));
                total_score := total_score + (similarity(labels1[i1], labels2[i2]));
            end if;
        end loop;
    end loop;
--     raise notice 'dist_card: %', cardinality(distances);
--     select into sum_dist ((select sum(d) from unnest(distances::int[]) as d)::float);
--     select into total_score ((select sum(d) from unnest(scores::float[]) as d)::float);
    raise notice 'sum_distance: %', sum_dist;
    raise notice 'total_score: %', total_score;
    if sum_dist is null or sum_dist = 0 or total_score is null or total_score = 0 then
        return 0;
    else
        return (total_score/(sum_dist/card));
    end if;
END;
$$ LANGUAGE 'plpgsql';


-- fn_append_array  ----------------------------------------------------------------------------------
-- append item to array even if array is null

CREATE OR REPLACE FUNCTION fn_append_array(text[], text)
RETURNS text[] AS
$$
DECLARE
    inar ALIAS FOR $1;
    toa ALIAS FOR $2;
    --
    tgt text[];
BEGIN
    tgt := array[toa]::text[];
    --raise notice 'number of dims: %', array_ndims(inar);
    if array_ndims(inar) = 1 then
        return (tgt || inar);
    else
        return tgt;
    end if;
END;
$$ LANGUAGE 'plpgsql';

-- fn_expand_array  ----------------------------------------------------------------------------------

create or replace function fn_expand_array(anyarray) returns setof anyelement as
$$
    select ($1)[s] from generate_series(1,array_upper($1, 1)) as s;
$$
language sql immutable;


--CREATE OR REPLACE FUNCTION jsonb_array_to_text_array(p_input jsonb)
-- RETURNS text[]
-- LANGUAGE sql
-- IMMUTABLE
--AS $function$
--
--SELECT array_agg(ary)::text[] FROM jsonb_array_elements_text(p_input) AS ary;
--$function$;

-- fn_top_array_item  -------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_top_n(n int, source text[])
RETURNS text[] AS
$$
DECLARE
    tgt text[];
BEGIN
    tgt := (select array_agg(x.item) from (select i item
              from unnest(source) i
             group by i
             order by count(i) desc
             limit n) x);
    return tgt;
END;
$$ LANGUAGE 'plpgsql';

-- fn_unique_array  -------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_unique_array(VARIADIC source text[])
RETURNS text[] AS
$$
DECLARE
    tgt text[];
BEGIN
    for i1 in 1 .. array_upper(source, 1) loop
        select into tgt (select distinct unnest((tgt || source[i1])));
    end loop;
    return tgt;
END;
$$ LANGUAGE 'plpgsql';

-- fn_flatten_array  -------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_flatten_array(VARIADIC source text[])
RETURNS text[] AS
$$
DECLARE
    tgt text[];
BEGIN
    for i1 in 1 .. array_upper(source, 1) loop
        if source[i1] is not NULL and array_ndim(source[i1]) = 1 then
            tgt := (tgt || source[i1]);
        else
            tgt := (tgt || fn_flatten_array(source[i1]));
        end if;
    end loop;
    return tgt;
END;
$$ LANGUAGE 'plpgsql';

-- fn_flatten_json_array  -------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION fn_flatten_json_array(source json, dkey text default null)
RETURNS text[] AS
$$
DECLARE
    tgt text[];
    elements text[];
BEGIN
    if dkey is null then
        select into elements (
            select array_agg(item ->> 0) FROM jsonb_array_elements(source::jsonb) as item
        );
    else
        select into elements (
            select array_agg(item ->> dkey) FROM jsonb_array_elements(source::jsonb) as item
        );
    end if;
    for i1 in 1 .. array_upper(elements, 1) loop
        if pg_typeof(source[i1]) like pg_typeof(array[]::text[]) then
            if array_ndim(source[i1]) = 1 then
                tgt := (tgt || source[i1]);
            else
                tgt := (tgt || fn_flatten_array(source[i1]));
            end if;
        else
            tgt := (tgt || source[i1]);
        end if;
    end loop;
    return tgt;
END;
$$ LANGUAGE 'plpgsql';


-- fn_get_event_number  -------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_get_occurrence(text, text, timestamptz, interval)
RETURNS occurrence AS
$$
DECLARE
    o_opath ALIAS FOR $1;
    o_otype ALIAS FOR $2;
    o_stime ALIAS FOR $3;
    o_ointerval ALIAS FOR $4;
    --
    o_onum bigint;
    o_etime timestamptz;
    --
    eoc occurrence;
    poc occurrence;
    noc occurrence;
    --
BEGIN
--     raise notice '%, %, %, %', opath, otype, otime, ointerval;
    o_onum := floor(EXTRACT(EPOCH FROM o_stime)/floor(extract(EPOCH from o_ointerval)));
    if o_onum = 0 or o_onum is null then
        o_onum := 60;
    end if;
    o_etime := o_stime + o_ointerval;
    --
    eoc := (select oc from occurrence oc
                    where oc.opath = text2ltree(o_opath) and
                             oc.otype = text2ltree(o_otype) and
                             oc.ointerval = o_ointerval and
                             oc.onum = o_onum);
--     raise notice '%', eoc;
    --
    poc := (select oc from occurrence oc
                    where oc.opath = text2ltree(o_opath) and
                             oc.otype = text2ltree(o_otype) and
                             oc.ointerval = o_ointerval and
                             oc.onum = o_onum - 1);
--     raise notice '%', poc;
    --
    noc := (select oc from occurrence oc
                    where oc.opath = text2ltree(o_opath) and
                             oc.otype = text2ltree(o_otype) and
                             oc.ointerval = o_ointerval and
                             oc.onum = o_onum + 1);
--     raise notice '%', noc;
    --
    if poc.onum is not null and poc.end_time > o_stime then
        return poc;
    elsif eoc.onum is not null then
        if poc is not null and poc.end_time < o_stime then
            update occurrence oc set start_time = o_stime
                where oc.onum = eoc.onum and
                      oc.opath = eoc.opath and
                      oc.otype = eoc.otype and
                      oc.ointerval = eoc.ointerval;
            eoc := (select oc
                from occurrence oc
                where oc.onum = o_onum and
                      oc.opath = text2ltree(o_opath) and
                      oc.otype = text2ltree(o_otype) and
                      oc.ointerval = o_ointerval);
        end if;
        return eoc;
    else
        insert into occurrence (onum, opath, otype, ointerval, start_time, end_time)
            values (o_onum, text2ltree(o_opath), text2ltree(o_otype), o_ointerval, o_stime, o_etime);
        eoc := (select oc
            from occurrence oc
            where oc.onum = o_onum and
                  oc.opath = text2ltree(o_opath) and
                  oc.otype = text2ltree(o_otype) and
                  oc.ointerval = o_ointerval);
        if noc.onum is null then
            insert into occurrence (onum, opath, otype, ointerval, start_time, end_time)
                values (eoc.onum + 1, eoc.opath, eoc.otype, eoc.ointerval, eoc.end_time, eoc.end_time + eoc.ointerval);
        end if;
        return eoc;
    end if;
    EXCEPTION
        WHEN others then
            return (select oc
                from occurrence oc
                where oc.onum = o_onum and
                      oc.opath = text2ltree(o_opath) and
                      oc.otype = text2ltree(o_otype) and
                      oc.ointerval = o_ointerval);
END;
$$ LANGUAGE 'plpgsql';

-- fn_get_occurrence_from_step  -------------------------------------------------------------------------------

-- drop function fn_get_occurrence_from_step(step image_step);

CREATE OR REPLACE FUNCTION fn_get_occurrence_from_step(step image_step)
RETURNS occurrence AS
$$
BEGIN
    if step.collection_interval is not null and extract(epoch from step.collection_interval) != 0 then
        return  fn_get_occurrence(
                            array_to_string(array[step.gateway_id, step.camera_id, step.step_name]::text[], '.'),
                            step.step_function,
                            step.collection_time,
                            step.collection_interval::interval
                );
    elsif step.step_function in  ('lpr', 'lpc') then
        return  fn_get_occurrence(
                            array_to_string(array[step.gateway_id, step.camera_id, step.step_name]::text[], '.'),
                            step.step_function,
                            step.collection_time,
                            interval '4 seconds'
                );
    else
        return  fn_get_occurrence(
                            array_to_string(array[step.gateway_id, step.camera_id, step.step_name]::text[], '.'),
                            step.step_function,
                            step.collection_time,
                            interval '60 seconds'
                );
    end if;
EXCEPTION
    WHEN others then
        return null;
END;
$$ LANGUAGE 'plpgsql';


-- notify_trigger  -------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION notify_trigger() RETURNS trigger AS $trigger$
DECLARE
    rec RECORD;
    payload TEXT;
    column_name TEXT;
    column_value TEXT;
    payload_items JSONB;
BEGIN
    -- Set record row depending on operation
    CASE TG_OP
    WHEN 'INSERT', 'UPDATE' THEN
       rec := NEW;
    WHEN 'DELETE' THEN
       rec := OLD;
    ELSE
       RAISE EXCEPTION 'Unknown TG_OP: "%". Should not occur!', TG_OP;
    END CASE;

    -- Get required fields
    FOREACH column_name IN ARRAY TG_ARGV LOOP
      EXECUTE format('SELECT $1.%I::TEXT', column_name)
      INTO column_value
      USING rec;
      payload_items := coalesce(payload_items,'{}')::jsonb || json_build_object(column_name,column_value)::jsonb;
    END LOOP;

    -- Build the payload
    payload := json_build_object(
      'timestamp',CURRENT_TIMESTAMP,
      'operation',TG_OP,
      'schema',TG_TABLE_SCHEMA,
      'table',TG_TABLE_NAME,
      'data',payload_items
    );

    -- Notify the channel
    PERFORM pg_notify(concat(TG_TABLE_NAME, '_change_events'), payload);
    RETURN rec;
END;
$trigger$ LANGUAGE plpgsql;


