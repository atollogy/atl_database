
------------------------------------------------------------------------------------------------------------------------
-- extensions
------------------------------------------------------------------------------------------------------------------------
CREATE EXTENSION IF NOT EXISTS ltree;
CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE EXTENSION IF NOT EXISTS pg_trgm;
CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;

------------------------------------------------------------------------------------------------------------------------
-- sequences
------------------------------------------------------------------------------------------------------------------------
create sequence reading_id_seq;

alter sequence reading_id_seq owner to atollogy;

create sequence customer_binding_id_seq;

alter sequence customer_binding_id_seq owner to atollogy;

create sequence customer_item_id_seq;

alter sequence customer_item_id_seq owner to atollogy;

create sequence stats_id_seq;

alter sequence stats_id_seq owner to atollogy;

create sequence status_id_seq;

alter sequence status_id_seq owner to atollogy;

create sequence image_id_seq;

alter sequence image_id_seq owner to atollogy;

create sequence image_step_annotation_id_seq;

alter sequence image_step_annotation_id_seq owner to atollogy;

create sequence subject_id_seq;

alter sequence subject_id_seq owner to atollogy;

create sequence blacklist_id_seq;

alter sequence blacklist_id_seq owner to atollogy;

create sequence lpr_overrides_id_seq;

alter sequence lpr_overrides_id_seq owner to atollogy;

create sequence lpn_overrides_id_seq;

alter sequence lpn_overrides_id_seq owner to atollogy;

create sequence lpr_correlation_id_seq;

alter sequence lpr_correlation_id_seq owner to atollogy;

create sequence report_cache_id_seq;

alter sequence report_cache_id_seq owner to atollogy;

------------------------------------------------------------------------------------------------------------------------
-- tables
------------------------------------------------------------------------------------------------------------------------

create table if not exists beacon_reading
(
    beacon_reading_id bigint default nextval('public.reading_id_seq'::regclass) not null
        constraint beacon_reading_key
            unique,
    namespace text not null,
    beacon_id text not null,
    gateway_id text not null,
    slot timestamp with time zone not null,
    mac text not null,
    avg_distance double precision not null,
    sd_distance double precision not null,
    distance_proximity text not null,
    avg_rssi double precision not null,
    sd_rssi double precision not null,
    tx_power_1m double precision not null,
    provenance json not null,
    node_name text not null,
    beacon_type text not null,
    beacon_subtype text,
    asset_key text not null,
    best_rssi double precision,
    best_tx_power_1m double precision,
    best_distance double precision,
    best_time timestamp with time zone,
    reading_count integer default 0,
    slot_raw_data json not null,
    api_version text not null,
    insert_time timestamp with time zone default now(),
    constraint beacon_reading_pkey
        primary key (namespace, beacon_id, gateway_id, slot)
);

alter table beacon_reading owner to atollogy;

create index if not exists beacon_reading_slot_key
    on beacon_reading (slot);

create table if not exists customer_beacon_transition_event_binding
(
    zone_name text not null,
    cycle_name text not null,
    state_name text,
    start_cycle boolean not null,
    end_cycle boolean not null,
    facility text,
    priority numeric
);

alter table customer_beacon_transition_event_binding owner to atollogy;

create table if not exists customer_binding
(
    binding_id bigint default nextval('public.customer_binding_id_seq'::regclass) not null
        constraint customer_binding_pkey
            primary key,
    namespace text not null,
    beacon_id text not null,
    start_time timestamp with time zone,
    end_time timestamp with time zone,
    subject text,
    subject_name text,
    radius double precision,
    zone_name text,
    facility text,
    rssi_smoothing_window_slots integer default 4,
    beacon_slot_size_seconds integer default 60
);

alter table customer_binding owner to atollogy;

create table if not exists customer_report_config
(
    subject_name text not null,
    measure_name text not null,
    facility text not null,
    gateway_name text,
    camera_id text,
    step_name text,
    extract_type text not null,
    extract_key text,
    extract_image_name text,
    cs_config json,
    composite_definition json[],
    hidden boolean,
    start_time timestamp with time zone not null,
    end_time timestamp with time zone,
    smoothing_look_ahead_slots bigint,
    smoothing_look_back_slots bigint,
    secondary_extract_key text,
    extract_confidence double precision,
    extract_threshold_definitions json,
    violation boolean default false,
    priority smallint,
    missing_data_enabled boolean default false,
    constraint customer_report_config_pk
        primary key (subject_name, measure_name, facility, start_time)
);

alter table customer_report_config owner to atollogy;

create table if not exists customer_shift
(
    shift_name text,
    shift_timezone_name text,
    day_of_week integer,
    start_offset interval,
    end_offset interval,
    facility text
);

alter table customer_shift owner to atollogy;

create table if not exists customer_transition_event_binding
(
    gateway_id text not null,
    camera_id text not null,
    step_name text not null,
    start_time timestamp with time zone,
    end_time timestamp with time zone,
    cycle_name text not null,
    state_name text not null,
    start_cycle boolean not null,
    end_cycle boolean not null,
    facility text,
    is_whitelist boolean default false,
    priority numeric
);

alter table customer_transition_event_binding owner to atollogy;

create table if not exists image_step
(
    image_step_id bigint default nextval('public.image_id_seq'::regclass) not null
        constraint image_step_key
            unique,
    gateway_id text not null,
    camera_id text not null,
    collection_time timestamp with time zone not null,
    collection_interval interval default '00:01:00'::interval,
    step_name text not null,
    step_function text not null,
    step_function_version text,
    step_config_version text,
    step_output json,
    step_start_time timestamp with time zone,
    step_end_time timestamp with time zone,
    processor_role text,
    images text[],
    insert_time timestamp with time zone default now(),
    ocid uuid,
    constraint image_step_pkey
        primary key (gateway_id, camera_id, collection_time, step_name)
);

alter table image_step owner to atollogy;

create index if not exists idx_calculate_slot_fn
    on image_step (calculate_slot(collection_time, collection_interval));

create index if not exists idx_collection_time
    on image_step (collection_time);

create index if not exists idx_gw_cam_sname
    on image_step (gateway_id, camera_id, step_name);

create index if not exists image_step_insert_key
    on image_step (insert_time);

create index if not exists uidx_image_step_colltime
    on image_step (collection_time);

create index if not exists uidx_image_step_stepname
    on image_step (step_name, collection_time);

create index if not exists uidx_image_step_gateway
    on image_step (gateway_id, collection_time);

create index if not exists uidx_image_step_metrics
    on image_step (step_name, gateway_id, collection_time);

create index if not exists uidx_image_step_ctime
    on image_step (collection_time);

create index if not exists uidx_image_interval
    on image_step (collection_time, image_step_id);

create table if not exists image_step_annotation
(
    image_step_annotation_id bigint default nextval('public.image_step_annotation_id_seq'::regclass) not null
        constraint image_step_annotation_pkey
            primary key,
    collection_time timestamp with time zone not null,
    gateway_id text not null,
    camera_id text not null,
    step_name text not null,
    step_function_version text not null,
    step_config_version text not null,
    session_id text,
    user_id text not null,
    comment text,
    is_accurate boolean,
    created_at_date timestamp with time zone default now(),
    qc_result json,
    applied bigint,
    ref_id bigint
);

alter table image_step_annotation owner to atollogy;

create table if not exists asset_attribute
(
    attribute_value text not null,
    qualifier text not null,
    latest_seen_time timestamp with time zone,
    asset_info jsonb,
    insert_time timestamp with time zone default now(),
    asset_id text not null,
    first_seen_time timestamp with time zone default now() not null,
    enabled boolean default true,
    constraint asset_attribute_pkey
        primary key (asset_id, qualifier, attribute_value)
);

alter table asset_attribute owner to atollogy;

create table if not exists report_cache
(
    report_cache_id bigint default nextval('public.report_cache_id_seq'::regclass) not null
        constraint report_cache_key
            unique,
    cache_key text not null
        constraint report_cache_pkey
            primary key,
    cache_value jsonb not null,
    expiration_time timestamp with time zone not null,
    insert_time timestamp with time zone default now() not null
);

alter table report_cache owner to atollogy;

create index if not exists idx_report_cache
    on report_cache (cache_key, expiration_time);

create table if not exists gateway_beacon_stats
(
    gateway_beacon_stats_id bigint default nextval('public.stats_id_seq'::regclass) not null,
    gateway_id text not null,
    beacon_slot_count integer default 0,
    reading_count integer default 0,
    unique_beacon_count integer default 0,
    unique_gateway_count integer default 0,
    unique_slot_count integer default 0,
    unique_slots json,
    backlog_unique_slot_count integer default 0,
    backlog_unique_slots json,
    backlog_beacon_slot_count integer default 0,
    backlog_reading_count integer default 0,
    backlog_unique_beacon_count integer default 0,
    backlog_unique_gateway_count integer default 0,
    insert_time timestamp with time zone default now()
);

alter table gateway_beacon_stats owner to atollogy;

create table if not exists gateway_service_status
(
    gateway_service_status_id bigint default nextval('public.status_id_seq'::regclass) not null,
    gateway_id text not null,
    application text not null,
    process_state text not null,
    memory_used double precision,
    cpu_utilization double precision,
    gateway_time timestamp with time zone not null,
    last_state_change timestamp with time zone not null,
    raw_status text,
    insert_time timestamp with time zone default now()
);

alter table gateway_service_status owner to atollogy;

create table if not exists customer_alert_config
(
    id serial not null,
    alert_type varchar(20),
    alert_config_value json
);

alter table customer_alert_config owner to atollogy;

create table if not exists resolver
(
    row_id uuid default gen_random_uuid(),
    uid text not null,
    dname text not null,
    sensor text not null,
    sname text not null,
    ftype text not null,
    epath ltree,
    etype ltree,
    hostname text,
    cgr text,
    env text,
    fqdn text,
    label_1 text,
    label_2 text,
    label_3 text,
    alt_label_1 text,
    alt_label_2 text,
    alt_label_3 text,
    valid_from timestamp with time zone default now() not null,
    valid_until timestamp with time zone,
    updated timestamp with time zone default now() not null,
    constraint resolver_pkey
        primary key (uid, dname, sensor, sname, ftype)
);

alter table resolver owner to atollogy;

create index if not exists idx_resolver_label_search
    on resolver (uid, label_1, label_2, label_3, alt_label_1, alt_label_2, alt_label_3);

create index if not exists idx_resolver_device_search
    on resolver (uid, row_id, dname, hostname, fqdn);

create index if not exists idx_resolver_epath
    on resolver (epath);

create index if not exists idx_resolver_etype
    on resolver (etype);

create table if not exists occurrence
(
    onum bigint not null,
    opath ltree not null,
    otype ltree not null,
    ointerval interval not null,
    start_time timestamp with time zone,
    end_time timestamp with time zone,
    occurrence_id uuid default gen_random_uuid(),
    insert_time timestamp with time zone default now(),
    constraint occurrence_pkey
        primary key (onum, opath, otype, ointerval),
    constraint occurrence_check
        check (end_time > start_time)
);

alter table occurrence owner to atollogy;

create index if not exists idx_opath
    on occurrence (opath);

create index if not exists idx_otype
    on occurrence (otype);

create index if not exists idx_occurrence_validity
    on occurrence (onum, opath, otype, ointerval, start_time, end_time);

create table if not exists lct_overrides
(
    lct_overrides_id serial not null,
    alias text not null,
    plate text not null,
    alias_confidence double precision default 95.0,
    confidence double precision default 100.0,
    score double precision default 5000.0,
    distance integer default 0,
    identifier boolean default false,
    secondary boolean default false,
    block boolean default false,
    vehicle_type text default 'tractor-trailer'::text,
    insert_time timestamp with time zone default now(),
    constraint lct_overrides_pkey
        primary key (alias, plate)
);

alter table lct_overrides owner to atollogy;

create table if not exists lct
(
    lct_id serial not null,
    alias text not null,
    plate text not null,
    alias_confidence double precision not null,
    confidence double precision not null,
    score double precision not null,
    distance integer not null,
    identifier boolean default false,
    secondary boolean default false,
    block boolean default false,
    vehicle_type text default 'tractor-trailer'::text,
    is_override boolean default false,
    insert_time timestamp with time zone default now(),
    constraint lct_pkey
        primary key (alias, plate)
);

alter table lct owner to atollogy;

create index if not exists idx_lct_alias
    on lct (alias);

create index if not exists idx_lct_distance
    on lct (alias, distance);

create index if not exists idx_lct_options
    on lct (plate, distance, identifier, secondary, block, vehicle_type);

create table manual_vehicle_events
(
    id uuid not null
        constraint manual_vehicle_events_pkey
            primary key,
    identifier varchar(255),
    id_type varchar(255),
    event_type varchar(255),
    event_time varchar(255),
    applied UUID,
    user_id integer not null,
    created_at timestamp(0),
    updated_at timestamp(0)
);

alter table manual_vehicle_events owner to atollogy;

--create trigger manual_vehicle_event_trigger
--    before insert
--    on manual_vehicle_events
--    for each row
--    execute procedure manual_vehicle_event_reaction();

