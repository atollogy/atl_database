
------------------------------------------------------------------------------------------------------------------------
-- custom types
------------------------------------------------------------------------------------------------------------------------

create type cycle_event as
(
    start_time timestamp with time zone,
    end_time timestamp with time zone,
    subject_id text,
    score double precision,
    tags text[],
    attributes jsonb,
    facility text,
    state text,
    cycle_id integer,
    weight integer,
    img_keys jsonb,
    is_start boolean,
    is_end boolean,
    priority integer,
    warning text,
    visit_count integer
);

alter type cycle_event owner to atollogy;

create type image_reference as
(
    gateway_id text,
    camera_id text,
    step_name text,
    collection_time timestamp with time zone,
    name text
);

alter type image_reference owner to atollogy;

create type raw_cycle_event as
(
    subject_id text,
    score double precision,
    gateway_id text,
    camera_id text,
    step_name text,
    step_function text,
    collection_time timestamp with time zone,
    facility text,
    state text,
    start_cycle boolean,
    end_cycle boolean,
    priority integer,
    whitelist boolean,
    img_keys image_reference
);

alter type raw_cycle_event owner to atollogy;

create type vattr as
(
    attr_name text,
    attr_value text,
    attr_flag text,
    img_keys image_reference
);

alter type vattr owner to atollogy;


create type bbox as
(
    x int,
    y int,
    w int,
    h int
);

alter type bbox owner to atollogy;

create type point as
(
    x int,
    y int
);

alter type point owner to atollogy;