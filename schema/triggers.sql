
CREATE OR REPLACE FUNCTION occurrence_id_trigger()
RETURNS TRIGGER LANGUAGE plpgsql AS $$
BEGIN
    new.ocid := (fn_get_occurrence_from_step(new)).occurrence_id;
    return new;
EXCEPTION
    WHEN others then
        return new;
END $$;

-- drop trigger ocid_trigger from image_step;

CREATE TRIGGER ocid_trigger
BEFORE INSERT ON image_step
FOR EACH ROW EXECUTE PROCEDURE occurrence_id_trigger();