CREATE DATABASE :new_db_name
  WITH
  OWNER = atollogy
  ENCODING = 'UTF8'
  LC_COLLATE = 'en_US.UTF-8'
  LC_CTYPE = 'en_US.UTF-8'
  CONNECTION LIMIT = -1;

REVOKE CONNECT ON DATABASE :new_db_name FROM PUBLIC;

------------------------------------------------------------

DROP USER IF EXISTS :new_user_name;
CREATE USER :new_user_name WITH
  LOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  NOCREATEROLE
  NOREPLICATION
  ENCRYPTED PASSWORD :new_user_password_quoted;

------------------------------------------------------------

GRANT ALL ON DATABASE :new_db_name TO :new_user_name;
GRANT ALL ON DATABASE :new_db_name TO alpha;
GRANT ALL ON DATABASE :new_db_name TO beta;

GRANT TEMPORARY ON DATABASE :new_db_name TO PUBLIC;
GRANT ALL ON DATABASE :new_db_name TO atollogy;
