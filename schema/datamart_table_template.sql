CREATE TABLE :new_db_name.public.f_events
(
  d_time_slot_time              TIMESTAMP WITH TIME ZONE NOT NULL,
  d_cgr                         TEXT DEFAULT '' :: TEXT  NOT NULL,
  d_facility                    TEXT DEFAULT '' :: TEXT  NOT NULL,
  d_subject_name_major          TEXT DEFAULT '' :: TEXT  NOT NULL,
  d_subject_name_minor          TEXT DEFAULT '' :: TEXT  NOT NULL,
  d_report_bar                  TEXT DEFAULT '' :: TEXT  NOT NULL,
  d_fn_step_function            TEXT,
  d_fn_step_name                TEXT DEFAULT '' :: TEXT  NOT NULL,
  d_state                       TEXT,
  f_collection_time             TIMESTAMP WITH TIME ZONE,
  f_brightness                  TEXT,
  f_images                      TEXT[],
  f_warning                     INTEGER,
  f_image_step_id               BIGINT,
  d_sensor_gateway_display_name TEXT,
  d_sensor_gateway_id           TEXT DEFAULT '' :: TEXT  NOT NULL,
  d_sensor_camera_id            TEXT DEFAULT '' :: TEXT  NOT NULL,
  CONSTRAINT f_events_pkey
    PRIMARY KEY (d_time_slot_time, d_cgr, d_facility, d_subject_name_major, d_subject_name_minor, d_report_bar,
                 d_fn_step_name, d_sensor_gateway_id, d_sensor_camera_id)
);

ALTER TABLE :new_db_name.public.f_events
  OWNER TO atollogy;

INSERT INTO f_events (d_time_slot_time, d_cgr, d_subject_name_major, d_subject_name_minor, d_report_bar,
                      d_fn_step_function, d_fn_step_name, d_state, f_collection_time, f_brightness, f_images,
                      f_warning, f_image_step_id, d_sensor_gateway_display_name, d_sensor_gateway_id,
                      d_sensor_camera_id)
VALUES ('2019-01-01 00:00:00.000000', :cgr_quoted, 'init', 'init', 'init', 'init', 'init', 'init',
        '2019-01-01 00:00:00.000000', null, null, 1, null, 'init', 'init', 'init');

------------------------------------------------------------

CREATE TABLE :new_db_name.public.f_intervals
(
  cgr                TEXT                     NOT NULL,
  facility           TEXT                     NOT NULL,
  shift_name         TEXT                     NOT NULL,
  subject_name       TEXT                     NOT NULL,
  measure_name       TEXT                     NOT NULL,
  measure_state      TEXT                     NOT NULL,
  measure_start_time TIMESTAMP WITH TIME ZONE NOT NULL,
  measure_end_time   TIMESTAMP WITH TIME ZONE NOT NULL,
  CONSTRAINT f_intervals_pkey
    PRIMARY KEY (cgr, facility, subject_name, measure_name, measure_start_time)
);

ALTER TABLE :new_db_name.public.f_intervals
  OWNER TO atollogy;

------------------------------------------------------------

CREATE TABLE :new_db_name.public.f_cycles_summarized
(
  subject     TEXT                     NOT NULL,
  start_time  TIMESTAMP WITH TIME ZONE NOT NULL,
  end_time    TIMESTAMP WITH TIME ZONE,
  cgr         TEXT                     NOT NULL,
  visit_count DOUBLE PRECISION,
  site        TEXT                     NOT NULL,
  CONSTRAINT f_cycles_summarized_pkey
    PRIMARY KEY (cgr, site, subject, start_time)
);

ALTER TABLE :new_db_name.public.f_cycles_summarized
  OWNER TO atollogy;

------------------------------------------------------------

CREATE TABLE :new_db_name.public.f_cycles_detailed
(
  subject           TEXT                     NOT NULL,
  visit_count       DOUBLE PRECISION,
  state             TEXT                     NOT NULL,
  start_time        TIMESTAMP WITH TIME ZONE NOT NULL,
  end_time          TIMESTAMP WITH TIME ZONE,
  weight            NUMERIC,
  cgr               TEXT                     NOT NULL,
  site              TEXT                     NOT NULL,
  is_state_of_cycle BOOLEAN,
  is_end_of_cycle   BOOLEAN,
  CONSTRAINT f_cycles_detailed_pkey
    PRIMARY KEY (cgr, site, subject, visit_count, state, start_time)
);

ALTER TABLE :new_db_name.public.f_cycles_detailed
  OWNER TO atollogy;

------------------------------------------------------------

GRANT SELECT ON ALL TABLES IN SCHEMA public TO :new_user_name;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO atollogy;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO alpha;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO beta;
