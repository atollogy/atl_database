select  distinct on (gateway_id)                gateway_id,
        fn_uid_name(gateway_id)                 nodename,
        (step_output ->> 'is_blurry_or_dirty')  is_dirty,
        (step_output ->> 'bd_factor')           filth_score,
        (step_output ->> 'bd_threshold')        threshold
from image_step
where step_function = 'lpr' and 
    collection_time >= (now() - interval '1 day') and
    (step_output ->> 'is_blurry_or_dirty')::text = 'True' and
    (step_output ->> 'brightness')::float >= 20 and
    (step_output ->> 'bd_factor')::float < 50;
