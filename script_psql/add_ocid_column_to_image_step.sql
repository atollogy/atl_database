ALTER TABLE image_step DROP COLUMN IF EXISTS scene_num;
ALTER TABLE image_step ADD COLUMN IF NOT EXISTS ocid UUID;

CREATE OR REPLACE FUNCTION occurrence_id_trigger()
RETURNS TRIGGER LANGUAGE plpgsql AS $$
BEGIN
    new.ocid := (fn_get_occurrence_from_step(new)).occurrence_id;
    return new;
END $$;

CREATE TRIGGER ocid_trigger
BEFORE INSERT OR UPDATE ON image_step
FOR EACH ROW EXECUTE PROCEDURE occurrence_id_trigger();
