UPDATE image_step
SET collection_interval = INTERVAL '60 seconds'
WHERE collection_interval = INTERVAL '3 seconds'
AND collection_time > '2021-06-01 00:00:00.000000 +00:00';
