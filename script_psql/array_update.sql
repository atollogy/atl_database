
CREATE OR REPLACE FUNCTION fn_update_resolver_from_array(text[])
RETURNS VOID AS
$$
DECLARE
    s3_path_list ALIAS FOR $1;
BEGIN
    for cnt in 1..(select array_upper(s3_path_list, 1)) by 1 loop
        perform fn_update_resolver(s3_path_list[cnt]);
    end loop;
END;
$$ LANGUAGE 'plpgsql';
