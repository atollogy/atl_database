BEGIN;
LOCK TABLE image_step IN EXCLUSIVE MODE;
SELECT setval('image_id_seq', (
                case
                  when ((SELECT nextval('image_id_seq')) > (SELECT MAX(image_step_id) FROM image_step))
                    then (SELECT nextval('image_id_seq'))
                  else (SELECT MAX(image_step_id)+10 FROM image_step)
                end), false);
COMMIT;

BEGIN;
LOCK TABLE gateway_beacon_stats IN EXCLUSIVE MODE;
SELECT setval('stats_id_seq', (
                case
                  when ((SELECT nextval('stats_id_seq')) > (SELECT MAX(gateway_beacon_stats_id) FROM gateway_beacon_stats))
                    then (SELECT nextval('stats_id_seq'))
                  else (SELECT MAX(gateway_beacon_stats_id)+10 FROM gateway_beacon_stats)
                end), false);
COMMIT;

BEGIN;
LOCK TABLE gateway_service_status IN EXCLUSIVE MODE;
SELECT setval('status_id_seq', (
                case
                  when ((SELECT nextval('status_id_seq')) > (SELECT MAX(gateway_service_status_id) FROM gateway_service_status))
                    then (SELECT nextval('status_id_seq'))
                  else (SELECT MAX(gateway_service_status_id)+10 FROM gateway_service_status)
                end), false);
COMMIT;

BEGIN;
LOCK TABLE image_step_annotation IN EXCLUSIVE MODE;
SELECT setval('image_step_annotation_id_seq', (
                case
                  when ((SELECT nextval('image_step_annotation_id_seq')) > (SELECT MAX(image_step_annotation_id) FROM image_step_annotation))
                    then (SELECT nextval('image_step_annotation_id_seq'))
                  else (SELECT MAX(image_step_annotation_id)+10 FROM image_step_annotation)
                end), false);
COMMIT;

BEGIN;
LOCK TABLE beacon_reading IN EXCLUSIVE MODE;
SELECT setval('reading_id_seq', (
                case
                  when ((SELECT nextval('reading_id_seq')) > (SELECT MAX(beacon_reading_id) FROM beacon_reading))
                    then (SELECT nextval('reading_id_seq'))
                  else (SELECT MAX(beacon_reading_id)+10 FROM beacon_reading)
                end), false);
COMMIT;