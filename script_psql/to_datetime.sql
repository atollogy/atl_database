CREATE OR REPLACE FUNCTION to_datetime(target text, tz text default 'UTC')
RETURNS timestamptz AS
$$
DECLARE
    new_timestamp timestamptz;
BEGIN
    return target::TIMESTAMP AT TIME ZONE tz;
EXCEPTION when others then
    select into new_timestamp (select to_timestamp(target::float));
    return new_timestamp::TIMESTAMP AT TIME ZONE tz;
END;
$$ LANGUAGE 'plpgsql';
