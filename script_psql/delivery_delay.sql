select i.gateway_id,
       sum(i.insert_time - i.collection_time)/count(*)
from image_step i
where i.insert_time > (now() - interval '2 days')
group by i.gateway_id;
