
CREATE SEQUENCE public.stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stats_id_seq OWNER TO atollogy;

--
-- Name: gateway_beacon_stats; Type: TABLE; Schema: public; Owner: atollogy
--

CREATE TABLE public.gateway_beacon_stats (
    gateway_beacon_stats_id bigint DEFAULT nextval('public.stats_id_seq'::regclass) NOT NULL,
    gateway_id text NOT NULL,
    beacon_slot_count integer DEFAULT 0,
    reading_count integer DEFAULT 0,
    unique_beacon_count integer DEFAULT 0,
    unique_gateway_count integer DEFAULT 0,
    unique_slot_count integer DEFAULT 0,
    unique_slots json,
    backlog_unique_slot_count integer DEFAULT 0,
    backlog_unique_slots json,
    backlog_beacon_slot_count integer DEFAULT 0,
    backlog_reading_count integer DEFAULT 0,
    backlog_unique_beacon_count integer DEFAULT 0,
    backlog_unique_gateway_count integer DEFAULT 0,
    insert_time timestamp with time zone DEFAULT now()
);


ALTER TABLE public.gateway_beacon_stats OWNER TO atollogy;

--
-- Name: status_id_seq; Type: SEQUENCE; Schema: public; Owner: atollogy
--

CREATE SEQUENCE public.status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.status_id_seq OWNER TO atollogy;

--
-- Name: gateway_service_status; Type: TABLE; Schema: public; Owner: atollogy
--

CREATE TABLE public.gateway_service_status (
    gateway_service_status_id bigint DEFAULT nextval('public.status_id_seq'::regclass) NOT NULL,
    gateway_id text NOT NULL,
    application text NOT NULL,
    process_state text NOT NULL,
    memory_used double precision,
    cpu_utilization double precision,
    gateway_time timestamp with time zone NOT NULL,
    last_state_change timestamp with time zone NOT NULL,
    raw_status text,
    insert_time timestamp with time zone DEFAULT now()
);


ALTER TABLE public.gateway_service_status OWNER TO atollogy;
