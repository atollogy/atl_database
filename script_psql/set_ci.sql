with missing as (
    select i.image_step_id,
           i.step_start_time,
           i.step_end_time,
            (i.step_start_time + i.collection_interval) new_end_time
    from image_step i
    where i.collection_interval = interval '60 milliseconds' and
          i.collection_time > '2020-08-22 02:00:00.000000' and
          i.step_function != 'lpr'
    )
    update image_step i
        set step_end_time = m.new_end_time,
            collection_interval = interval '60 sec'
    from missing m
    where m.image_step_id = i.image_step_id;;
