#!/usr/bin/env python3.6

from attribute_dict import *
from common import *
import  csv
from collections import namedtuple
import psycopg2

columns = ['id', 'group_id', 'camera_id', 'gateway_id']
_column = namedtuple("Column", ["ftype", "cannotBeNull", "otype"])
keys = AD({
    "id": _column(lambda x: int(x), True, lambda x: int(x)),
    "group_id": _column(lambda x: int(x), True, lambda x: int(x)),
    "camera_id": _column(lambda x: "{}".format(x), True, lambda x: "{}".format(x)),
    "gateway_id": _column(lambda x: "{}".format(x), True, lambda x: "{}".format(x) if x else None)
})

missing_gwids = '''
  select c.id,
         c.group_id,
         c.camera_id
  from camera c
  where c.gateway_id is NULL'''

conn = psycopg2.connect(host='tools-prd.cluster-c5nxnkm4ctr1.us-west-2.rds.amazonaws.com',
                       port=5432,
                       user='',
                       password='',
                       dbname='atollogy'
                    )

cur = conn.cursor()

resolver = get_mdata('_nodes/by_nodename')

def unpack(data):
    return [AD({k: keys[k].otype(v) for k, v in dict(zip(columns, r)).items()}) for r in data]

cur.execute(missing_gwids)

for cam in unpack(cur.fetchall()):
    try:
        cam.gateway_id = resolver[cam.camera_id].gateway_id if cam.camera_id in resolver and resolver[cam.camera_id] else None
        if cam.gateway_id:
            print(cam)
            cur.execute(f'''update camera set gateway_id = '{cam.gateway_id}' where id = '{cam.id}' and group_id = '{cam.group_id}';''')
    except Exception as e:
        continue
