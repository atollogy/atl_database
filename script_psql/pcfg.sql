with t as (
	select  crc.subject_name, crc.measure_name, cb.subject_name, cb.beacon_id, crc.facility, crc.step_name,
                (select i.step_function from image_step i where i.gateway_id = cb.beacon_id and i.step_name = crc.step_name limit 1) step_function,
        	crc.extract_key, crc.extract_type, crc.extract_image_name, crc.cs_config, crc.composite_definition, crc.hidden, crc.priority, crc.violation,
	        crc.smoothing_look_ahead_slots, crc.smoothing_look_back_slots
	from    customer_report_config crc
        	inner join customer_binding cb on crc.gateway_name = cb.subject_name
	where   crc.end_time is null and
        	cb.end_time is null
) 
select json_agg(t) from t 
