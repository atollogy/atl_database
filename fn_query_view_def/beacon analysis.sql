-- decay amount: none
-- decay time: -
-- moving average window: 5 (4 preceding plus current)
-- --> extra generated elements: 4

-- gap filling: up to 4 minute gap
-- fill in small gaps with linear interpolation : WHEN gap <= 5 AND ofst < gap THEN rssi + ofst * ((future_rssi - rssi) / gap)
-- fill in small gaps with the least good value WHEN gap <= 5 AND ofst < gap THEN LEAST(future_rssi, rssi)

-- distance cutoff for winner candidates: 10 meters
-- gap filling time for event coalescing : none


WITH extended AS (
	SELECT 
		namespace,
		beacon_id, 
		gateway_id, 
		slot + ofst * INTERVAL '1 minute' slot, 
		CASE WHEN ofst = 0 THEN rssi 
			 WHEN gap <= 5 AND ofst < gap THEN LEAST(future_rssi, rssi) -- fill in small gaps with the worse of the two values
			 END rssi,
		tx_power
	FROM (
		SELECT namespace, beacon_id, gateway_id, slot, rssi, tx_power, CASE WHEN gap > 1 THEN generate_series(0, LEAST(gap-1, 4), 1) ELSE 0 END ofst, gap, future_rssi
		FROM (
			SELECT namespace, 
				beacon_id,
				gateway_id, 
				slot, 
				avg_rssi rssi,
				tx_power_1m tx_power,
				coalesce(extract(epoch from lead(slot) OVER (PARTITION BY namespace, beacon_id, gateway_id ORDER BY slot) - slot)::NUMERIC / 60, LEAST(extract(epoch from (SELECT max(slot) from beacon_reading) - slot)::NUMERIC / 60,5)) gap,
				lead(avg_rssi) OVER (PARTITION BY namespace, beacon_id, gateway_id ORDER BY slot) future_rssi
			FROM beacon_reading
			--WHERE slot > now() - INTERVAL '1 day'
		) x
	) y
),

moving_avg AS (
  SELECT namespace, beacon_id, gateway_id, slot, CASE WHEN rssi is not null THEN (sum(rssi) OVER w  / count(rssi) OVER w) END rssi, count(rssi) over w, tx_power
	FROM extended
	WINDOW w AS (PARTITION BY namespace, beacon_id, gateway_id ORDER BY slot ROWS BETWEEN 4 PRECEDING AND CURRENT ROW)
),

-- This has the smoothed RSSI and distance by beacon and gateway
smoothed AS (
	select namespace, beacon_id, gateway_id, slot, rssi, tx_power, POW(10, (tx_power - rssi) / 20.0) distance from moving_avg
),

gw_info AS (
  SELECT node_name, gateway_id, slot FROM beacon_reading GROUP BY 1,2,3
),

-- this has the basic winner election of the best gateway for each beacon
winner AS (
	SELECT winner.namespace, winner.beacon_id, winner.slot, 
		winner.gateway_id, winner.distance,
		b.subject subject,
		coalesce(b.subject_name, winner.beacon_id) beacon_name,
		coalesce(g.subject_name, gw.node_name) gateway_name,
		b.binding_id beacon_binding,
		g.binding_id gateway_binding
	FROM (
		SELECT DISTINCT ON (namespace, beacon_id, slot) namespace,
			beacon_id,
		    slot,
		    distance,
		    gateway_id
		FROM smoothed
		WHERE rssi IS NOT NULL AND distance <= 10
		ORDER BY 1, 2, 3, 4 ASC
	) winner
	LEFT OUTER JOIN customer_binding b ON
		winner.namespace = b.namespace AND 
		winner.beacon_id = b.beacon_id AND
		(b.start_time IS NULL OR winner.slot >= b.start_time) AND
		(b.end_time IS NULL OR winner.slot < b.end_time)
	LEFT OUTER JOIN customer_binding g ON
		'ad135bf7eecc08752f96' = g.namespace AND -- the Atollogy Edystone gateway namespace
		winner.gateway_id = g.beacon_id AND
		(g.start_time IS NULL OR winner.slot >= g.start_time) AND
		(g.end_time IS NULL OR winner.slot < g.end_time)
	JOIN gw_info gw USING (gateway_id, slot)
),

-- now we can do event coalescing:

gaps AS (
 	SELECT namespace, beacon_id,
 		slot,
 		gateway_id,
	    beacon_binding,
	    gateway_binding,
	    distance,
        CASE
            WHEN (lag(gateway_id) OVER (PARTITION BY namespace, beacon_id, beacon_binding ORDER BY slot) <> gateway_id) OR coalesce(lag(gateway_binding) OVER (PARTITION BY namespace, beacon_id, beacon_binding ORDER BY slot), 0) <> coalesce(gateway_binding,0) THEN 1
            ELSE 0
        END AS new_gate,
        CASE
            WHEN extract (epoch from slot - lag(slot) OVER (PARTITION BY namespace, beacon_id ORDER BY slot)) > 60 THEN 1
            ELSE 0
        END AS gap
	FROM winner
),

groups AS (
 	SELECT namespace, beacon_id,
 		gateway_id,
	    gateway_binding,
	    beacon_binding,
	    slot,
	    sum(new_gate + gap) OVER (PARTITION BY namespace, beacon_id, beacon_binding ORDER BY slot) AS grp
   	FROM gaps
),

events AS (
	SELECT namespace, beacon_id, 
		beacon_binding,
		gateway_binding, 
		gateway_id,
		grp, 
		min(slot) AS starttime,
	    max(slot) AS endtime -- note that for display or calculating duration, you should add one minute
    FROM groups
    GROUP BY 1,2,3,4,5,6
),

cm_events AS (
	SELECT a.namespace, a.beacon_id, a.grp, a.starttime, a.endtime, a.gateway_id,
		b.subject subject,
		coalesce(b.subject_name, a.beacon_id) beacon_name,
		coalesce(g.subject_name, a.gateway_id) gateway_name
	FROM events a
	LEFT OUTER JOIN customer_binding b ON a.beacon_binding = b.binding_id
	LEFT OUTER JOIN customer_binding g ON a.gateway_binding = g.binding_id
)

select * from cm_events



