COPY (
  SELECT count(*) AS beacons_counted, current_database() AS db,
      CASE WHEN current_database() IN ('atl', 'template040')
        THEN 'test'
      WHEN current_database() IN ('borgwarner','eimco','eurofins','fbx','generalmills','lumenetix','mcroskey','sbay','schaeffler','schneiderelectric','tesla','vdb')
        THEN 'manufacturing'
      WHEN current_database() IN ('cemex','cemexusa','lehighhanson','titanamerica','tlf','trimac')
        THEN 'yard ops'
      WHEN current_database() IN ('gatwickairport','tia')
        THEN 'ground ops'
      ELSE 'test' END sector,
    date_trunc('month', insert_time)
  FROM beacon_reading
  GROUP BY 3,4
  ORDER BY 3,4 DESC) TO stdout WITH CSV DELIMITER ',';
