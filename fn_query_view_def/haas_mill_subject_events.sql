WITH filtered_image_step AS (
    select * from image_step WHERE collection_time >= $1 AND collection_time < $2
),
bbox AS (
    SELECT
      c.subject_name,
      c.measure_name,
      date_trunc('minute', i.collection_time) + trunc(date_part('second', i.collection_time)/EXTRACT(epoch from c.collection_interval)) * c.collection_interval collection_time,
      COALESCE(i.step_output->>'reason', i.step_output->>'warning') as exception,
      CASE
        WHEN i.step_output ->> 'status' = 'unknown' OR i.step_output ->> 'status' = 'error' THEN NULL -- previously 'MISSING DATA'
        WHEN ((i.step_output -> 'subjects' ->> c.extract_key)::int > '0'::int) THEN 'present'
        WHEN (i.step_output -> 'subjects' ->> c.extract_key = '0') THEN 'absent'
      END state
    FROM filtered_image_step i
    LEFT OUTER JOIN customer_binding b ON
       i.gateway_id = b.beacon_id
       AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
       AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
       AND (b.end_time IS NULL OR i.collection_time < b.end_time)
    JOIN customer_report_config c ON
      b.subject_name = c.gateway_name
      AND i.camera_id = c.camera_id
      AND i.step_name = c.step_name
      AND c.extract_type = 'bbox'
),
state AS (
    SELECT
      c.subject_name,
      c.measure_name,
      date_trunc('minute', i.collection_time) + trunc(date_part('second', i.collection_time)/EXTRACT(epoch from c.collection_interval)) * c.collection_interval collection_time,
      CASE
        WHEN (i.step_output ->> 'brightness') :: float > 110 THEN 'BRIGHTNESS EXCEPTION'
        ELSE COALESCE(i.step_output->>'reason', i.step_output->>'warning')
      END exception,
      CASE
        WHEN i.step_output ->> 'status' = 'unknown' OR i.step_output ->> 'status' = 'error' THEN NULL -- previously 'MISSING DATA'
        ELSE i.step_output ->> c.extract_key
      END state
    FROM filtered_image_step i
    LEFT OUTER JOIN customer_binding b ON
       i.gateway_id = b.beacon_id
       AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
       AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
       AND (b.end_time IS NULL OR i.collection_time < b.end_time)
    JOIN customer_report_config c ON
      b.subject_name = c.gateway_name
      AND i.camera_id = c.camera_id
      AND i.step_name = c.step_name
      AND c.extract_type = 'state'
),
color_state AS (
    SELECT
      c.subject_name,
      c.measure_name,
      date_trunc('minute', i.collection_time) + trunc(date_part('second', i.collection_time)/EXTRACT(epoch from c.collection_interval)) * c.collection_interval collection_time,
      i.step_output ->> 'reason' as exception,
      CASE WHEN (i.step_output ->> 'state') IS NULL THEN NULL
           WHEN (i.step_output ->> 'state') <> 'off' THEN (i.step_output ->> 'state') || ' ' || (i.step_output ->> 'color')
           WHEN (i.step_output ->> 'state') = 'off' THEN 'off'
      END state
    FROM filtered_image_step i
    LEFT OUTER JOIN customer_binding b ON
       i.gateway_id = b.beacon_id
       AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
       AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
       AND (b.end_time IS NULL OR i.collection_time < b.end_time)
    JOIN customer_report_config c ON
      b.subject_name = c.gateway_name
      AND i.camera_id = c.camera_id
      AND i.step_name = c.step_name
      AND c.extract_type = 'color_state'
),
haas_mill_composite AS (
  SELECT
    c.subject_name,
    c.measure_name,
    o.collection_time,
    NULL::text exception,
    CASE
      WHEN b.state = 'solid green' THEN 'Running'
      WHEN o.state = 'present' THEN 'Setup/Load'
      WHEN (d.exception IS NULL OR d.exception <> 'BRIGHTNESS EXCEPTION') AND d.state = 'closed' THEN 'Ignored'
      ELSE 'Idle'
      END state
  FROM
   customer_report_config c
   JOIN bbox o ON
    o.subject_name = c.subject_name
    AND o.measure_name = 'operator'
   JOIN color_state b ON
    b.collection_time = o.collection_time
    AND b.subject_name = c.subject_name
    AND b.measure_name = 'bulb'
   JOIN state d ON
    d.collection_time = o.collection_time
    AND d.subject_name = c.subject_name
    AND d.measure_name = 'door'
  WHERE c.extract_type = 'haas_mill_composite'
),
subject_event AS (
  (SELECT * FROM bbox)
  UNION (SELECT * FROM state)
  UNION (SELECT * FROM color_state)
  UNION (SELECT * FROM haas_mill_composite)
)
